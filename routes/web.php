<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $alert = \TCG\Voyager\Models\Page::where('slug','alert')->first();
    $nossasPraticas = \TCG\Voyager\Models\Page::where('slug','section-nossas-praticas')->first();
    $curriculoRafaela = \TCG\Voyager\Models\Page::where('slug','curriculo-rafaela')->first();
    $curriculoVivien = \TCG\Voyager\Models\Page::where('slug','curriculo-vivien')->first();
    $about = \TCG\Voyager\Models\Page::where('slug','section-about')->first();
    return view('home',['nossasPraticas' => $nossasPraticas,
                        'curriculoRafaela' => $curriculoRafaela,
                        'curriculoVivien' => $curriculoVivien,
                        'about' => $about,
                        'alert' => $alert]);
});

Route::get('/cursos', function () {
    $curso = \TCG\Voyager\Models\Page::where('slug','curso')->first();
    return view('cursos',['curso' => $curso]);
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
