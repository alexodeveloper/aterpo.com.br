@extends('layout.master')

@section('content')
    <section class="section section--hero" js-handler="scrollToTarget" js-target="#home"> <div class="section__background" data-parallax="scroll" data-image-src="/assets/img/hero.png"></div> <div class="container container--flex"> <div class="section__content"> <h1 class="section__content__title"> <span class="line">Conheça nossos <span class="bolder">Cursos</span></span> </h1> <p class="section__content__paragraph">Cursos online ou presenciais em Fisioterapia Especializada na área do sono.</p> </div> </div> </section>
    <section class="section section--courses"> 
        <div class="container course"> 
            {!! $curso->body !!}    
        </div> 
    </section>
@endsection