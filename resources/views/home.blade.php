@extends('layout.master')

@section('content')


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: 80vw !important;">
        <div class="modal-content" style="padding: 20px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Aviso importante</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! $alert->body !!}
            </div>
        </div>
    </div>
</div>

<section class="section section--hero" js-handler="scrollToTarget" js-target="#home" id="home">
    <div class="section__background" data-parallax="scroll" data-image-src="/assets/img/hero.png"></div>
    <div class="container container--flex">
        <div class="section__content">
            <h1 class="section__content__title"><span class="line">ATERPO <span class="bolder">Consultoria</span></span> <span class="line"><span class="bolder">em CPAP & Bi-Nível</span></span>
            </h1>
            <p class="section__content__paragraph">Terapia com Pressão Positiva para Distúrbios Respiratórios do
                Sono.</p> <a href="#about" class="cta" js-handler="scrollToLink">Saiba mais</a>
        </div>
    </div>
</section>
<section class="section section--about" js-handler="scrollToTarget" js-target="#about" id="about">
    <div class="container">
        <h1 class="section__title">A ATERPO</h1>
        {!! $about->body !!}
    </div>
</section>
<section class="section section--services">
    <div class="container container--flex">
        <div class="service"><span class="service__icon"><i class="fas fa-users"></i></span>
            <h3 class="service__title">Adaptação personalizada</h3>
            <p class="service__description">CPAP e Binível (BIPAP®), com ou sem oxigênio suplementar e máscara a ser
                utilizada na terapia com pressão positiva.</p>
        </div>
        <div class="service"><span class="service__icon"><i class="fas fa-share-square"></i></span>
            <h3 class="service__title">Encaminhamento de laudos</h3>
            <p class="service__description">Envio de laudos evolutivos para o médico, sobre o uso do equipamento de
                pressão positiva.</p>
        </div>
        <div class="service"><span class="service__icon"><i class="fas fa-question-circle"></i></span>
            <h3 class="service__title">Orientações</h3>
            <p class="service__description">Orientações para o paciente sobre Higiene e Conforto do Sono, aquisição
                e cuidados com o equipamento e acessórios.</p>
        </div>
        <div class="service"><span class="service__icon"><i class="fas fa-comment"></i></span>
            <h3 class="service__title">Suporte personalizado</h3>
            <p class="service__description">Suporte personalizado para o paciente para resolução imediata de
                problemas de adaptação.</p>
        </div>
    </div>
    <div class="container container--flex">
        <div class="service"><span class="service__icon"><i class="fas fa-user-md"></i></span>
            <h3 class="service__title">Consultoria em fisioterapia</h3>
            <p class="service__description">Consultoria na área de distúrbios respiratórios do sono, para avaliação
                de terapia com pressão positiva (adesão e efetividade).</p>
        </div>
        <div class="service"><span class="service__icon"><i class="fas fa-book"></i></span>
            <h3 class="service__title">Cursos</h3>
            <p class="service__description">Cursos online ou presenciais sobre temas relacionados a área de
                distúrbios respiratórios do sono.</p>
        </div>
        <div class="service"><span class="service__icon"><i class="fas fa-medkit"></i></span>
            <h3 class="service__title">Exames</h3>
            <p class="service__description">A pedido médico realizamos poligrafia domiciliar noturna para
                diagnóstico de pacientes com suspeita de apneia obstrutiva do sono (laudo elaborado manualmente por
                médico parceiro da Aterpo).</p>
        </div>
    </div>
</section>
<section class="section section--about-dr">
    <div class="container">
        {!! $curriculoVivien->body !!}
    </div>
    <div class="container">
        {!! $curriculoRafaela->body !!}
    </div>

</section>
<section class="section section--our-practices" js-handler="scrollToTarget" js-target="#ourPractices" id="ourPractices">
    <div class="container">
        <h1 class="section__title">Nossas práticas</h1>
        {!! $nossasPraticas->body !!}
        <div class="gallery">
            <a href="/assets/img/image1.jpg" class="gallery__item">
                <div class="gallery__item__background" style="background-image: url(/assets/img/image1.jpg);"><img class="gallery__item__background__thumbnail" src="/assets/img/image1.jpg"></div>
                <div class="gallery__item__hover"><span class="gallery__item__hover__label">Dra. Vivien Piccin e Dra. Rafaela Andrade</span></div>
            </a>
            <a href="/assets/img/image2.jpg" class="gallery__item">
                <div class="gallery__item__background" style="background-image: url(/assets/img/image2.jpg);"><img class="gallery__item__background__thumbnail" src="/assets/img/image2.jpg"></div>
                <div class="gallery__item__hover"><span class="gallery__item__hover__label">Dra. Vivien Piccin e Dra. Rafaela Andrade</span></div>
            </a>
            <a href="/assets/img/image3.jpg" class="gallery__item">
                <div class="gallery__item__background" style="background-image: url(/assets/img/image3.jpg);"><img class="gallery__item__background__thumbnail" src="/assets/img/image3.jpg"></div>
                <div class="gallery__item__hover"><span class="gallery__item__hover__label">Sala</span></div>
            </a>

            <a href="/assets/img/image4.jpg" class="gallery__item">
                <div class="gallery__item__background" style="background-image: url(/assets/img/image4.jpg);"><img class="gallery__item__background__thumbnail" src="/assets/img/image4.jpg"></div>
                <div class="gallery__item__hover"><span class="gallery__item__hover__label">Recepção</span></div>
            </a>
        </div>
    </div>
</section>
<section class="section section--tips" js-handler="scrollToTarget" js-target="#tips" id="tips">
    <h1 class="section__title">Dicas e Orientações para o Paciente</h1>
    <div class="container container--flex tips">
        <div class="tip">
            <a href="/assets/pdf/Atuação-da-Fisioterapia-no-Sono.pdf" target="blank" class="tip__link">
                <p class="tip__name">Atuação da Fisioterapia no Sono</p> <img src="/assets/img/covers/AtuaÃ§Ã£o-da-Fisioterapia-no-Sono.jpg" alt="/home/storage/d/9d/80/aterpo1/public_html/public/img/covers/AtuaÃ§Ã£o-da-Fisioterapia-no-Sono.jpg" class="tip__thumb">
            </a>
        </div>
        <div class="tip">
            <a href="/assets/pdf/Cuidados-com-o-CPAP.pdf" target="blank" class="tip__link">
                <p class="tip__name">Cuidados com o CPAP</p> <img src="/assets/img/covers/Cuidados-com-o-CPAP.jpg" alt="/home/storage/d/9d/80/aterpo1/public_html/public/img/covers/Cuidados-com-o-CPAP.jpg" class="tip__thumb">
            </a>
        </div>
        <div class="tip">
            <a href="/assets/pdf/Errata-Atuação-da-Fisioterapia-no-Sono.pdf" target="blank" class="tip__link">
                <p class="tip__name">Errata Atuação da Fisioterapia no Sono</p> <img src="/assets/img/covers/Errata-AtuaÃ§Ã£o-da-Fisioterapia-no-Sono.jpg" alt="/home/storage/d/9d/80/aterpo1/public_html/public/img/covers/Errata-AtuaÃ§Ã£o-da-Fisioterapia-no-Sono.jpg" class="tip__thumb">
            </a>
        </div>
        <div class="tip">
            <a href="/assets/pdf/Higiene-do-Sono.pdf" target="blank" class="tip__link">
                <p class="tip__name">Higiene do Sono</p> <img src="/assets/img/covers/Higiene-do-Sono.jpg" alt="/home/storage/d/9d/80/aterpo1/public_html/public/img/covers/Higiene-do-Sono.jpg" class="tip__thumb">
            </a>
        </div>
        <div class="tip">
            <a href="/assets/pdf/O-que-é-o-CPAP.pdf" target="blank" class="tip__link">
                <p class="tip__name">O que é o CPAP</p> <img src="/assets/img/covers/O-que-Ã©-o-CPAP.jpg" alt="/home/storage/d/9d/80/aterpo1/public_html/public/img/covers/O-que-Ã©-o-CPAP.jpg" class="tip__thumb">
            </a>
        </div>
        <div class="tip">
            <a href="/assets/pdf/Programa-de-adaptação-ao-CPAP.pdf" target="blank" class="tip__link">
                <p class="tip__name">Programa de adaptação ao CPAP</p> <img src="/assets/img/covers/Programa-de-adaptaÃ§Ã£o-ao-CPAP.jpg" alt="/home/storage/d/9d/80/aterpo1/public_html/public/img/covers/Programa-de-adaptaÃ§Ã£o-ao-CPAP.jpg" class="tip__thumb">
            </a>
        </div>
    </div>
</section>
<section class="section section--video" data-parallax="scroll" data-image-src="/assets/img/video.jpg" js-handler="scrollToTarget" js-target="#videos" id="videos">
    <div class="container">
        <h1 class="section__title">Vídeos</h1>
        <div class="swiper-container video-slider">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <iframe src="https://www.youtube.com/embed/De6rAJNsC3I"></iframe>
                </div>
                <div class="swiper-slide">
                    <iframe src="https://www.youtube.com/embed/8I0RVe62kdc"></iframe>
                </div>
                <div class="swiper-slide">
                    <iframe src="https://www.youtube.com/embed/NasXFqKG8HU"></iframe>
                </div>
                <div class="swiper-slide">
                    <iframe src="https://www.youtube.com/embed/nA4fS353_WY"></iframe>
                </div>
            </div>
        </div>
        <div class="video-slider-control prev-video"><i class="fas fa-chevron-left"></i></div>
        <div class="video-slider-control next-video"><i class="fas fa-chevron-right"></i></div>
    </div>
</section>
<section class="section section--our-courses" js-handler="scrollToTarget" js-target="#ourCourses" id="ourCourses">
    <div class="container">
        <h1 class="section__title">Conheça nossos cursos!</h1>
        <h3 class="section__subtitle">Cursos online ou presenciais em Fisioterapia Especializada na área do
            sono</h3> <a href="/cursos" class="cta">Mais informações</a>
    </div>
</section>
@endsection
@section('js')
<script>
    jQuery(window).on('load', function() {
        jQuery('.bd-example-modal-lg').modal('toggle')
    })
</script>
@endsection
