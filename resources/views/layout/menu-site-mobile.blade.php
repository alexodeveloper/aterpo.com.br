@foreach($items as $menu_item)
<a href="{{ $menu_item->link() }}" class="navbar__link navbar__link--mobile active" js-handler="scrollToLink">{{ $menu_item->title }}</a> 
@endforeach