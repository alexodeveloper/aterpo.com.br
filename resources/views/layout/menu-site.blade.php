@foreach($items as $menu_item)
<a href="{{ $menu_item->link() }}" class="navbar__link active"> <span class="navbar__link__label">{{ $menu_item->title }}</span> <span class="navbar__link__label__hidden">{{ $menu_item->title }}</span> </a>
@endforeach