<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-us"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en-us"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en-us"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en-us" class="no-js">
<!--<![endif]-->

<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ setting('site.title') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <!-- ROBOTS -->
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow">
    <meta name="audience" content="all">
    <!-- GENERAL -->
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="rating" content="GENERAL">
    <link rel="canonical" href="/">
    <!-- OPEN GRAPH -->
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="" />
    <!-- IOS SPECIFIC -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-TileImage" content="/assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#000000">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- MODERNIZR -->
    <script>
        window.Modernizr = function(a, b, c) {
                function D(a) {
                    j.cssText = a
                }

                function E(a, b) {
                    return D(n.join(a + ";") + (b || ""))
                }

                function F(a, b) {
                    return typeof a === b
                }

                function G(a, b) {
                    return !!~("" + a).indexOf(b)
                }

                function H(a, b) {
                    for (var d in a) {
                        var e = a[d];
                        if (!G(e, "-") && j[e] !== c) return b == "pfx" ? e : !0
                    }
                    return !1
                }

                function I(a, b, d) {
                    for (var e in a) {
                        var f = b[a[e]];
                        if (f !== c) return d === !1 ? a[e] : F(f, "function") ? f.bind(d || b) : f
                    }
                    return !1
                }

                function J(a, b, c) {
                    var d = a.charAt(0).toUpperCase() + a.slice(1),
                        e = (a + " " + p.join(d + " ") + d).split(" ");
                    return F(b, "string") || F(b, "undefined") ? H(e, b) : (e = (a + " " + q.join(d + " ") + d).split(" "), I(e, b, c))
                }

                function K() {
                    e.input = function(c) {
                        for (var d = 0, e = c.length; d < e; d++) u[c[d]] = c[d] in k;
                        return u.list && (u.list = !!b.createElement("datalist") && !!a.HTMLDataListElement), u
                    }("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")), e.inputtypes = function(a) {
                        for (var d = 0, e, f, h, i = a.length; d < i; d++) k.setAttribute("type", f = a[d]), e = k.type !== "text", e && (k.value = l, k.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(f) && k.style.WebkitAppearance !== c ? (g.appendChild(k), h = b.defaultView, e = h.getComputedStyle && h.getComputedStyle(k, null).WebkitAppearance !== "textfield" && k.offsetHeight !== 0, g.removeChild(k)) : /^(search|tel)$/.test(f) || (/^(url|email)$/.test(f) ? e = k.checkValidity && k.checkValidity() === !1 : e = k.value != l)), t[a[d]] = !!e;
                        return t
                    }("search tel url email datetime date month week time datetime-local number range color".split(" "))
                }
                var d = "2.8.3",
                    e = {},
                    f = !0,
                    g = b.documentElement,
                    h = "modernizr",
                    i = b.createElement(h),
                    j = i.style,
                    k = b.createElement("input"),
                    l = ":)",
                    m = {}.toString,
                    n = " -webkit- -moz- -o- -ms- ".split(" "),
                    o = "Webkit Moz O ms",
                    p = o.split(" "),
                    q = o.toLowerCase().split(" "),
                    r = {
                        svg: "http://www.w3.org/2000/svg"
                    },
                    s = {},
                    t = {},
                    u = {},
                    v = [],
                    w = v.slice,
                    x, y = function(a, c, d, e) {
                        var f, i, j, k, l = b.createElement("div"),
                            m = b.body,
                            n = m || b.createElement("body");
                        if (parseInt(d, 10))
                            while (d--) j = b.createElement("div"), j.id = e ? e[d] : h + (d + 1), l.appendChild(j);
                        return f = ["&#173;", '<style id="s', h, '">', a, "</style>"].join(""), l.id = h, (m ? l : n).innerHTML += f, n.appendChild(l), m || (n.style.background = "", n.style.overflow = "hidden", k = g.style.overflow, g.style.overflow = "hidden", g.appendChild(n)), i = c(l, a), m ? l.parentNode.removeChild(l) : (n.parentNode.removeChild(n), g.style.overflow = k), !!i
                    },
                    z = function(b) {
                        var c = a.matchMedia || a.msMatchMedia;
                        if (c) return c(b) && c(b).matches || !1;
                        var d;
                        return y("@media " + b + " { #" + h + " { position: absolute; } }", function(b) {
                            d = (a.getComputedStyle ? getComputedStyle(b, null) : b.currentStyle)["position"] == "absolute"
                        }), d
                    },
                    A = function() {
                        function d(d, e) {
                            e = e || b.createElement(a[d] || "div"), d = "on" + d;
                            var f = d in e;
                            return f || (e.setAttribute || (e = b.createElement("div")), e.setAttribute && e.removeAttribute && (e.setAttribute(d, ""), f = F(e[d], "function"), F(e[d], "undefined") || (e[d] = c), e.removeAttribute(d))), e = null, f
                        }
                        var a = {
                            select: "input",
                            change: "input",
                            submit: "form",
                            reset: "form",
                            error: "img",
                            load: "img",
                            abort: "img"
                        };
                        return d
                    }(),
                    B = {}.hasOwnProperty,
                    C;
                !F(B, "undefined") && !F(B.call, "undefined") ? C = function(a, b) {
                    return B.call(a, b)
                } : C = function(a, b) {
                    return b in a && F(a.constructor.prototype[b], "undefined")
                }, Function.prototype.bind || (Function.prototype.bind = function(b) {
                    var c = this;
                    if (typeof c != "function") throw new TypeError;
                    var d = w.call(arguments, 1),
                        e = function() {
                            if (this instanceof e) {
                                var a = function() {};
                                a.prototype = c.prototype;
                                var f = new a,
                                    g = c.apply(f, d.concat(w.call(arguments)));
                                return Object(g) === g ? g : f
                            }
                            return c.apply(b, d.concat(w.call(arguments)))
                        };
                    return e
                }), s.flexbox = function() {
                    return J("flexWrap")
                }, s.canvas = function() {
                    var a = b.createElement("canvas");
                    return !!a.getContext && !!a.getContext("2d")
                }, s.canvastext = function() {
                    return !!e.canvas && !!F(b.createElement("canvas").getContext("2d").fillText, "function")
                }, s.webgl = function() {
                    return !!a.WebGLRenderingContext
                }, s.touch = function() {
                    var c;
                    return "ontouchstart" in a || a.DocumentTouch && b instanceof DocumentTouch ? c = !0 : y(["@media (", n.join("touch-enabled),("), h, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(a) {
                        c = a.offsetTop === 9
                    }), c
                }, s.geolocation = function() {
                    return "geolocation" in navigator
                }, s.postmessage = function() {
                    return !!a.postMessage
                }, s.websqldatabase = function() {
                    return !!a.openDatabase
                }, s.indexedDB = function() {
                    return !!J("indexedDB", a)
                }, s.hashchange = function() {
                    return A("hashchange", a) && (b.documentMode === c || b.documentMode > 7)
                }, s.history = function() {
                    return !!a.history && !!history.pushState
                }, s.draganddrop = function() {
                    var a = b.createElement("div");
                    return "draggable" in a || "ondragstart" in a && "ondrop" in a
                }, s.websockets = function() {
                    return "WebSocket" in a || "MozWebSocket" in a
                }, s.rgba = function() {
                    return D("background-color:rgba(150,255,150,.5)"), G(j.backgroundColor, "rgba")
                }, s.hsla = function() {
                    return D("background-color:hsla(120,40%,100%,.5)"), G(j.backgroundColor, "rgba") || G(j.backgroundColor, "hsla")
                }, s.multiplebgs = function() {
                    return D("background:url(https://),url(https://),red url(https://)"), /(url\s*\(.*?){3}/.test(j.background)
                }, s.backgroundsize = function() {
                    return J("backgroundSize")
                }, s.borderimage = function() {
                    return J("borderImage")
                }, s.borderradius = function() {
                    return J("borderRadius")
                }, s.boxshadow = function() {
                    return J("boxShadow")
                }, s.textshadow = function() {
                    return b.createElement("div").style.textShadow === ""
                }, s.opacity = function() {
                    return E("opacity:.55"), /^0.55$/.test(j.opacity)
                }, s.cssanimations = function() {
                    return J("animationName")
                }, s.csscolumns = function() {
                    return J("columnCount")
                }, s.cssgradients = function() {
                    var a = "background-image:",
                        b = "gradient(linear,left top,right bottom,from(#9f9),to(white));",
                        c = "linear-gradient(left top,#9f9, white);";
                    return D((a + "-webkit- ".split(" ").join(b + a) + n.join(c + a)).slice(0, -a.length)), G(j.backgroundImage, "gradient")
                }, s.cssreflections = function() {
                    return J("boxReflect")
                }, s.csstransforms = function() {
                    return !!J("transform")
                }, s.csstransforms3d = function() {
                    var a = !!J("perspective");
                    return a && "webkitPerspective" in g.style && y("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(b, c) {
                        a = b.offsetLeft === 9 && b.offsetHeight === 3
                    }), a
                }, s.csstransitions = function() {
                    return J("transition")
                }, s.fontface = function() {
                    var a;
                    return y('@font-face {font-family:"font";src:url("https://")}', function(c, d) {
                        var e = b.getElementById("smodernizr"),
                            f = e.sheet || e.styleSheet,
                            g = f ? f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "" : "";
                        a = /src/i.test(g) && g.indexOf(d.split(" ")[0]) === 0
                    }), a
                }, s.generatedcontent = function() {
                    var a;
                    return y(["#", h, "{font:0/0 a}#", h, ':after{content:"', l, '";visibility:hidden;font:3px/1 a}'].join(""), function(b) {
                        a = b.offsetHeight >= 3
                    }), a
                }, s.video = function() {
                    var a = b.createElement("video"),
                        c = !1;
                    try {
                        if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), c.h264 = a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), c.webm = a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, "")
                    } catch (d) {}
                    return c
                }, s.audio = function() {
                    var a = b.createElement("audio"),
                        c = !1;
                    try {
                        if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""), c.mp3 = a.canPlayType("audio/mpeg;").replace(/^no$/, ""), c.wav = a.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""), c.m4a = (a.canPlayType("audio/x-m4a;") || a.canPlayType("audio/aac;")).replace(/^no$/, "")
                    } catch (d) {}
                    return c
                }, s.localstorage = function() {
                    try {
                        return localStorage.setItem(h, h), localStorage.removeItem(h), !0
                    } catch (a) {
                        return !1
                    }
                }, s.sessionstorage = function() {
                    try {
                        return sessionStorage.setItem(h, h), sessionStorage.removeItem(h), !0
                    } catch (a) {
                        return !1
                    }
                }, s.webworkers = function() {
                    return !!a.Worker
                }, s.applicationcache = function() {
                    return !!a.applicationCache
                }, s.svg = function() {
                    return !!b.createElementNS && !!b.createElementNS(r.svg, "svg").createSVGRect
                }, s.inlinesvg = function() {
                    var a = b.createElement("div");
                    return a.innerHTML = "<svg/>", (a.firstChild && a.firstChild.namespaceURI) == r.svg
                }, s.smil = function() {
                    return !!b.createElementNS && /SVGAnimate/.test(m.call(b.createElementNS(r.svg, "animate")))
                }, s.svgclippaths = function() {
                    return !!b.createElementNS && /SVGClipPath/.test(m.call(b.createElementNS(r.svg, "clipPath")))
                };
                for (var L in s) C(s, L) && (x = L.toLowerCase(), e[x] = s[L](), v.push((e[x] ? "" : "no-") + x));
                return e.input || K(), e.addTest = function(a, b) {
                        if (typeof a == "object")
                            for (var d in a) C(a, d) && e.addTest(d, a[d]);
                        else {
                            a = a.toLowerCase();
                            if (e[a] !== c) return e;
                            b = typeof b == "function" ? b() : b, typeof f != "undefined" && f && (g.className += " " + (b ? "" : "no-") + a), e[a] = b
                        }
                        return e
                    }, D(""), i = k = null,
                    function(a, b) {
                        function l(a, b) {
                            var c = a.createElement("p"),
                                d = a.getElementsByTagName("head")[0] || a.documentElement;
                            return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild)
                        }

                        function m() {
                            var a = s.elements;
                            return typeof a == "string" ? a.split(" ") : a
                        }

                        function n(a) {
                            var b = j[a[h]];
                            return b || (b = {}, i++, a[h] = i, j[i] = b), b
                        }

                        function o(a, c, d) {
                            c || (c = b);
                            if (k) return c.createElement(a);
                            d || (d = n(c));
                            var g;
                            return d.cache[a] ? g = d.cache[a].cloneNode() : f.test(a) ? g = (d.cache[a] = d.createElem(a)).cloneNode() : g = d.createElem(a), g.canHaveChildren && !e.test(a) && !g.tagUrn ? d.frag.appendChild(g) : g
                        }

                        function p(a, c) {
                            a || (a = b);
                            if (k) return a.createDocumentFragment();
                            c = c || n(a);
                            var d = c.frag.cloneNode(),
                                e = 0,
                                f = m(),
                                g = f.length;
                            for (; e < g; e++) d.createElement(f[e]);
                            return d
                        }

                        function q(a, b) {
                            b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment, b.frag = b.createFrag()), a.createElement = function(c) {
                                return s.shivMethods ? o(c, a, b) : b.createElem(c)
                            }, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + m().join().replace(/[\w\-]+/g, function(a) {
                                return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")'
                            }) + ");return n}")(s, b.frag)
                        }

                        function r(a) {
                            a || (a = b);
                            var c = n(a);
                            return s.shivCSS && !g && !c.hasCSS && (c.hasCSS = !!l(a, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), k || q(a, c), a
                        }
                        var c = "3.7.0",
                            d = a.html5 || {},
                            e = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
                            f = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
                            g, h = "_html5shiv",
                            i = 0,
                            j = {},
                            k;
                        (function() {
                            try {
                                var a = b.createElement("a");
                                a.innerHTML = "<xyz></xyz>", g = "hidden" in a, k = a.childNodes.length == 1 || function() {
                                    b.createElement("a");
                                    var a = b.createDocumentFragment();
                                    return typeof a.cloneNode == "undefined" || typeof a.createDocumentFragment == "undefined" || typeof a.createElement == "undefined"
                                }()
                            } catch (c) {
                                g = !0, k = !0
                            }
                        })();
                        var s = {
                            elements: d.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
                            version: c,
                            shivCSS: d.shivCSS !== !1,
                            supportsUnknownElements: k,
                            shivMethods: d.shivMethods !== !1,
                            type: "default",
                            shivDocument: r,
                            createElement: o,
                            createDocumentFragment: p
                        };
                        a.html5 = s, r(b)
                    }(this, b), e._version = d, e._prefixes = n, e._domPrefixes = q, e._cssomPrefixes = p, e.mq = z, e.hasEvent = A, e.testProp = function(a) {
                        return H([a])
                    }, e.testAllProps = J, e.testStyles = y, e.prefixed = function(a, b, c) {
                        return b ? J(a, b, c) : J(a, "pfx")
                    }, g.className = g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (f ? " js " + v.join(" ") : ""), e
            }(this, this.document),
            function(a, b, c) {
                function d(a) {
                    return "[object Function]" == o.call(a)
                }

                function e(a) {
                    return "string" == typeof a
                }

                function f() {}

                function g(a) {
                    return !a || "loaded" == a || "complete" == a || "uninitialized" == a
                }

                function h() {
                    var a = p.shift();
                    q = 1, a ? a.t ? m(function() {
                        ("c" == a.t ? B.injectCss : B.injectJs)(a.s, 0, a.a, a.x, a.e, 1)
                    }, 0) : (a(), h()) : q = 0
                }

                function i(a, c, d, e, f, i, j) {
                    function k(b) {
                        if (!o && g(l.readyState) && (u.r = o = 1, !q && h(), l.onload = l.onreadystatechange = null, b)) {
                            "img" != a && m(function() {
                                t.removeChild(l)
                            }, 50);
                            for (var d in y[c]) y[c].hasOwnProperty(d) && y[c][d].onload()
                        }
                    }
                    var j = j || B.errorTimeout,
                        l = b.createElement(a),
                        o = 0,
                        r = 0,
                        u = {
                            t: d,
                            s: c,
                            e: f,
                            a: i,
                            x: j
                        };
                    1 === y[c] && (r = 1, y[c] = []), "object" == a ? l.data = c : (l.src = c, l.type = a), l.width = l.height = "0", l.onerror = l.onload = l.onreadystatechange = function() {
                        k.call(this, r)
                    }, p.splice(e, 0, u), "img" != a && (r || 2 === y[c] ? (t.insertBefore(l, s ? null : n), m(k, j)) : y[c].push(l))
                }

                function j(a, b, c, d, f) {
                    return q = 0, b = b || "j", e(a) ? i("c" == b ? v : u, a, b, this.i++, c, d, f) : (p.splice(this.i++, 0, a), 1 == p.length && h()), this
                }

                function k() {
                    var a = B;
                    return a.loader = {
                        load: j,
                        i: 0
                    }, a
                }
                var l = b.documentElement,
                    m = a.setTimeout,
                    n = b.getElementsByTagName("script")[0],
                    o = {}.toString,
                    p = [],
                    q = 0,
                    r = "MozAppearance" in l.style,
                    s = r && !!b.createRange().compareNode,
                    t = s ? l : n.parentNode,
                    l = a.opera && "[object Opera]" == o.call(a.opera),
                    l = !!b.attachEvent && !l,
                    u = r ? "object" : l ? "script" : "img",
                    v = l ? "script" : u,
                    w = Array.isArray || function(a) {
                        return "[object Array]" == o.call(a)
                    },
                    x = [],
                    y = {},
                    z = {
                        timeout: function(a, b) {
                            return b.length && (a.timeout = b[0]), a
                        }
                    },
                    A, B;
                B = function(a) {
                    function b(a) {
                        var a = a.split("!"),
                            b = x.length,
                            c = a.pop(),
                            d = a.length,
                            c = {
                                url: c,
                                origUrl: c,
                                prefixes: a
                            },
                            e, f, g;
                        for (f = 0; f < d; f++) g = a[f].split("="), (e = z[g.shift()]) && (c = e(c, g));
                        for (f = 0; f < b; f++) c = x[f](c);
                        return c
                    }

                    function g(a, e, f, g, h) {
                        var i = b(a),
                            j = i.autoCallback;
                        i.url.split(".").pop().split("?").shift(), i.bypass || (e && (e = d(e) ? e : e[a] || e[g] || e[a.split("/").pop().split("?")[0]]), i.instead ? i.instead(a, e, f, g, h) : (y[i.url] ? i.noexec = !0 : y[i.url] = 1, f.load(i.url, i.forceCSS || !i.forceJS && "css" == i.url.split(".").pop().split("?").shift() ? "c" : c, i.noexec, i.attrs, i.timeout), (d(e) || d(j)) && f.load(function() {
                            k(), e && e(i.origUrl, h, g), j && j(i.origUrl, h, g), y[i.url] = 2
                        })))
                    }

                    function h(a, b) {
                        function c(a, c) {
                            if (a) {
                                if (e(a)) c || (j = function() {
                                    var a = [].slice.call(arguments);
                                    k.apply(this, a), l()
                                }), g(a, j, b, 0, h);
                                else if (Object(a) === a)
                                    for (n in m = function() {
                                            var b = 0,
                                                c;
                                            for (c in a) a.hasOwnProperty(c) && b++;
                                            return b
                                        }(), a) a.hasOwnProperty(n) && (!c && !--m && (d(j) ? j = function() {
                                        var a = [].slice.call(arguments);
                                        k.apply(this, a), l()
                                    } : j[n] = function(a) {
                                        return function() {
                                            var b = [].slice.call(arguments);
                                            a && a.apply(this, b), l()
                                        }
                                    }(k[n])), g(a[n], j, b, n, h))
                            } else !c && l()
                        }
                        var h = !!a.test,
                            i = a.load || a.both,
                            j = a.callback || f,
                            k = j,
                            l = a.complete || f,
                            m, n;
                        c(h ? a.yep : a.nope, !!i), i && c(i)
                    }
                    var i, j, l = this.yepnope.loader;
                    if (e(a)) g(a, 0, l, 0);
                    else if (w(a))
                        for (i = 0; i < a.length; i++) j = a[i], e(j) ? g(j, 0, l, 0) : w(j) ? B(j) : Object(j) === j && h(j, l);
                    else Object(a) === a && h(a, l)
                }, B.addPrefix = function(a, b) {
                    z[a] = b
                }, B.addFilter = function(a) {
                    x.push(a)
                }, B.errorTimeout = 1e4, null == b.readyState && b.addEventListener && (b.readyState = "loading", b.addEventListener("DOMContentLoaded", A = function() {
                    b.removeEventListener("DOMContentLoaded", A, 0), b.readyState = "complete"
                }, 0)), a.yepnope = k(), a.yepnope.executeStack = h, a.yepnope.injectJs = function(a, c, d, e, i, j) {
                    var k = b.createElement("script"),
                        l, o, e = e || B.errorTimeout;
                    k.src = a;
                    for (o in d) k.setAttribute(o, d[o]);
                    c = j ? h : c || f, k.onreadystatechange = k.onload = function() {
                        !l && g(k.readyState) && (l = 1, c(), k.onload = k.onreadystatechange = null)
                    }, m(function() {
                        l || (l = 1, c(1))
                    }, e), i ? k.onload() : n.parentNode.insertBefore(k, n)
                }, a.yepnope.injectCss = function(a, c, d, e, g, i) {
                    var e = b.createElement("link"),
                        j, c = i ? h : c || f;
                    e.href = a, e.rel = "stylesheet", e.type = "text/css";
                    for (j in d) e.setAttribute(j, d[j]);
                    g || (n.parentNode.insertBefore(e, n), m(c, 0))
                }
            }(this, document), Modernizr.load = function() {
                yepnope.apply(window, [].slice.call(arguments, 0))
            }
    </script>
    <!-- STYLES -->
    <style>
        html {
            font-family: sans-serif;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%
        }

        body {
            margin: 0
        }

        article,
        aside,
        details,
        figcaption,
        figure,
        footer,
        header,
        main,
        menu,
        nav,
        section,
        summary {
            display: block
        }

        audio,
        canvas,
        progress,
        video {
            display: inline-block
        }

        audio:not([controls]) {
            display: none;
            height: 0
        }

        progress {
            vertical-align: baseline
        }

        template,
        [hidden] {
            display: none
        }

        a {
            background-color: transparent;
            -webkit-text-decoration-skip: objects
        }

        a:active,
        a:hover {
            outline-width: 0
        }

        abbr[title] {
            border-bottom: none;
            text-decoration: underline;
            text-decoration: underline dotted
        }

        b,
        strong {
            font-weight: inherit
        }

        b,
        strong {
            font-weight: bolder
        }

        dfn {
            font-style: italic
        }

        h1 {
            font-size: 2em;
            margin: .67em 0
        }

        mark {
            background-color: #ff0;
            color: #000
        }

        small {
            font-size: 80%
        }

        sub,
        sup {
            font-size: 75%;
            line-height: 0;
            position: relative;
            vertical-align: baseline
        }

        sub {
            bottom: -.25em
        }

        sup {
            top: -.5em
        }

        img {
            border-style: none
        }

        svg:not(:root) {
            overflow: hidden
        }

        code,
        kbd,
        pre,
        samp {
            font-family: monospace, monospace;
            font-size: 1em
        }

        figure {
            margin: 1em 40px
        }

        hr {
            box-sizing: content-box;
            height: 0;
            overflow: visible
        }

        button,
        input,
        select,
        textarea {
            font: inherit;
            margin: 0
        }

        optgroup {
            font-weight: 700
        }

        button,
        input {
            overflow: visible
        }

        button,
        select {
            text-transform: none
        }

        button,
        html [type="button"],
        [type="reset"],
        [type="submit"] {
            -webkit-appearance: button
        }

        button::-moz-focus-inner,
        [type="button"]::-moz-focus-inner,
        [type="reset"]::-moz-focus-inner,
        [type="submit"]::-moz-focus-inner {
            border-style: none;
            padding: 0
        }

        button:-moz-focusring,
        [type="button"]:-moz-focusring,
        [type="reset"]:-moz-focusring,
        [type="submit"]:-moz-focusring {
            outline: 1px dotted ButtonText
        }

        fieldset {
            border: 1px solid silver;
            margin: 0 2px;
            padding: .35em .625em .75em
        }

        legend {
            box-sizing: border-box;
            color: inherit;
            display: table;
            max-width: 100%;
            padding: 0;
            white-space: normal
        }

        textarea {
            overflow: auto
        }

        [type="checkbox"],
        [type="radio"] {
            box-sizing: border-box;
            padding: 0
        }

        [type="number"]::-webkit-inner-spin-button,
        [type="number"]::-webkit-outer-spin-button {
            height: auto
        }

        [type="search"] {
            -webkit-appearance: textfield;
            outline-offset: -2px
        }

        [type="search"]::-webkit-search-cancel-button,
        [type="search"]::-webkit-search-decoration {
            -webkit-appearance: none
        }

        ::-webkit-input-placeholder {
            color: inherit;
            opacity: .54
        }

        ::-webkit-file-upload-button {
            -webkit-appearance: button;
            font: inherit
        }

        *,
        *:before,
        *:after {
            -webkit-font-smoothing: antialiased;
            -moz-font-smoothing: antialiased;
            -ms-font-smoothing: antialiased;
            -o-font-smoothing: antialiased;
            font-smoothing: antialiased;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            text-rendering: optimizeLegibility
        }

        .clearfix:after {
            display: block;
            content: "";
            font-size: 0;
            clear: both;
            zoom: 1
        }

        object {
            display: none
        }

        .cfm-layer {
            display: block;
            width: 100%;
            height: 100vh;
            background: rgba(0, 0, 0, 0);
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999999999;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: all 0.3s ease-out;
            -moz-transition: all 0.3s ease-out;
            -ms-transition: all 0.3s ease-out;
            -o-transition: all 0.3s ease-out;
            transition: all 0.3s ease-out
        }

        .cfm-layer.cfm-visible {
            background: rgba(0, 0, 0, .25)
        }

        .cfm-layer .cfm-box-container {
            display: block;
            width: 100%;
            height: auto;
            position: absolute;
            top: 0;
            left: 0;
            padding: 15px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: all 0.3s ease-out;
            -moz-transition: all 0.3s ease-out;
            -ms-transition: all 0.3s ease-out;
            -o-transition: all 0.3s ease-out;
            transition: all 0.3s ease-out;
            -webkit-transform: translateY(-100%);
            -moz-transform: translateY(-100%);
            -ms-transform: translateY(-100%);
            -o-transform: translateY(-100%);
            transform: translateY(-100%)
        }

        .cfm-layer .cfm-box-container.cfm-visible {
            -webkit-transform: translateY(0);
            -moz-transform: translateY(0);
            -ms-transform: translateY(0);
            -o-transform: translateY(0);
            transform: translateY(0)
        }

        .cfm-layer .cfm-box-container .cfm-box {
            display: block;
            width: 100%;
            height: auto;
            background: rgba(255, 255, 255, .85);
            border-radius: 7px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-backdrop-filter: blur(10px);
            -moz-backdrop-filter: blur(10px);
            -ms-backdrop-filter: blur(10px);
            -o-backdrop-filter: blur(10px);
            backdrop-filter: blur(10px)
        }

        .cfm-layer .cfm-box-container .cfm-box:after {
            display: block;
            content: "";
            font-size: 0;
            zoom: 1;
            clear: both
        }

        .cfm-layer .cfm-box-container .cfm-box .cfm-message {
            display: block;
            width: 100%;
            height: auto;
            font-family: inherit;
            font-weight: inherit;
            font-size: inherit;
            color: #000;
            padding: 15px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            border-bottom: 1px solid rgba(0, 0, 0, .1)
        }

        .cfm-layer .cfm-box-container .cfm-box .cfm-btns {
            display: block;
            float: right;
            padding: 0;
            margin: 0;
            list-style-type: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box
        }

        .cfm-layer .cfm-box-container .cfm-box .cfm-btns:after {
            display: block;
            content: "";
            font-size: 0;
            zoom: 1;
            clear: both
        }

        .cfm-layer .cfm-box-container .cfm-box .cfm-btns .cfm-btn {
            display: block;
            float: left;
            margin: 0;
            padding: 15px;
            font-family: inherit;
            font-size: inherit;
            font-weight: inherit;
            color: inherit;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box
        }

        .cfm-layer .cfm-box-container .cfm-box .cfm-btns .cfm-btn.cfm-btn-separator {
            padding: 15px 0
        }

        .cfm-layer .cfm-box-container .cfm-box .cfm-btns .cfm-btn.cfm-btn-agree,
        .cfm-layer .cfm-box-container .cfm-box .cfm-btns .cfm-btn.cfm-btn-disagree {
            cursor: pointer
        }

        .ntf-box-container {
            display: block;
            width: 100%;
            height: auto;
            position: fixed;
            top: 0;
            left: 0;
            padding: 15px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: all 0.3s ease-out;
            -moz-transition: all 0.3s ease-out;
            -ms-transition: all 0.3s ease-out;
            -o-transition: all 0.3s ease-out;
            transition: all 0.3s ease-out;
            -webkit-transform: translateY(-100%);
            -moz-transform: translateY(-100%);
            -ms-transform: translateY(-100%);
            -o-transform: translateY(-100%);
            transform: translateY(-100%);
            z-index: 999999999
        }

        .ntf-box-container.ntf-visible {
            -webkit-transform: translateY(0);
            -moz-transform: translateY(0);
            -ms-transform: translateY(0);
            -o-transform: translateY(0);
            transform: translateY(0)
        }

        .ntf-box-container .ntf-box {
            display: block;
            width: 100%;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box
        }

        .ntf-box-container .ntf-box .ntf-message {
            display: block;
            width: 100%;
            height: auto;
            font-family: inherit;
            font-weight: inherit;
            font-size: inherit;
            color: #fff;
            padding: 15px;
            cursor: pointer;
            text-align: center;
            border-radius: 7px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-backdrop-filter: blur(10px);
            -moz-backdrop-filter: blur(10px);
            -ms-backdrop-filter: blur(10px);
            -o-backdrop-filter: blur(10px);
            backdrop-filter: blur(10px)
        }

        .ntf-box-container .ntf-box .ntf-message.ntf-message-success {
            background: rgba(0, 128, 0, .85)
        }

        .ntf-box-container .ntf-box .ntf-message.ntf-message-error {
            background: rgba(255, 0, 0, .85)
        }

        .ppt-layer {
            display: block;
            width: 100%;
            height: 100vh;
            background: rgba(0, 0, 0, 0);
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999999999;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: all 0.3s ease-out;
            -moz-transition: all 0.3s ease-out;
            -ms-transition: all 0.3s ease-out;
            -o-transition: all 0.3s ease-out;
            transition: all 0.3s ease-out
        }

        .ppt-layer.ppt-visible {
            background: rgba(0, 0, 0, .25)
        }

        .ppt-layer .ppt-box-container {
            display: block;
            width: 100%;
            height: auto;
            position: absolute;
            top: 0;
            left: 0;
            padding: 15px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: all 0.3s ease-out;
            -moz-transition: all 0.3s ease-out;
            -ms-transition: all 0.3s ease-out;
            -o-transition: all 0.3s ease-out;
            transition: all 0.3s ease-out;
            -webkit-transform: translateY(-100%);
            -moz-transform: translateY(-100%);
            -ms-transform: translateY(-100%);
            -o-transform: translateY(-100%);
            transform: translateY(-100%)
        }

        .ppt-layer .ppt-box-container.ppt-visible {
            -webkit-transform: translateY(0);
            -moz-transform: translateY(0);
            -ms-transform: translateY(0);
            -o-transform: translateY(0);
            transform: translateY(0)
        }

        .ppt-layer .ppt-box-container .ppt-box {
            display: block;
            width: 100%;
            height: auto;
            background: rgba(255, 255, 255, .85);
            border-radius: 7px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-backdrop-filter: blur(10px);
            -moz-backdrop-filter: blur(10px);
            -ms-backdrop-filter: blur(10px);
            -o-backdrop-filter: blur(10px);
            backdrop-filter: blur(10px)
        }

        .ppt-layer .ppt-box-container .ppt-box:after {
            display: block;
            content: "";
            font-size: 0;
            zoom: 1;
            clear: both
        }

        .ppt-layer .ppt-box-container .ppt-box .ppt-message {
            display: block;
            width: 100%;
            height: auto;
            font-family: inherit;
            font-weight: inherit;
            font-size: inherit;
            color: #000;
            padding: 15px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box
        }

        .ppt-layer .ppt-box-container .ppt-box .ppt-fieldset {
            display: block;
            width: 100%;
            height: auto;
            position: relative;
            border-bottom: 1px solid rgba(0, 0, 0, .1);
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box
        }

        .ppt-layer .ppt-box-container .ppt-box .ppt-fieldset .ppt-placeholder {
            display: inline-block;
            font-family: inherit;
            font-size: 10px;
            font-weight: 700;
            color: inherit;
            text-transform: uppercase;
            position: absolute;
            top: 0;
            left: 15px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box
        }

        .ppt-layer .ppt-box-container .ppt-box .ppt-fieldset .ppt-input {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            padding: 15px;
            background: transparent;
            font-family: inherit;
            font-size: inherit;
            font-weight: inherit;
            color: inherit;
            border: 0;
            border-radius: 0;
            outline: 0;
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            appearance: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box
        }

        .ppt-layer .ppt-box-container .ppt-box .ppt-btns {
            display: block;
            float: right;
            padding: 0;
            margin: 0;
            list-style-type: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box
        }

        .ppt-layer .ppt-box-container .ppt-box .ppt-btns:after {
            display: block;
            content: "";
            font-size: 0;
            zoom: 1;
            clear: both
        }

        .ppt-layer .ppt-box-container .ppt-box .ppt-btns .ppt-btn {
            display: block;
            float: left;
            margin: 0;
            padding: 15px;
            font-family: inherit;
            font-size: inherit;
            font-weight: inherit;
            color: inherit;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box
        }

        .ppt-layer .ppt-box-container .ppt-box .ppt-btns .ppt-btn.ppt-btn-separator {
            padding: 15px 0
        }

        .ppt-layer .ppt-box-container .ppt-box .ppt-btns .ppt-btn.ppt-btn-agree,
        .ppt-layer .ppt-box-container .ppt-box .ppt-btns .ppt-btn.ppt-btn-disagree {
            cursor: pointer
        }

        .mfp-bg {
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1042;
            overflow: hidden;
            position: fixed;
            background: #0b0b0b;
            opacity: .8
        }

        .mfp-wrap {
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1043;
            position: fixed;
            outline: none!important;
            -webkit-backface-visibility: hidden
        }

        .mfp-container {
            text-align: center;
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            padding: 0 8px;
            box-sizing: border-box
        }

        .mfp-container:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle
        }

        .mfp-align-top .mfp-container:before {
            display: none
        }

        .mfp-content {
            position: relative;
            display: inline-block;
            vertical-align: middle;
            margin: 0 auto;
            text-align: left;
            z-index: 1045
        }

        .mfp-inline-holder .mfp-content,
        .mfp-ajax-holder .mfp-content {
            width: 100%;
            cursor: auto
        }

        .mfp-ajax-cur {
            cursor: progress
        }

        .mfp-zoom-out-cur,
        .mfp-zoom-out-cur .mfp-image-holder .mfp-close {
            cursor: -moz-zoom-out;
            cursor: -webkit-zoom-out;
            cursor: zoom-out
        }

        .mfp-zoom {
            cursor: pointer;
            cursor: -webkit-zoom-in;
            cursor: -moz-zoom-in;
            cursor: zoom-in
        }

        .mfp-auto-cursor .mfp-content {
            cursor: auto
        }

        .mfp-close,
        .mfp-arrow,
        .mfp-preloader,
        .mfp-counter {
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none
        }

        .mfp-loading.mfp-figure {
            display: none
        }

        .mfp-hide {
            display: none!important
        }

        .mfp-preloader {
            color: #CCC;
            position: absolute;
            top: 50%;
            width: auto;
            text-align: center;
            margin-top: -.8em;
            left: 8px;
            right: 8px;
            z-index: 1044
        }

        .mfp-preloader a {
            color: #CCC
        }

        .mfp-preloader a:hover {
            color: #FFF
        }

        .mfp-s-ready .mfp-preloader {
            display: none
        }

        .mfp-s-error .mfp-content {
            display: none
        }

        button.mfp-close,
        button.mfp-arrow {
            overflow: visible;
            cursor: pointer;
            background: transparent;
            border: 0;
            -webkit-appearance: none;
            display: block;
            outline: none;
            padding: 0;
            z-index: 1046;
            box-shadow: none;
            touch-action: manipulation
        }

        button::-moz-focus-inner {
            padding: 0;
            border: 0
        }

        .mfp-close {
            width: 44px;
            height: 44px;
            line-height: 44px;
            position: absolute;
            right: 0;
            top: 0;
            text-decoration: none;
            text-align: center;
            opacity: .65;
            padding: 0 0 18px 10px;
            color: #FFF;
            font-style: normal;
            font-size: 28px;
            font-family: Arial, Baskerville, monospace
        }

        .mfp-close:hover,
        .mfp-close:focus {
            opacity: 1
        }

        .mfp-close:active {
            top: 1px
        }

        .mfp-close-btn-in .mfp-close {
            color: #333
        }

        .mfp-image-holder .mfp-close,
        .mfp-iframe-holder .mfp-close {
            color: #FFF;
            right: -6px;
            text-align: right;
            padding-right: 6px;
            width: 100%
        }

        .mfp-counter {
            position: absolute;
            top: 0;
            right: 0;
            color: #CCC;
            font-size: 12px;
            line-height: 18px;
            white-space: nowrap
        }

        .mfp-arrow {
            position: absolute;
            opacity: .65;
            margin: 0;
            top: 50%;
            margin-top: -55px;
            padding: 0;
            width: 90px;
            height: 110px;
            -webkit-tap-highlight-color: transparent
        }

        .mfp-arrow:active {
            margin-top: -54px
        }

        .mfp-arrow:hover,
        .mfp-arrow:focus {
            opacity: 1
        }

        .mfp-arrow:before,
        .mfp-arrow:after {
            content: '';
            display: block;
            width: 0;
            height: 0;
            position: absolute;
            left: 0;
            top: 0;
            margin-top: 35px;
            margin-left: 35px;
            border: medium inset transparent
        }

        .mfp-arrow:after {
            border-top-width: 13px;
            border-bottom-width: 13px;
            top: 8px
        }

        .mfp-arrow:before {
            border-top-width: 21px;
            border-bottom-width: 21px;
            opacity: .7
        }

        .mfp-arrow-left {
            left: 0
        }

        .mfp-arrow-left:after {
            border-right: 17px solid #FFF;
            margin-left: 31px
        }

        .mfp-arrow-left:before {
            margin-left: 25px;
            border-right: 27px solid #3F3F3F
        }

        .mfp-arrow-right {
            right: 0
        }

        .mfp-arrow-right:after {
            border-left: 17px solid #FFF;
            margin-left: 39px
        }

        .mfp-arrow-right:before {
            border-left: 27px solid #3F3F3F
        }

        .mfp-iframe-holder {
            padding-top: 40px;
            padding-bottom: 40px
        }

        .mfp-iframe-holder .mfp-content {
            line-height: 0;
            width: 100%;
            max-width: 900px
        }

        .mfp-iframe-holder .mfp-close {
            top: -40px
        }

        .mfp-iframe-scaler {
            width: 100%;
            height: 0;
            overflow: hidden;
            padding-top: 56.25%
        }

        .mfp-iframe-scaler iframe {
            position: absolute;
            display: block;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            box-shadow: 0 0 8px rgba(0, 0, 0, .6);
            background: #000
        }

        img.mfp-img {
            width: auto;
            max-width: 100%;
            height: auto;
            display: block;
            line-height: 0;
            box-sizing: border-box;
            padding: 40px 0 40px;
            margin: 0 auto
        }

        .mfp-figure {
            line-height: 0
        }

        .mfp-figure:after {
            content: '';
            position: absolute;
            left: 0;
            top: 40px;
            bottom: 40px;
            display: block;
            right: 0;
            width: auto;
            height: auto;
            z-index: -1;
            box-shadow: 0 0 8px rgba(0, 0, 0, .6);
            background: #444
        }

        .mfp-figure small {
            color: #BDBDBD;
            display: block;
            font-size: 12px;
            line-height: 14px
        }

        .mfp-figure figure {
            margin: 0
        }

        .mfp-bottom-bar {
            margin-top: -36px;
            position: absolute;
            top: 100%;
            left: 0;
            width: 100%;
            cursor: auto
        }

        .mfp-title {
            text-align: left;
            line-height: 18px;
            color: #F3F3F3;
            word-wrap: break-word;
            padding-right: 36px
        }

        .mfp-image-holder .mfp-content {
            max-width: 100%
        }

        .mfp-gallery .mfp-image-holder .mfp-figure {
            cursor: pointer
        }

        @media screen and (max-width:800px) and (orientation:landscape),
        screen and (max-height:300px) {
            .mfp-img-mobile .mfp-image-holder {
                padding-left: 0;
                padding-right: 0
            }
            .mfp-img-mobile img.mfp-img {
                padding: 0
            }
            .mfp-img-mobile .mfp-figure:after {
                top: 0;
                bottom: 0
            }
            .mfp-img-mobile .mfp-figure small {
                display: inline;
                margin-left: 5px
            }
            .mfp-img-mobile .mfp-bottom-bar {
                background: rgba(0, 0, 0, .6);
                bottom: 0;
                margin: 0;
                top: auto;
                padding: 3px 5px;
                position: fixed;
                box-sizing: border-box
            }
            .mfp-img-mobile .mfp-bottom-bar:empty {
                padding: 0
            }
            .mfp-img-mobile .mfp-counter {
                right: 5px;
                top: 3px
            }
            .mfp-img-mobile .mfp-close {
                top: 0;
                right: 0;
                width: 35px;
                height: 35px;
                line-height: 35px;
                background: rgba(0, 0, 0, .6);
                position: fixed;
                text-align: center;
                padding: 0
            }
        }

        @media all and (max-width:900px) {
            .mfp-arrow {
                -webkit-transform: scale(.75);
                transform: scale(.75)
            }
            .mfp-arrow-left {
                -webkit-transform-origin: 0;
                transform-origin: 0
            }
            .mfp-arrow-right {
                -webkit-transform-origin: 100%;
                transform-origin: 100%
            }
            .mfp-container {
                padding-left: 6px;
                padding-right: 6px
            }
        }

        .swiper-container {
            margin: 0 auto;
            position: relative;
            overflow: hidden;
            list-style: none;
            padding: 0;
            z-index: 1
        }

        .swiper-container-no-flexbox .swiper-slide {
            float: left
        }

        .swiper-container-vertical>.swiper-wrapper {
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column
        }

        .swiper-wrapper {
            position: relative;
            width: 100%;
            height: 100%;
            z-index: 1;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-transition-property: -webkit-transform;
            transition-property: -webkit-transform;
            -o-transition-property: transform;
            transition-property: transform;
            transition-property: transform, -webkit-transform;
            -webkit-box-sizing: content-box;
            box-sizing: content-box
        }

        .swiper-container-android .swiper-slide,
        .swiper-wrapper {
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0)
        }

        .swiper-container-multirow>.swiper-wrapper {
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap
        }

        .swiper-container-free-mode>.swiper-wrapper {
            -webkit-transition-timing-function: ease-out;
            -o-transition-timing-function: ease-out;
            transition-timing-function: ease-out;
            margin: 0 auto
        }

        .swiper-slide {
            -webkit-flex-shrink: 0;
            -ms-flex-negative: 0;
            flex-shrink: 0;
            width: 100%;
            height: 100%;
            position: relative;
            -webkit-transition-property: -webkit-transform;
            transition-property: -webkit-transform;
            -o-transition-property: transform;
            transition-property: transform;
            transition-property: transform, -webkit-transform
        }

        .swiper-invisible-blank-slide {
            visibility: hidden
        }

        .swiper-container-autoheight,
        .swiper-container-autoheight .swiper-slide {
            height: auto
        }

        .swiper-container-autoheight .swiper-wrapper {
            -webkit-box-align: start;
            -webkit-align-items: flex-start;
            -ms-flex-align: start;
            align-items: flex-start;
            -webkit-transition-property: height, -webkit-transform;
            transition-property: height, -webkit-transform;
            -o-transition-property: transform, height;
            transition-property: transform, height;
            transition-property: transform, height, -webkit-transform
        }

        .swiper-container-3d {
            -webkit-perspective: 1200px;
            perspective: 1200px
        }

        .swiper-container-3d .swiper-cube-shadow,
        .swiper-container-3d .swiper-slide,
        .swiper-container-3d .swiper-slide-shadow-bottom,
        .swiper-container-3d .swiper-slide-shadow-left,
        .swiper-container-3d .swiper-slide-shadow-right,
        .swiper-container-3d .swiper-slide-shadow-top,
        .swiper-container-3d .swiper-wrapper {
            -webkit-transform-style: preserve-3d;
            transform-style: preserve-3d
        }

        .swiper-container-3d .swiper-slide-shadow-bottom,
        .swiper-container-3d .swiper-slide-shadow-left,
        .swiper-container-3d .swiper-slide-shadow-right,
        .swiper-container-3d .swiper-slide-shadow-top {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            pointer-events: none;
            z-index: 10
        }

        .swiper-container-3d .swiper-slide-shadow-left {
            background-image: -webkit-gradient(linear, right top, left top, from(rgba(0, 0, 0, .5)), to(rgba(0, 0, 0, 0)));
            background-image: -webkit-linear-gradient(right, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0));
            background-image: -o-linear-gradient(right, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0));
            background-image: linear-gradient(to left, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0))
        }

        .swiper-container-3d .swiper-slide-shadow-right {
            background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, .5)), to(rgba(0, 0, 0, 0)));
            background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0));
            background-image: -o-linear-gradient(left, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0));
            background-image: linear-gradient(to right, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0))
        }

        .swiper-container-3d .swiper-slide-shadow-top {
            background-image: -webkit-gradient(linear, left bottom, left top, from(rgba(0, 0, 0, .5)), to(rgba(0, 0, 0, 0)));
            background-image: -webkit-linear-gradient(bottom, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0));
            background-image: -o-linear-gradient(bottom, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0));
            background-image: linear-gradient(to top, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0))
        }

        .swiper-container-3d .swiper-slide-shadow-bottom {
            background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, .5)), to(rgba(0, 0, 0, 0)));
            background-image: -webkit-linear-gradient(top, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0));
            background-image: -o-linear-gradient(top, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0));
            background-image: linear-gradient(to bottom, rgba(0, 0, 0, .5), rgba(0, 0, 0, 0))
        }

        .swiper-container-wp8-horizontal,
        .swiper-container-wp8-horizontal>.swiper-wrapper {
            -ms-touch-action: pan-y;
            touch-action: pan-y
        }

        .swiper-container-wp8-vertical,
        .swiper-container-wp8-vertical>.swiper-wrapper {
            -ms-touch-action: pan-x;
            touch-action: pan-x
        }

        .swiper-button-next,
        .swiper-button-prev {
            position: absolute;
            top: 50%;
            width: 27px;
            height: 44px;
            margin-top: -22px;
            z-index: 10;
            cursor: pointer;
            background-size: 27px 44px;
            background-position: center;
            background-repeat: no-repeat
        }

        .swiper-button-next.swiper-button-disabled,
        .swiper-button-prev.swiper-button-disabled {
            opacity: .35;
            cursor: auto;
            pointer-events: none
        }

        .swiper-button-prev,
        .swiper-container-rtl .swiper-button-next {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E");
            left: 10px;
            right: auto
        }

        .swiper-button-next,
        .swiper-container-rtl .swiper-button-prev {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E");
            right: 10px;
            left: auto
        }

        .swiper-button-prev.swiper-button-white,
        .swiper-container-rtl .swiper-button-next.swiper-button-white {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E")
        }

        .swiper-button-next.swiper-button-white,
        .swiper-container-rtl .swiper-button-prev.swiper-button-white {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E")
        }

        .swiper-button-prev.swiper-button-black,
        .swiper-container-rtl .swiper-button-next.swiper-button-black {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E")
        }

        .swiper-button-next.swiper-button-black,
        .swiper-container-rtl .swiper-button-prev.swiper-button-black {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E")
        }

        .swiper-button-lock {
            display: none
        }

        .swiper-pagination {
            position: absolute;
            text-align: center;
            -webkit-transition: .3s opacity;
            -o-transition: .3s opacity;
            transition: .3s opacity;
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
            z-index: 10
        }

        .swiper-pagination.swiper-pagination-hidden {
            opacity: 0
        }

        .swiper-container-horizontal>.swiper-pagination-bullets,
        .swiper-pagination-custom,
        .swiper-pagination-fraction {
            bottom: 10px;
            left: 0;
            width: 100%
        }

        .swiper-pagination-bullets-dynamic {
            overflow: hidden;
            font-size: 0
        }

        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet {
            -webkit-transform: scale(.33);
            -ms-transform: scale(.33);
            transform: scale(.33);
            position: relative
        }

        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active {
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1)
        }

        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-main {
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1)
        }

        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev {
            -webkit-transform: scale(.66);
            -ms-transform: scale(.66);
            transform: scale(.66)
        }

        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev-prev {
            -webkit-transform: scale(.33);
            -ms-transform: scale(.33);
            transform: scale(.33)
        }

        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next {
            -webkit-transform: scale(.66);
            -ms-transform: scale(.66);
            transform: scale(.66)
        }

        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next-next {
            -webkit-transform: scale(.33);
            -ms-transform: scale(.33);
            transform: scale(.33)
        }

        .swiper-pagination-bullet {
            width: 8px;
            height: 8px;
            display: inline-block;
            border-radius: 100%;
            background: #000;
            opacity: .2
        }

        button.swiper-pagination-bullet {
            border: none;
            margin: 0;
            padding: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none
        }

        .swiper-pagination-clickable .swiper-pagination-bullet {
            cursor: pointer
        }

        .swiper-pagination-bullet-active {
            opacity: 1;
            background: #007aff
        }

        .swiper-container-vertical>.swiper-pagination-bullets {
            right: 10px;
            top: 50%;
            -webkit-transform: translate3d(0, -50%, 0);
            transform: translate3d(0, -50%, 0)
        }

        .swiper-container-vertical>.swiper-pagination-bullets .swiper-pagination-bullet {
            margin: 6px 0;
            display: block
        }

        .swiper-container-vertical>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic {
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            width: 8px
        }

        .swiper-container-vertical>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet {
            display: inline-block;
            -webkit-transition: .2s top, .2s -webkit-transform;
            transition: .2s top, .2s -webkit-transform;
            -o-transition: .2s transform, .2s top;
            transition: .2s transform, .2s top;
            transition: .2s transform, .2s top, .2s -webkit-transform
        }

        .swiper-container-horizontal>.swiper-pagination-bullets .swiper-pagination-bullet {
            margin: 0 4px
        }

        .swiper-container-horizontal>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic {
            left: 50%;
            -webkit-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            transform: translateX(-50%);
            white-space: nowrap
        }

        .swiper-container-horizontal>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet {
            -webkit-transition: .2s left, .2s -webkit-transform;
            transition: .2s left, .2s -webkit-transform;
            -o-transition: .2s transform, .2s left;
            transition: .2s transform, .2s left;
            transition: .2s transform, .2s left, .2s -webkit-transform
        }

        .swiper-container-horizontal.swiper-container-rtl>.swiper-pagination-bullets-dynamic .swiper-pagination-bullet {
            -webkit-transition: .2s right, .2s -webkit-transform;
            transition: .2s right, .2s -webkit-transform;
            -o-transition: .2s transform, .2s right;
            transition: .2s transform, .2s right;
            transition: .2s transform, .2s right, .2s -webkit-transform
        }

        .swiper-pagination-progressbar {
            background: rgba(0, 0, 0, .25);
            position: absolute
        }

        .swiper-pagination-progressbar .swiper-pagination-progressbar-fill {
            background: #007aff;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            -webkit-transform: scale(0);
            -ms-transform: scale(0);
            transform: scale(0);
            -webkit-transform-origin: left top;
            -ms-transform-origin: left top;
            transform-origin: left top
        }

        .swiper-container-rtl .swiper-pagination-progressbar .swiper-pagination-progressbar-fill {
            -webkit-transform-origin: right top;
            -ms-transform-origin: right top;
            transform-origin: right top
        }

        .swiper-container-horizontal>.swiper-pagination-progressbar {
            width: 100%;
            height: 4px;
            left: 0;
            top: 0
        }

        .swiper-container-vertical>.swiper-pagination-progressbar {
            width: 4px;
            height: 100%;
            left: 0;
            top: 0
        }

        .swiper-pagination-white .swiper-pagination-bullet-active {
            background: #fff
        }

        .swiper-pagination-progressbar.swiper-pagination-white {
            background: rgba(255, 255, 255, .25)
        }

        .swiper-pagination-progressbar.swiper-pagination-white .swiper-pagination-progressbar-fill {
            background: #fff
        }

        .swiper-pagination-black .swiper-pagination-bullet-active {
            background: #000
        }

        .swiper-pagination-progressbar.swiper-pagination-black {
            background: rgba(0, 0, 0, .25)
        }

        .swiper-pagination-progressbar.swiper-pagination-black .swiper-pagination-progressbar-fill {
            background: #000
        }

        .swiper-pagination-lock {
            display: none
        }

        .swiper-scrollbar {
            border-radius: 10px;
            position: relative;
            -ms-touch-action: none;
            background: rgba(0, 0, 0, .1)
        }

        .swiper-container-horizontal>.swiper-scrollbar {
            position: absolute;
            left: 1%;
            bottom: 3px;
            z-index: 50;
            height: 5px;
            width: 98%
        }

        .swiper-container-vertical>.swiper-scrollbar {
            position: absolute;
            right: 3px;
            top: 1%;
            z-index: 50;
            width: 5px;
            height: 98%
        }

        .swiper-scrollbar-drag {
            height: 100%;
            width: 100%;
            position: relative;
            background: rgba(0, 0, 0, .5);
            border-radius: 10px;
            left: 0;
            top: 0
        }

        .swiper-scrollbar-cursor-drag {
            cursor: move
        }

        .swiper-scrollbar-lock {
            display: none
        }

        .swiper-zoom-container {
            width: 100%;
            height: 100%;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            text-align: center
        }

        .swiper-zoom-container>canvas,
        .swiper-zoom-container>img,
        .swiper-zoom-container>svg {
            max-width: 100%;
            max-height: 100%;
            -o-object-fit: contain;
            object-fit: contain
        }

        .swiper-slide-zoomed {
            cursor: move
        }

        .swiper-lazy-preloader {
            width: 42px;
            height: 42px;
            position: absolute;
            left: 50%;
            top: 50%;
            margin-left: -21px;
            margin-top: -21px;
            z-index: 10;
            -webkit-transform-origin: 50%;
            -ms-transform-origin: 50%;
            transform-origin: 50%;
            -webkit-animation: swiper-preloader-spin 1s steps(12, end) infinite;
            animation: swiper-preloader-spin 1s steps(12, end) infinite
        }

        .swiper-lazy-preloader:after {
            display: block;
            content: '';
            width: 100%;
            height: 100%;
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%236c6c6c'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E");
            background-position: 50%;
            background-size: 100%;
            background-repeat: no-repeat
        }

        .swiper-lazy-preloader-white:after {
            background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%23fff'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E")
        }

        @-webkit-keyframes swiper-preloader-spin {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }

        @keyframes swiper-preloader-spin {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }

        .swiper-container .swiper-notification {
            position: absolute;
            left: 0;
            top: 0;
            pointer-events: none;
            opacity: 0;
            z-index: -1000
        }

        .swiper-container-fade.swiper-container-free-mode .swiper-slide {
            -webkit-transition-timing-function: ease-out;
            -o-transition-timing-function: ease-out;
            transition-timing-function: ease-out
        }

        .swiper-container-fade .swiper-slide {
            pointer-events: none;
            -webkit-transition-property: opacity;
            -o-transition-property: opacity;
            transition-property: opacity
        }

        .swiper-container-fade .swiper-slide .swiper-slide {
            pointer-events: none
        }

        .swiper-container-fade .swiper-slide-active,
        .swiper-container-fade .swiper-slide-active .swiper-slide-active {
            pointer-events: auto
        }

        .swiper-container-cube {
            overflow: visible
        }

        .swiper-container-cube .swiper-slide {
            pointer-events: none;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            z-index: 1;
            visibility: hidden;
            -webkit-transform-origin: 0 0;
            -ms-transform-origin: 0 0;
            transform-origin: 0 0;
            width: 100%;
            height: 100%
        }

        .swiper-container-cube .swiper-slide .swiper-slide {
            pointer-events: none
        }

        .swiper-container-cube.swiper-container-rtl .swiper-slide {
            -webkit-transform-origin: 100% 0;
            -ms-transform-origin: 100% 0;
            transform-origin: 100% 0
        }

        .swiper-container-cube .swiper-slide-active,
        .swiper-container-cube .swiper-slide-active .swiper-slide-active {
            pointer-events: auto
        }

        .swiper-container-cube .swiper-slide-active,
        .swiper-container-cube .swiper-slide-next,
        .swiper-container-cube .swiper-slide-next + .swiper-slide,
        .swiper-container-cube .swiper-slide-prev {
            pointer-events: auto;
            visibility: visible
        }

        .swiper-container-cube .swiper-slide-shadow-bottom,
        .swiper-container-cube .swiper-slide-shadow-left,
        .swiper-container-cube .swiper-slide-shadow-right,
        .swiper-container-cube .swiper-slide-shadow-top {
            z-index: 0;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        .swiper-container-cube .swiper-cube-shadow {
            position: absolute;
            left: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            background: #000;
            opacity: .6;
            -webkit-filter: blur(50px);
            filter: blur(50px);
            z-index: 0
        }

        .swiper-container-flip {
            overflow: visible
        }

        .swiper-container-flip .swiper-slide {
            pointer-events: none;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            z-index: 1
        }

        .swiper-container-flip .swiper-slide .swiper-slide {
            pointer-events: none
        }

        .swiper-container-flip .swiper-slide-active,
        .swiper-container-flip .swiper-slide-active .swiper-slide-active {
            pointer-events: auto
        }

        .swiper-container-flip .swiper-slide-shadow-bottom,
        .swiper-container-flip .swiper-slide-shadow-left,
        .swiper-container-flip .swiper-slide-shadow-right,
        .swiper-container-flip .swiper-slide-shadow-top {
            z-index: 0;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        .swiper-container-coverflow .swiper-wrapper {
            -ms-perspective: 1200px
        }

        @-webkit-keyframes beat {
            50% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
            100% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
        }

        @-moz-keyframes beat {
            50% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
            100% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
        }

        @-ms-keyframes beat {
            50% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
            100% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
        }

        @-o-keyframes beat {
            50% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
            100% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
        }

        @keyframes beat {
            50% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
            100% {
                opacity: 0;
                -webkit-transform: scale(1.4, 1.4);
                -moz-transform: scale(1.4, 1.4);
                -ms-transform: scale(1.4, 1.4);
                -o-transform: scale(1.4, 1.4);
                transform: scale(1.4, 1.4)
            }
        }

        html {
            height: 100%
        }

        body {
            width: 100%;
            height: 100%;
            font-family: "Roboto", sans-serif;
            font-weight: 300;
            font-size: 16px;
            color: #000;
            padding-top: 100px
        }

        body .preloading {
            display: flex;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999999999;
            background: #fff;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body .preloading.preloading--hidden {
            opacity: 0
        }

        body .preloading .center {
            display: inline-block;
            margin: auto;
            text-align: center;
            position: relative
        }

        body .preloading .center .beats {
            position: absolute;
            top: 0;
            left: 0;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
            -webkit-animation: beat 1.5s infinite;
            -moz-animation: beat 1.5s infinite;
            -ms-animation: beat 1.5s infinite;
            -o-animation: beat 1.5s infinite;
            animation: beat 1.5s infinite
        }

        body .container {
            display: block;
            width: 100%;
            margin-bottom: 50px !important;
            max-width: 80%;
            height: 100%;
            margin: 0 auto;
            position: relative
        }

        body .container.container--flex {
            display: flex
        }

        body a {
            color: inherit;
            text-decoration: none
        }

        body .section {
            display: block;
            width: 100%;
            height: auto;
            position: relative
        }

        body .navbar {
            display: block;
            width: 100%;
            height: 100px;
            background: #fff;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999
        }

        @media (max-width:960px) {
            body .navbar {
                display: none
            }
        }

        @supports (-webkit-backdrop-filter:blur(15px)) or (backdrop-filter:blur(15px)) {
            body .navbar {
                background: rgba(255, 255, 255, .85);
                -webkit-backdrop-filter: blur(15px);
                -moz-backdrop-filter: blur(15px);
                -ms-backdrop-filter: blur(15px);
                -o-backdrop-filter: blur(15px);
                backdrop-filter: blur(15px)
            }
        }

        body .navbar .container--flex {
            align-items: center
        }

        body .navbar .container--flex .navbar__link {
            display: block;
            margin: 0;
            margin-left: 5px;
            padding: 0 10px;
            position: relative;
            overflow: hidden
        }

        body .navbar .container--flex .navbar__link:first-of-type {
            margin-left: 0;
            margin-right: auto;
            padding: 0
        }

        body .navbar .container--flex .navbar__link:first-of-type .logo {
            display: block;
            height: 100%;
            max-height: 75px;
            margin: 0;
            padding: 5px 0
        }

        body .navbar .container--flex .navbar__link .navbar__link__label {
            display: block;
            position: relative;
            font-family: "Montserrat", sans-serif;
            font-size: 15px;
            text-transform: uppercase;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body .navbar .container--flex .navbar__link .navbar__link__label__hidden {
            display: block;
            width: 100%;
            position: absolute;
            top: 101%;
            left: 0;
            font-family: "Montserrat", sans-serif;
            font-size: 15px;
            font-weight: 900;
            color: #4bcaff;
            text-transform: uppercase;
            text-align: center;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body .navbar .container--flex .navbar__link:hover .navbar__link__label,
        body .navbar .container--flex .navbar__link.active .navbar__link__label {
            opacity: 0
        }

        body .navbar .container--flex .navbar__link:hover .navbar__link__label__hidden,
        body .navbar .container--flex .navbar__link.active .navbar__link__label__hidden {
            top: 0
        }

        body .navbar.navbar--mobile {
            display: none
        }

        @media (max-width:960px) {
            body .navbar.navbar--mobile {
                display: block
            }
        }

        body .navbar.navbar--mobile .container--flex .navbar__link {
            margin-right: auto
        }

        body .navbar.navbar--mobile .container--flex .navbar__toggle {
            display: block;
            width: 35px;
            height: auto;
            position: relative;
            cursor: pointer
        }

        body .navbar.navbar--mobile .container--flex .navbar__toggle:hover .bar:nth-child(1),
        body .navbar.navbar--mobile .container--flex .navbar__toggle:hover .bar:nth-child(3),
        body .navbar.navbar--mobile .container--flex .navbar__toggle.clicked .bar:nth-child(1),
        body .navbar.navbar--mobile .container--flex .navbar__toggle.clicked .bar:nth-child(3) {
            width: 100%
        }

        body .navbar.navbar--mobile .container--flex .navbar__toggle .bar {
            display: block;
            width: 100%;
            height: 2px;
            background: #000;
            margin: 0;
            margin-left: auto;
            margin-bottom: 6px;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body .navbar.navbar--mobile .container--flex .navbar__toggle .bar:nth-child(1) {
            width: 75%
        }

        body .navbar.navbar--mobile .container--flex .navbar__toggle .bar:nth-child(3) {
            width: 50%;
            margin-bottom: 0
        }

        body .navbar.navbar--mobile .navbar__links {
            display: none;
            background: #fff
        }

        @supports (-webkit-backdrop-filter:blur(0)) or (backdrop-filter:blur(0)) {
            body .navbar.navbar--mobile .navbar__links {
                background: rgba(255, 255, 255, .85);
                -webkit-backdrop-filter: blur(15px);
                -moz-backdrop-filter: blur(15px);
                -ms-backdrop-filter: blur(15px);
                -o-backdrop-filter: blur(15px);
                backdrop-filter: blur(15px)
            }
        }

        body .navbar.navbar--mobile .navbar__links .navbar__link {
            display: block;
            position: relative;
            font-family: "Montserrat", sans-serif;
            font-size: 15px;
            text-transform: uppercase;
            padding: 15px 10%;
            border-bottom: 1px solid rgba(0, 0, 0, .1);
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body .section--hero {
            height: 500px
        }

        body .section--hero .section__background {
            display: block;
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0
        }

        body .section--hero .container--flex {
            justify-content: flex-end
        }

        body .section--hero .container--flex .section__content {
            display: block;
            margin: auto 0;
            color: #fff;
            font-family: "Montserrat", sans-serif
        }

        @media (max-width:480px) {
            body .section--hero .container--flex .section__content {
                width: 100%
            }
        }

        body .section--hero .container--flex .section__content .section__content__title {
            display: block;
            margin: 0;
            margin-bottom: 25px;
            text-transform: uppercase;
            font-size: 42px;
            font-weight: inherit
        }

        @media (max-width:480px) {
            body .section--hero .container--flex .section__content .section__content__title {
                font-size: 26px
            }
        }

        body .section--hero .container--flex .section__content .section__content__title .line {
            display: block
        }

        body .section--hero .container--flex .section__content .section__content__title .bolder {
            font-weight: 700
        }

        body .section--hero .container--flex .section__content .section__content__paragraph {
            display: block;
            margin: 0;
            margin-bottom: 45px
        }

        body .section--hero .container--flex .section__content .cta {
            margin: 0;
            padding: 10px 25px;
            background: #4bcaff;
            color: #fff;
            font-family: "Montserrat", sans-serif;
            text-transform: uppercase;
            font-weight: 700;
            font-size: 14px;
            letter-spacing: 2px;
            border-radius: 3px;
            border-bottom: 5px solid #00b3fe;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body .section--hero .container--flex .section__content .cta:hover {
            background: #00b3fe;
            border-bottom: 5px solid #007db1
        }

        body .section--contact {
            padding: 75px 0
        }

        body .section--contact .container .section__title {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            margin-bottom: 25px;
            padding: 0;
            padding-bottom: 25px;
            text-align: center;
            position: relative;
            text-transform: uppercase;
            font-family: "Montserrat", sans-serif
        }

        body .section--contact .container .section__title:after {
            display: block;
            content: "";
            width: 50px;
            height: 5px;
            background: #4bcaff;
            position: absolute;
            top: auto;
            left: 50%;
            bottom: 0;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body .section--contact .container .section__paragraph {
            display: block;
            width: 100%;
            height: auto;
            text-align: center;
            margin: 0;
            margin-bottom: 15px;
            line-height: 1.5
        }

        body .section--contact .container .section__paragraph:last-of-type {
            margin-bottom: 75px
        }

        body .section--contact .container .section__paragraph .line {
            display: block
        }

        body .section--contact .container .contact-form {
            display: flex;
            width: 100%;
            height: auto;
            margin: 0;
            padding: 0;
            flex-flow: row wrap
        }

        body .section--contact .container .contact-form .contact-form__left,
        body .section--contact .container .contact-form .contact-form__right {
            display: block;
            width: 100%;
            height: auto;
            padding-right: calc(75px / 2);
            flex: 1
        }

        body .section--contact .container .contact-form .contact-form__right {
            padding-right: 0;
            padding-left: calc(75px / 2)
        }

        @media (max-width:960px) {
            body .section--contact .container .contact-form .contact-form__left,
            body .section--contact .container .contact-form .contact-form__right {
                flex: 1 100%;
                padding: 0
            }
        }

        body .section--contact .container .contact-form input[type="text"],
        body .section--contact .container .contact-form textarea {
            display: block;
            width: 100%;
            height: auto;
            background: transparent;
            padding: 15px;
            border: 0;
            border-top: 1px solid #cdcdcd;
            border-radius: 0;
            outline: 0;
            font-family: inherit;
            font-weight: inherit;
            font-size: inherit;
            color: inherit
        }

        body .section--contact .container .contact-form input[type="text"]:last-of-type,
        body .section--contact .container .contact-form textarea:last-of-type {
            border-bottom: 1px solid #cdcdcd
        }

        body .section--contact .container .contact-form textarea {
            resize: none;
            line-height: 1.5;
            margin-bottom: 25px
        }

        body .section--contact .container .contact-form button[type="submit"] {
            display: block;
            margin-left: auto;
            background: transparent;
            padding: 15px 0;
            border: 0;
            border-radius: 0;
            outline: 0;
            font-family: "Montserrat", sans-serif;
            font-weight: 900;
            font-size: 14px;
            color: inherit;
            text-transform: uppercase;
            cursor: pointer
        }

        body .section--map iframe {
            display: block;
            width: 100%;
            height: 300px
        }

        body .footer {
            padding: 25px 0;
            background: #000;
            color: rgba(255, 255, 255, .5);
            font-size: 14px
        }

        body .footer .copyright a {
            text-decoration: underline
        }

        body.index-view .section--about {
            padding: 75px 0
        }

        body.index-view .section--about .container .section__title {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            margin-bottom: 25px;
            padding: 0;
            padding-bottom: 25px;
            text-align: center;
            position: relative;
            text-transform: uppercase;
            font-family: "Montserrat", sans-serif
        }

        body.index-view .section--about .container .section__title:after {
            display: block;
            content: "";
            width: 50px;
            height: 5px;
            background: #4bcaff;
            position: absolute;
            top: auto;
            left: 50%;
            bottom: 0;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body.index-view .section--about .container .section__paragraph {
            display: block;
            width: 100%;
            height: auto;
            text-align: center;
            margin: 0;
            margin-bottom: 15px;
            line-height: 1.5
        }

        body.index-view .section--about .container .section__paragraph:last-of-type {
            margin-bottom: 0
        }

        body.index-view .section--services {
            padding-bottom: 75px
        }

        body.index-view .section--services .container--flex {
            margin-bottom: 25px;
            justify-content: center;
            flex-flow: row wrap
        }

        body.index-view .section--services .container--flex .service {
            display: block;
            width: 100%;
            max-width: 320px;
            height: auto;
            text-align: center;
            margin: 0 25px;
            flex: 1
        }

        @media (max-width:700px) {
            body.index-view .section--services .container--flex .service {
                flex: 1 100%;
                margin: 0;
                margin-bottom: 25px
            }
            body.index-view .section--services .container--flex .service:last-of-type {
                margin-bottom: 0
            }
        }

        body.index-view .section--services .container--flex .service:hover .service__icon {
            background: #4bcaff;
            color: #fff
        }

        body.index-view .section--services .container--flex .service .service__icon {
            display: inline-block;
            padding: 25px;
            border-radius: 100%;
            background: #f3f3f3;
            font-size: 30px;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body.index-view .section--services .container--flex .service .service__title {
            display: block;
            text-transform: uppercase;
            font-family: "Montserrat", sans-serif;
            font-size: 14px;
            font-weight: 700
        }

        body.index-view .section--about-dr {
            padding-bottom: 75px
        }

        body.index-view .section--about-dr .container {
            text-align: center
        }

        body.index-view .section--about-dr .container .section__subtitle {
            margin-top: 0
        }

        body.index-view .section--about-dr .container .section__paragraph {
            display: block;
            line-height: 1.5;
            margin-bottom: 55px
        }

        body.index-view .section--about-dr .container .section__list {
            display: block;
            margin: 0;
            margin-bottom: 55px;
            padding: 0;
            list-style-type: none;
            text-align: left
        }

        body.index-view .section--about-dr .container .section__list .section__list__item {
            display: block;
            margin: 0;
            margin-bottom: 15px;
            font-size: 14px
        }

        body.index-view .section--about-dr .container .section__list .section__list__item .authors {
            white-space: nowrap;
            font-weight: 700
        }

        @media (max-width:600px) {
            body.index-view .section--about-dr .container .section__list .section__list__item .authors {
                white-space: pre-wrap
            }
        }

        body.index-view .section--about-dr .container .cta {
            margin: 0;
            padding: 10px 25px;
            background: #4bcaff;
            color: #fff;
            font-family: "Montserrat", sans-serif;
            text-transform: uppercase;
            font-weight: 700;
            font-size: 14px;
            letter-spacing: 2px;
            border-radius: 3px;
            border-bottom: 5px solid #00b3fe;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body.index-view .section--about-dr .container .cta:hover {
            background: #00b3fe;
            border-bottom: 5px solid #007db1
        }

        body.index-view .section--our-practices {
            padding: 75px 0;
            background: #f3f3f3
        }

        body.index-view .section--our-practices .container .section__title {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            margin-bottom: 25px;
            padding: 0;
            padding-bottom: 25px;
            text-align: center;
            position: relative;
            text-transform: uppercase;
            font-family: "Montserrat", sans-serif
        }

        body.index-view .section--our-practices .container .section__title:after {
            display: block;
            content: "";
            width: 50px;
            height: 5px;
            background: #4bcaff;
            position: absolute;
            top: auto;
            left: 50%;
            bottom: 0;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body.index-view .section--our-practices .container .section__subtitle {
            display: block;
            width: 100%;
            height: auto;
            text-align: center
        }

        body.index-view .section--our-practices .container .section__paragraph {
            display: block;
            width: 100%;
            height: auto;
            text-align: center;
            margin: 0;
            margin-bottom: 15px;
            line-height: 1.5
        }

        body.index-view .section--our-practices .container .section__paragraph:last-of-type {
            margin-bottom: 75px
        }

        body.index-view .section--our-practices .container .gallery {
            display: flex;
            width: 100%;
            height: auto;
            justify-content: center;
            flex-flow: row wrap
        }

        body.index-view .section--our-practices .container .gallery .gallery__item {
            display: block;
            width: 100%;
            max-width: 320px;
            height: 200px;
            margin: 0 25px;
            position: relative;
            top: 0;
            left: 0;
            overflow: hidden;
            flex: 1
        }

        @media (max-width:700px) {
            body.index-view .section--our-practices .container .gallery .gallery__item {
                flex: 1 100%;
                margin: 0;
                margin-bottom: 25px
            }
            body.index-view .section--our-practices .container .gallery .gallery__item:last-of-type {
                margin-bottom: 0
            }
        }

        body.index-view .section--our-practices .container .gallery .gallery__item:hover .gallery__item__hover {
            top: 0
        }

        body.index-view .section--our-practices .container .gallery .gallery__item .gallery__item__background {
            display: block;
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-radius: 3px
        }

        body.index-view .section--our-practices .container .gallery .gallery__item .gallery__item__background .gallery__item__background__thumbnail {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            padding: 0;
            opacity: 0
        }

        body.index-view .section--our-practices .container .gallery .gallery__item .gallery__item__hover {
            display: flex;
            width: 100%;
            height: 100%;
            position: absolute;
            top: 100%;
            left: 0;
            background: rgba(75, 202, 255, .85);
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
            border-radius: 3px
        }

        @supports (-webkit-backdrop-filter:blur(10px)) or (backdrop-filter:blur(10px)) {
            body.index-view .section--our-practices .container .gallery .gallery__item .gallery__item__hover {
                background: rgba(75, 202, 255, .75);
                -webkit-backdrop-filter: blur(10px);
                -moz-backdrop-filter: blur(10px);
                -ms-backdrop-filter: blur(10px);
                -o-backdrop-filter: blur(10px);
                backdrop-filter: blur(10px)
            }
        }

        body.index-view .section--our-practices .container .gallery .gallery__item .gallery__item__hover .gallery__item__hover__label {
            display: inline-block;
            margin: auto;
            pointer-events: none;
            font-family: "Montserrat", sans-serif;
            font-weight: 700;
            font-size: 12px;
            text-transform: uppercase;
            color: #fff
        }

        body.index-view .section--tips {
            padding: 75px 0
        }

        body.index-view .section--tips .section__title {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            margin-bottom: 75px;
            padding: 0;
            padding-bottom: 25px;
            text-align: center;
            position: relative;
            text-transform: uppercase;
            font-family: "Montserrat", sans-serif
        }

        body.index-view .section--tips .section__title:after {
            display: block;
            content: "";
            width: 50px;
            height: 5px;
            background: #4bcaff;
            position: absolute;
            top: auto;
            left: 50%;
            bottom: 0;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body.index-view .section--tips .tips {
            flex-flow: wrap;
            justify-content: center
        }

        body.index-view .section--tips .tips .tip {
            display: block;
            width: 175px;
            height: auto;
            margin: 0;
            margin-right: 15px;
            margin-left: 15px
        }

        @media (max-width:530px) {
            body.index-view .section--tips .tips .tip {
                flex: 1 100%;
                margin: 0;
                margin-bottom: 15px
            }
            body.index-view .section--tips .tips .tip:last-of-type {
                margin-bottom: 0
            }
        }

        body.index-view .section--tips .tips .tip .tip__link {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            padding: 0;
            color: inherit
        }

        body.index-view .section--tips .tips .tip .tip__link:hover .tip__thumb,
        body.index-view .section--tips .tips .tip .tip__link:hover .tip__name {
            border-left: 5px solid #4bcaff
        }

        body.index-view .section--tips .tips .tip .tip__link .tip__thumb {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            padding: 0;
            border-left: 5px solid #f3f3f3;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body.index-view .section--tips .tips .tip .tip__link .tip__name {
            display: block;
            font-weight: 400;
            margin: 0;
            padding: 0;
            padding-bottom: 15px;
            padding-left: 15px;
            border-left: 5px solid #f3f3f3;
            font-family: "Montserrat", sans-serif;
            text-transform: uppercase;
            font-size: 12px;
            font-weight: 700;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body.index-view .section--video {
            padding: 75px 0;
            text-align: center
        }

        body.index-view .section--video:before {
            display: block;
            content: "";
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background: rgba(75, 202, 255, .5)
        }

        body.index-view .section--video .container .section__title {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            margin-bottom: 25px;
            padding: 0;
            padding-bottom: 25px;
            text-align: center;
            position: relative;
            text-transform: uppercase;
            font-family: "Montserrat", sans-serif;
            color: #fff
        }

        body.index-view .section--video .container .section__title:after {
            display: block;
            content: "";
            width: 50px;
            height: 5px;
            background: #4bcaff;
            position: absolute;
            top: auto;
            left: 50%;
            bottom: 0;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body.index-view .section--video .container .video-slider {
            display: block;
            width: 560px;
            height: 315px;
            margin: 0 auto;
            margin-bottom: 25px
        }

        @media (max-width:700px) {
            body.index-view .section--video .container .video-slider {
                width: 100%
            }
        }

        body.index-view .section--video .container .video-slider iframe {
            display: block;
            width: 100%;
            height: 100%;
            border: 0;
            padding: 0;
            margin: 0;
            outline: 0
        }

        body.index-view .section--video .container .video-slider-control {
            display: flex;
            width: 50px;
            height: 50px;
            position: absolute;
            top: 50%;
            left: 0;
            -webkit-transform: translateY(-50%);
            -moz-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            -o-transform: translateY(-50%);
            transform: translateY(-50%);
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
            cursor: pointer;
            color: #fff;
            border-radius: 100%;
            border: 1px solid #fff
        }

        @media (max-width:700px) {
            body.index-view .section--video .container .video-slider-control {
                top: 100%;
                -webkit-transform: translateY(0);
                -moz-transform: translateY(0);
                -ms-transform: translateY(0);
                -o-transform: translateY(0);
                transform: translateY(0)
            }
        }

        body.index-view .section--video .container .video-slider-control.swiper-button-disabled {
            border: 1px solid rgba(255, 255, 255, .25);
            color: rgba(255, 255, 255, .25);
            cursor: default
        }

        body.index-view .section--video .container .video-slider-control:not(.swiper-button-disabled):hover {
            background: #fff;
            color: #4bcaff
        }

        body.index-view .section--video .container .video-slider-control.next-video {
            left: auto;
            right: 0
        }

        body.index-view .section--video .container .video-slider-control svg {
            display: inline-block;
            margin: auto
        }

        body.index-view .section--video .container .section__paragraph {
            display: block;
            width: 100%;
            height: auto;
            text-align: center;
            margin: 0;
            margin-bottom: 15px;
            line-height: 1.5;
            color: #fff
        }

        body.index-view .section--video .container .section__paragraph:last-of-type {
            margin-bottom: 0
        }

        body.index-view .section--our-courses {
            padding: 75px 0;
            background: #f3f3f3
        }

        body.index-view .section--our-courses .container {
            text-align: center
        }

        body.index-view .section--our-courses .container .section__title {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            margin-bottom: 25px;
            padding: 0;
            padding-bottom: 25px;
            text-align: center;
            position: relative;
            text-transform: uppercase;
            font-family: "Montserrat", sans-serif
        }

        body.index-view .section--our-courses .container .section__title:after {
            display: block;
            content: "";
            width: 50px;
            height: 5px;
            background: #4bcaff;
            position: absolute;
            top: auto;
            left: 50%;
            bottom: 0;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body.index-view .section--our-courses .container .section__subtitle {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            margin-bottom: 35px
        }

        body.index-view .section--our-courses .container .cta {
            margin: 0;
            padding: 10px 25px;
            background: #4bcaff;
            color: #fff;
            font-family: "Montserrat", sans-serif;
            text-transform: uppercase;
            font-weight: 700;
            font-size: 14px;
            letter-spacing: 2px;
            border-radius: 3px;
            border-bottom: 5px solid #00b3fe;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease
        }

        body.index-view .section--our-courses .container .cta:hover {
            background: #00b3fe;
            border-bottom: 5px solid #007db1
        }

        body.courses-view .section--courses {
            padding: 75px 0;
            background: #f3f3f3
        }

        body.courses-view .section--courses .container.course {
            margin-bottom: 75px;
            padding-bottom: 75px;
            border-bottom: 1px solid #cdcdcd
        }

        body.courses-view .section--courses .container.course:last-of-type {
            border-bottom: 0;
            margin-bottom: 0;
            padding-bottom: 0
        }

        body.courses-view .section--courses .container.course .section__title {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            margin-bottom: 25px;
            padding: 0;
            padding-bottom: 25px;
            position: relative;
            text-transform: uppercase;
            font-family: "Montserrat", sans-serif
        }

        body.courses-view .section--courses .container.course .section__title:after {
            display: block;
            content: "";
            width: 50px;
            height: 5px;
            background: #4bcaff;
            position: absolute;
            top: auto;
            left: 0;
            bottom: 0
        }

        body.courses-view .section--courses .container.course .section__paragraph {
            display: block;
            width: 100%;
            height: auto;
            margin: 0;
            margin-bottom: 15px;
            line-height: 1.5
        }

        body.courses-view .section--courses .container.course .section__list {
            display: block;
            width: 100%;
            height: auto;
            list-style-type: none;
            margin: 0;
            margin-bottom: 15px;
            padding: 0;
            padding-left: 15px
        }

        body.courses-view .section--courses .container.course .section__list .section__list__item {
            display: block;
            font-style: italic;
            line-height: 1.5
        }

        body.courses-view .section--courses .container.course .course-info {
            display: flex;
            width: 100%;
            height: auto;
            line-height: 1.5;
            margin-bottom: 25px
        }

        body.courses-view .section--courses .container.course .course-info:last-of-type {
            margin-bottom: 0
        }

        body.courses-view .section--courses .container.course .course-info .course-info__topic {
            display: block;
            white-space: nowrap;
            margin: 0
        }

        body.courses-view .section--courses .container.course .course-info .course-info__description {
            display: block;
            width: 100%;
            max-width: 85%;
            margin: 0;
            margin-left: auto
        }
    </style>
</head>

<body class="index-view">
{{--    <div class="preloading">--}}
{{--        <div class="center"> <img src="/storage/{{ setting('site.logo') }}" alt="ATERPO" class="beats"> <img src="/storage/{{ setting('site.logo') }}" alt="ATERPO" class="logo"> </div>--}}
{{--    </div>--}}
    <nav class="navbar">
        <div class="container container--flex">
            <a href="/" class="navbar__link"><img src="/storage/{{ setting('site.logo') }}" alt="ATERPO" class="logo"></a>
            {{ menu('site', 'layout.menu-site') }}
        </div>
    </nav>
    <nav class="navbar navbar--mobile">
        <div class="container container--flex">
            <a href="/" class="navbar__link"><img src="/assets/img/logo.png" alt="ATERPO" class="logo"></a>
            <div class="navbar__toggle" js-handler="navbarToggle"> <span class="bar"></span> <span class="bar"></span> <span class="bar"></span> </div>
        </div>
        <div class="navbar__links" js-handler="navbarLinks">
             {{ menu('site', 'layout.menu-site-mobile') }}
            </div>
    </nav>
    @yield('content')
    @include('shared.contacts')

    <footer class="footer">
        <div class="container">
            <p class="copyright">Desenvolvido com <i class="fas fa-heart"></i> por <a href="http://www.4movproducao.com.br/" target="blank">4Mov Produções</a> e  <a href="http://www.developbacklog.com.br/" target="blank">Develop Backlog</a>. &copy; 2020 Todos os direitos reservados.</p>
        </div>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
     @yield('js')
    <script>
        function confirmit(object, callback) {
            var agreeLabel = $.trim(object.agreeLabel) || 'Agree';
            var disagreeLabel = $.trim(object.disagreeLabel) || 'Disagree';
            var $cfmLayer = $('<div class="cfm-layer"></div>');
            var $cfmBoxContainer = $('<div class="cfm-box-container"></div>');
            var $cfmBox = $('<div class="cfm-box"></div>');
            var $cfmMessage = $('<span class="cfm-message">' + $.trim(object.message) + '</span>');
            var $cfmBtns = $('<ul class="cfm-btns"></ul>');
            var $cfmBtnAgree = $('<li class="cfm-btn cfm-btn-agree">' + agreeLabel + '</li>');
            var $cfmBtnBullet = $('<li class="cfm-btn cfm-btn-separator">&bull;</li>');
            var $cfmBtnDisagree = $('<li class="cfm-btn cfm-btn-disagree">' + disagreeLabel + '</li>');
            $cfmBtns.append($cfmBtnAgree).append($cfmBtnBullet).append($cfmBtnDisagree);
            $cfmBox.append($cfmMessage).append($cfmBtns);
            $cfmBoxContainer.append($cfmBox);
            $cfmLayer.append($cfmBoxContainer);
            $('body').append($cfmLayer);
            setTimeout(function() {
                $cfmLayer.addClass('cfm-visible');
                $cfmBoxContainer.addClass('cfm-visible')
            }, 100);
            $cfmBtnAgree.on('click', function(e) {
                if (typeof callback === 'function') {
                    $cfmLayer.removeClass('cfm-visible');
                    $cfmBoxContainer.removeClass('cfm-visible');
                    setTimeout(function() {
                        $cfmLayer.remove();
                        callback(!0)
                    }, 350)
                }
            });
            $cfmBtnDisagree.on('click', function(e) {
                if (typeof callback === 'function') {
                    $cfmLayer.removeClass('cfm-visible');
                    $cfmBoxContainer.removeClass('cfm-visible');
                    setTimeout(function() {
                        $cfmLayer.remove();
                        callback(!1)
                    }, 350)
                }
            })
        }
    </script>
    <script>
        function notifyit(object) {
            var $ntfBoxContainer = $('<div class="ntf-box-container"></div>');
            var $ntfBox = $('<div class="ntf-box"></div>');
            var $ntfMessage = $('<span class="ntf-message ntf-message-' + $.trim(object.status) + '">' + $.trim(object.message) + '</span>');
            $('.ntf-box-container-current').removeClass('ntf-visible');
            setTimeout(function() {
                $('.ntf-box-container-current').remove()
            }, 350);
            $ntfBox.append($ntfMessage);
            $ntfBoxContainer.append($ntfBox);
            $('body').append($ntfBoxContainer);
            setTimeout(function() {
                $ntfBoxContainer.addClass('ntf-visible')
            }, 100);
            setTimeout(function() {
                $ntfBoxContainer.addClass('ntf-box-container-current')
            }, 350);
            $ntfMessage.on('click', function() {
                $ntfBoxContainer.removeClass('ntf-visible');
                setTimeout(function() {
                    $ntfBoxContainer.remove()
                }, 350)
            });
            setTimeout(function() {
                $ntfBoxContainer.removeClass('ntf-visible');
                setTimeout(function() {
                    $ntfBoxContainer.remove()
                }, 350)
            }, 8000)
        }
    </script>
    <script>
        function promptit(object, callback) {
            var agreeLabel = $.trim(object.agreeLabel) || 'Ok';
            var disagreeLabel = $.trim(object.disagreeLabel) || 'Cancel';
            var placeholder = $.trim(object.placeholder) || 'Your input here';
            var $pptLayer = $('<div class="ppt-layer"></div>');
            var $pptBoxContainer = $('<div class="ppt-box-container"></div>');
            var $pptBox = $('<div class="ppt-box"></div>');
            var $pptMessage = $('<span class="ppt-message">' + $.trim(object.message) + '</span>');
            var $pptFieldset = $('<div class="ppt-fieldset"></div>');
            var $pptPlaceholder = ('<span class="ppt-placeholder">' + placeholder + '</span>');
            var $pptInput = $('<input type="text" class="ppt-input" autocomplete="off">');
            var $pptBtns = $('<ul class="ppt-btns"></ul>');
            var $pptBtnAgree = $('<li class="ppt-btn ppt-btn-agree">' + agreeLabel + '</li>');
            var $pptBtnSeparator = $('<li class="ppt-btn ppt-btn-separator">&bull;</li>');
            var $pptBtnDisagree = $('<li class="ppt-btn ppt-btn-disagree">' + disagreeLabel + '</li>');
            $pptBtns.append($pptBtnAgree).append($pptBtnSeparator).append($pptBtnDisagree);
            $pptFieldset.append($pptPlaceholder).append($pptInput);
            $pptBox.append($pptMessage).append($pptFieldset).append($pptBtns);
            $pptBoxContainer.append($pptBox);
            $pptLayer.append($pptBoxContainer);
            $('body').append($pptLayer);
            setTimeout(function() {
                $pptLayer.addClass('ppt-visible');
                $pptBoxContainer.addClass('ppt-visible');
                $pptInput.focus()
            }, 100);
            $pptBtnDisagree.on('click', function(e) {
                $pptLayer.removeClass('ppt-visible');
                $pptBoxContainer.removeClass('ppt-visible');
                setTimeout(function() {
                    $pptLayer.remove()
                }, 350)
            });
            $pptBtnAgree.on('click', function(e) {
                if (typeof callback === 'function') {
                    if ($.trim($pptInput.val()).length) {
                        $pptLayer.removeClass('ppt-visible');
                        $pptBoxContainer.removeClass('ppt-visible');
                        setTimeout(function() {
                            callback($.trim($pptInput.val()));
                            $pptLayer.remove()
                        }, 350)
                    } else {
                        $pptInput.attr('placeholder', 'Required field')
                    }
                }
            });
            $pptInput.on('keypress', function(e) {
                if (e.keyCode == 13) {
                    var $this = $(this);
                    if (typeof callback === 'function') {
                        if ($.trim($this.val()).length) {
                            $this.parents('.ppt-box-container').removeClass('ppt-visible').parents('.ppt-layer').removeClass('ppt-visible');
                            setTimeout(function() {
                                callback($.trim($pptInput.val()));
                                $this.parents('.ppt-layer').remove()
                            }, 300)
                        } else {
                            $this.attr('placeholder', 'Required field')
                        }
                    }
                }
            })
        }
    </script>
    <script>
        ! function(a) {
            "use strict";
            var b = function(a, c, d) {
                    var e, f, g = document.createElement("img");
                    if (g.onerror = c, g.onload = function() {
                            !f || d && d.noRevoke || b.revokeObjectURL(f), c && c(b.scale(g, d))
                        }, b.isInstanceOf("Blob", a) || b.isInstanceOf("File", a)) e = f = b.createObjectURL(a), g._type = a.type;
                    else {
                        if ("string" != typeof a) return !1;
                        e = a, d && d.crossOrigin && (g.crossOrigin = d.crossOrigin)
                    }
                    return e ? (g.src = e, g) : b.readFile(a, function(a) {
                        var b = a.target;
                        b && b.result ? g.src = b.result : c && c(a)
                    })
                },
                c = window.createObjectURL && window || window.URL && URL.revokeObjectURL && URL || window.webkitURL && webkitURL;
            b.isInstanceOf = function(a, b) {
                return Object.prototype.toString.call(b) === "[object " + a + "]"
            }, b.transformCoordinates = function() {}, b.getTransformedOptions = function(a, b) {
                var c, d, e, f, g = b.aspectRatio;
                if (!g) return b;
                c = {};
                for (d in b) b.hasOwnProperty(d) && (c[d] = b[d]);
                return c.crop = !0, e = a.naturalWidth || a.width, f = a.naturalHeight || a.height, e / f > g ? (c.maxWidth = f * g, c.maxHeight = f) : (c.maxWidth = e, c.maxHeight = e / g), c
            }, b.renderImageToCanvas = function(a, b, c, d, e, f, g, h, i, j) {
                return a.getContext("2d").drawImage(b, c, d, e, f, g, h, i, j), a
            }, b.hasCanvasOption = function(a) {
                return a.canvas || a.crop || a.aspectRatio
            }, b.scale = function(a, c) {
                c = c || {};
                var d, e, f, g, h, i, j, k, l, m = document.createElement("canvas"),
                    n = a.getContext || b.hasCanvasOption(c) && m.getContext,
                    o = a.naturalWidth || a.width,
                    p = a.naturalHeight || a.height,
                    q = o,
                    r = p,
                    s = function() {
                        var a = Math.max((f || q) / q, (g || r) / r);
                        a > 1 && (q *= a, r *= a)
                    },
                    t = function() {
                        var a = Math.min((d || q) / q, (e || r) / r);
                        1 > a && (q *= a, r *= a)
                    };
                return n && (c = b.getTransformedOptions(a, c), j = c.left || 0, k = c.top || 0, c.sourceWidth ? (h = c.sourceWidth, void 0 !== c.right && void 0 === c.left && (j = o - h - c.right)) : h = o - j - (c.right || 0), c.sourceHeight ? (i = c.sourceHeight, void 0 !== c.bottom && void 0 === c.top && (k = p - i - c.bottom)) : i = p - k - (c.bottom || 0), q = h, r = i), d = c.maxWidth, e = c.maxHeight, f = c.minWidth, g = c.minHeight, n && d && e && c.crop ? (q = d, r = e, l = h / i - d / e, 0 > l ? (i = e * h / d, void 0 === c.top && void 0 === c.bottom && (k = (p - i) / 2)) : l > 0 && (h = d * i / e, void 0 === c.left && void 0 === c.right && (j = (o - h) / 2))) : ((c.contain || c.cover) && (f = d = d || f, g = e = e || g), c.cover ? (t(), s()) : (s(), t())), n ? (m.width = q, m.height = r, b.transformCoordinates(m, c), b.renderImageToCanvas(m, a, j, k, h, i, 0, 0, q, r)) : (a.width = q, a.height = r, a)
            }, b.createObjectURL = function(a) {
                return c ? c.createObjectURL(a) : !1
            }, b.revokeObjectURL = function(a) {
                return c ? c.revokeObjectURL(a) : !1
            }, b.readFile = function(a, b, c) {
                if (window.FileReader) {
                    var d = new FileReader;
                    if (d.onload = d.onerror = b, c = c || "readAsDataURL", d[c]) return d[c](a), d
                }
                return !1
            }, "function" == typeof define && define.amd ? define(function() {
                return b
            }) : a.loadImage = b
        }(window),
        function(a) {
            "use strict";
            "function" == typeof define && define.amd ? define(["load-image"], a) : a(window.loadImage)
        }(function(a) {
            "use strict";
            if (window.navigator && window.navigator.platform && /iP(hone|od|ad)/.test(window.navigator.platform)) {
                var b = a.renderImageToCanvas;
                a.detectSubsampling = function(a) {
                    var b, c;
                    return a.width * a.height > 1048576 ? (b = document.createElement("canvas"), b.width = b.height = 1, c = b.getContext("2d"), c.drawImage(a, -a.width + 1, 0), 0 === c.getImageData(0, 0, 1, 1).data[3]) : !1
                }, a.detectVerticalSquash = function(a, b) {
                    var c, d, e, f, g, h = a.naturalHeight || a.height,
                        i = document.createElement("canvas"),
                        j = i.getContext("2d");
                    for (b && (h /= 2), i.width = 1, i.height = h, j.drawImage(a, 0, 0), c = j.getImageData(0, 0, 1, h).data, d = 0, e = h, f = h; f > d;) g = c[4 * (f - 1) + 3], 0 === g ? e = f : d = f, f = e + d >> 1;
                    return f / h || 1
                }, a.renderImageToCanvas = function(c, d, e, f, g, h, i, j, k, l) {
                    if ("image/jpeg" === d._type) {
                        var m, n, o, p, q = c.getContext("2d"),
                            r = document.createElement("canvas"),
                            s = 1024,
                            t = r.getContext("2d");
                        if (r.width = s, r.height = s, q.save(), m = a.detectSubsampling(d), m && (e /= 2, f /= 2, g /= 2, h /= 2), n = a.detectVerticalSquash(d, m), m || 1 !== n) {
                            for (f *= n, k = Math.ceil(s * k / g), l = Math.ceil(s * l / h / n), j = 0, p = 0; h > p;) {
                                for (i = 0, o = 0; g > o;) t.clearRect(0, 0, s, s), t.drawImage(d, e, f, g, h, -o, -p, g, h), q.drawImage(r, 0, 0, s, s, i, j, k, l), o += s, i += k;
                                p += s, j += l
                            }
                            return q.restore(), c
                        }
                    }
                    return b(c, d, e, f, g, h, i, j, k, l)
                }
            }
        }),
        function(a) {
            "use strict";
            "function" == typeof define && define.amd ? define(["load-image"], a) : a(window.loadImage)
        }(function(a) {
            "use strict";
            var b = a.hasCanvasOption,
                c = a.transformCoordinates,
                d = a.getTransformedOptions;
            a.hasCanvasOption = function(c) {
                return b.call(a, c) || c.orientation
            }, a.transformCoordinates = function(b, d) {
                c.call(a, b, d);
                var e = b.getContext("2d"),
                    f = b.width,
                    g = b.height,
                    h = d.orientation;
                if (h && !(h > 8)) switch (h > 4 && (b.width = g, b.height = f), h) {
                    case 2:
                        e.translate(f, 0), e.scale(-1, 1);
                        break;
                    case 3:
                        e.translate(f, g), e.rotate(Math.PI);
                        break;
                    case 4:
                        e.translate(0, g), e.scale(1, -1);
                        break;
                    case 5:
                        e.rotate(.5 * Math.PI), e.scale(1, -1);
                        break;
                    case 6:
                        e.rotate(.5 * Math.PI), e.translate(0, -g);
                        break;
                    case 7:
                        e.rotate(.5 * Math.PI), e.translate(f, -g), e.scale(-1, 1);
                        break;
                    case 8:
                        e.rotate(-.5 * Math.PI), e.translate(-f, 0)
                }
            }, a.getTransformedOptions = function(b, c) {
                var e, f, g = d.call(a, b, c),
                    h = g.orientation;
                if (!h || h > 8 || 1 === h) return g;
                e = {};
                for (f in g) g.hasOwnProperty(f) && (e[f] = g[f]);
                switch (g.orientation) {
                    case 2:
                        e.left = g.right, e.right = g.left;
                        break;
                    case 3:
                        e.left = g.right, e.top = g.bottom, e.right = g.left, e.bottom = g.top;
                        break;
                    case 4:
                        e.top = g.bottom, e.bottom = g.top;
                        break;
                    case 5:
                        e.left = g.top, e.top = g.left, e.right = g.bottom, e.bottom = g.right;
                        break;
                    case 6:
                        e.left = g.top, e.top = g.right, e.right = g.bottom, e.bottom = g.left;
                        break;
                    case 7:
                        e.left = g.bottom, e.top = g.right, e.right = g.top, e.bottom = g.left;
                        break;
                    case 8:
                        e.left = g.bottom, e.top = g.left, e.right = g.top, e.bottom = g.right
                }
                return g.orientation > 4 && (e.maxWidth = g.maxHeight, e.maxHeight = g.maxWidth, e.minWidth = g.minHeight, e.minHeight = g.minWidth, e.sourceWidth = g.sourceHeight, e.sourceHeight = g.sourceWidth), e
            }
        }),
        function(a) {
            "use strict";
            "function" == typeof define && define.amd ? define(["load-image"], a) : a(window.loadImage)
        }(function(a) {
            "use strict";
            var b = window.Blob && (Blob.prototype.slice || Blob.prototype.webkitSlice || Blob.prototype.mozSlice);
            a.blobSlice = b && function() {
                var a = this.slice || this.webkitSlice || this.mozSlice;
                return a.apply(this, arguments)
            }, a.metaDataParsers = {
                jpeg: {
                    65505: []
                }
            }, a.parseMetaData = function(b, c, d) {
                d = d || {};
                var e = this,
                    f = d.maxMetaDataSize || 262144,
                    g = {},
                    h = !(window.DataView && b && b.size >= 12 && "image/jpeg" === b.type && a.blobSlice);
                (h || !a.readFile(a.blobSlice.call(b, 0, f), function(b) {
                    if (b.target.error) return console.log(b.target.error), void c(g);
                    var f, h, i, j, k = b.target.result,
                        l = new DataView(k),
                        m = 2,
                        n = l.byteLength - 4,
                        o = m;
                    if (65496 === l.getUint16(0)) {
                        for (; n > m && (f = l.getUint16(m), f >= 65504 && 65519 >= f || 65534 === f);) {
                            if (h = l.getUint16(m + 2) + 2, m + h > l.byteLength) {
                                console.log("Invalid meta data: Invalid segment size.");
                                break
                            }
                            if (i = a.metaDataParsers.jpeg[f])
                                for (j = 0; j < i.length; j += 1) i[j].call(e, l, m, h, g, d);
                            m += h, o = m
                        }!d.disableImageHead && o > 6 && (g.imageHead = k.slice ? k.slice(0, o) : new Uint8Array(k).subarray(0, o))
                    } else console.log("Invalid JPEG file: Missing JPEG marker.");
                    c(g)
                }, "readAsArrayBuffer")) && c(g)
            }
        }),
        function(a) {
            "use strict";
            "function" == typeof define && define.amd ? define(["load-image", "load-image-meta"], a) : a(window.loadImage)
        }(function(a) {
            "use strict";
            a.ExifMap = function() {
                return this
            }, a.ExifMap.prototype.map = {
                Orientation: 274
            }, a.ExifMap.prototype.get = function(a) {
                return this[a] || this[this.map[a]]
            }, a.getExifThumbnail = function(a, b, c) {
                var d, e, f;
                if (!c || b + c > a.byteLength) return void console.log("Invalid Exif data: Invalid thumbnail data.");
                for (d = [], e = 0; c > e; e += 1) f = a.getUint8(b + e), d.push((16 > f ? "0" : "") + f.toString(16));
                return "data:image/jpeg,%" + d.join("%")
            }, a.exifTagTypes = {
                1: {
                    getValue: function(a, b) {
                        return a.getUint8(b)
                    },
                    size: 1
                },
                2: {
                    getValue: function(a, b) {
                        return String.fromCharCode(a.getUint8(b))
                    },
                    size: 1,
                    ascii: !0
                },
                3: {
                    getValue: function(a, b, c) {
                        return a.getUint16(b, c)
                    },
                    size: 2
                },
                4: {
                    getValue: function(a, b, c) {
                        return a.getUint32(b, c)
                    },
                    size: 4
                },
                5: {
                    getValue: function(a, b, c) {
                        return a.getUint32(b, c) / a.getUint32(b + 4, c)
                    },
                    size: 8
                },
                9: {
                    getValue: function(a, b, c) {
                        return a.getInt32(b, c)
                    },
                    size: 4
                },
                10: {
                    getValue: function(a, b, c) {
                        return a.getInt32(b, c) / a.getInt32(b + 4, c)
                    },
                    size: 8
                }
            }, a.exifTagTypes[7] = a.exifTagTypes[1], a.getExifValue = function(b, c, d, e, f, g) {
                var h, i, j, k, l, m, n = a.exifTagTypes[e];
                if (!n) return void console.log("Invalid Exif data: Invalid tag type.");
                if (h = n.size * f, i = h > 4 ? c + b.getUint32(d + 8, g) : d + 8, i + h > b.byteLength) return void console.log("Invalid Exif data: Invalid data offset.");
                if (1 === f) return n.getValue(b, i, g);
                for (j = [], k = 0; f > k; k += 1) j[k] = n.getValue(b, i + k * n.size, g);
                if (n.ascii) {
                    for (l = "", k = 0; k < j.length && (m = j[k], "\x00" !== m); k += 1) l += m;
                    return l
                }
                return j
            }, a.parseExifTag = function(b, c, d, e, f) {
                var g = b.getUint16(d, e);
                f.exif[g] = a.getExifValue(b, c, d, b.getUint16(d + 2, e), b.getUint32(d + 4, e), e)
            }, a.parseExifTags = function(a, b, c, d, e) {
                var f, g, h;
                if (c + 6 > a.byteLength) return void console.log("Invalid Exif data: Invalid directory offset.");
                if (f = a.getUint16(c, d), g = c + 2 + 12 * f, g + 4 > a.byteLength) return void console.log("Invalid Exif data: Invalid directory size.");
                for (h = 0; f > h; h += 1) this.parseExifTag(a, b, c + 2 + 12 * h, d, e);
                return a.getUint32(g, d)
            }, a.parseExifData = function(b, c, d, e, f) {
                if (!f.disableExif) {
                    var g, h, i, j = c + 10;
                    if (1165519206 === b.getUint32(c + 4)) {
                        if (j + 8 > b.byteLength) return void console.log("Invalid Exif data: Invalid segment size.");
                        if (0 !== b.getUint16(c + 8)) return void console.log("Invalid Exif data: Missing byte alignment offset.");
                        switch (b.getUint16(j)) {
                            case 18761:
                                g = !0;
                                break;
                            case 19789:
                                g = !1;
                                break;
                            default:
                                return void console.log("Invalid Exif data: Invalid byte alignment marker.")
                        }
                        if (42 !== b.getUint16(j + 2, g)) return void console.log("Invalid Exif data: Missing TIFF marker.");
                        h = b.getUint32(j + 4, g), e.exif = new a.ExifMap, h = a.parseExifTags(b, j, j + h, g, e), h && !f.disableExifThumbnail && (i = {
                            exif: {}
                        }, h = a.parseExifTags(b, j, j + h, g, i), i.exif[513] && (e.exif.Thumbnail = a.getExifThumbnail(b, j + i.exif[513], i.exif[514]))), e.exif[34665] && !f.disableExifSub && a.parseExifTags(b, j, j + e.exif[34665], g, e), e.exif[34853] && !f.disableExifGps && a.parseExifTags(b, j, j + e.exif[34853], g, e)
                    }
                }
            }, a.metaDataParsers.jpeg[65505].push(a.parseExifData)
        }),
        function(a) {
            "use strict";
            "function" == typeof define && define.amd ? define(["load-image", "load-image-exif"], a) : a(window.loadImage)
        }(function(a) {
            "use strict";
            a.ExifMap.prototype.tags = {
                    256: "ImageWidth",
                    257: "ImageHeight",
                    34665: "ExifIFDPointer",
                    34853: "GPSInfoIFDPointer",
                    40965: "InteroperabilityIFDPointer",
                    258: "BitsPerSample",
                    259: "Compression",
                    262: "PhotometricInterpretation",
                    274: "Orientation",
                    277: "SamplesPerPixel",
                    284: "PlanarConfiguration",
                    530: "YCbCrSubSampling",
                    531: "YCbCrPositioning",
                    282: "XResolution",
                    283: "YResolution",
                    296: "ResolutionUnit",
                    273: "StripOffsets",
                    278: "RowsPerStrip",
                    279: "StripByteCounts",
                    513: "JPEGInterchangeFormat",
                    514: "JPEGInterchangeFormatLength",
                    301: "TransferFunction",
                    318: "WhitePoint",
                    319: "PrimaryChromaticities",
                    529: "YCbCrCoefficients",
                    532: "ReferenceBlackWhite",
                    306: "DateTime",
                    270: "ImageDescription",
                    271: "Make",
                    272: "Model",
                    305: "Software",
                    315: "Artist",
                    33432: "Copyright",
                    36864: "ExifVersion",
                    40960: "FlashpixVersion",
                    40961: "ColorSpace",
                    40962: "PixelXDimension",
                    40963: "PixelYDimension",
                    42240: "Gamma",
                    37121: "ComponentsConfiguration",
                    37122: "CompressedBitsPerPixel",
                    37500: "MakerNote",
                    37510: "UserComment",
                    40964: "RelatedSoundFile",
                    36867: "DateTimeOriginal",
                    36868: "DateTimeDigitized",
                    37520: "SubSecTime",
                    37521: "SubSecTimeOriginal",
                    37522: "SubSecTimeDigitized",
                    33434: "ExposureTime",
                    33437: "FNumber",
                    34850: "ExposureProgram",
                    34852: "SpectralSensitivity",
                    34855: "PhotographicSensitivity",
                    34856: "OECF",
                    34864: "SensitivityType",
                    34865: "StandardOutputSensitivity",
                    34866: "RecommendedExposureIndex",
                    34867: "ISOSpeed",
                    34868: "ISOSpeedLatitudeyyy",
                    34869: "ISOSpeedLatitudezzz",
                    37377: "ShutterSpeedValue",
                    37378: "ApertureValue",
                    37379: "BrightnessValue",
                    37380: "ExposureBias",
                    37381: "MaxApertureValue",
                    37382: "SubjectDistance",
                    37383: "MeteringMode",
                    37384: "LightSource",
                    37385: "Flash",
                    37396: "SubjectArea",
                    37386: "FocalLength",
                    41483: "FlashEnergy",
                    41484: "SpatialFrequencyResponse",
                    41486: "FocalPlaneXResolution",
                    41487: "FocalPlaneYResolution",
                    41488: "FocalPlaneResolutionUnit",
                    41492: "SubjectLocation",
                    41493: "ExposureIndex",
                    41495: "SensingMethod",
                    41728: "FileSource",
                    41729: "SceneType",
                    41730: "CFAPattern",
                    41985: "CustomRendered",
                    41986: "ExposureMode",
                    41987: "WhiteBalance",
                    41988: "DigitalZoomRatio",
                    41989: "FocalLengthIn35mmFilm",
                    41990: "SceneCaptureType",
                    41991: "GainControl",
                    41992: "Contrast",
                    41993: "Saturation",
                    41994: "Sharpness",
                    41995: "DeviceSettingDescription",
                    41996: "SubjectDistanceRange",
                    42016: "ImageUniqueID",
                    42032: "CameraOwnerName",
                    42033: "BodySerialNumber",
                    42034: "LensSpecification",
                    42035: "LensMake",
                    42036: "LensModel",
                    42037: "LensSerialNumber",
                    0: "GPSVersionID",
                    1: "GPSLatitudeRef",
                    2: "GPSLatitude",
                    3: "GPSLongitudeRef",
                    4: "GPSLongitude",
                    5: "GPSAltitudeRef",
                    6: "GPSAltitude",
                    7: "GPSTimeStamp",
                    8: "GPSSatellites",
                    9: "GPSStatus",
                    10: "GPSMeasureMode",
                    11: "GPSDOP",
                    12: "GPSSpeedRef",
                    13: "GPSSpeed",
                    14: "GPSTrackRef",
                    15: "GPSTrack",
                    16: "GPSImgDirectionRef",
                    17: "GPSImgDirection",
                    18: "GPSMapDatum",
                    19: "GPSDestLatitudeRef",
                    20: "GPSDestLatitude",
                    21: "GPSDestLongitudeRef",
                    22: "GPSDestLongitude",
                    23: "GPSDestBearingRef",
                    24: "GPSDestBearing",
                    25: "GPSDestDistanceRef",
                    26: "GPSDestDistance",
                    27: "GPSProcessingMethod",
                    28: "GPSAreaInformation",
                    29: "GPSDateStamp",
                    30: "GPSDifferential",
                    31: "GPSHPositioningError"
                }, a.ExifMap.prototype.stringValues = {
                    ExposureProgram: {
                        0: "Undefined",
                        1: "Manual",
                        2: "Normal program",
                        3: "Aperture priority",
                        4: "Shutter priority",
                        5: "Creative program",
                        6: "Action program",
                        7: "Portrait mode",
                        8: "Landscape mode"
                    },
                    MeteringMode: {
                        0: "Unknown",
                        1: "Average",
                        2: "CenterWeightedAverage",
                        3: "Spot",
                        4: "MultiSpot",
                        5: "Pattern",
                        6: "Partial",
                        255: "Other"
                    },
                    LightSource: {
                        0: "Unknown",
                        1: "Daylight",
                        2: "Fluorescent",
                        3: "Tungsten (incandescent light)",
                        4: "Flash",
                        9: "Fine weather",
                        10: "Cloudy weather",
                        11: "Shade",
                        12: "Daylight fluorescent (D 5700 - 7100K)",
                        13: "Day white fluorescent (N 4600 - 5400K)",
                        14: "Cool white fluorescent (W 3900 - 4500K)",
                        15: "White fluorescent (WW 3200 - 3700K)",
                        17: "Standard light A",
                        18: "Standard light B",
                        19: "Standard light C",
                        20: "D55",
                        21: "D65",
                        22: "D75",
                        23: "D50",
                        24: "ISO studio tungsten",
                        255: "Other"
                    },
                    Flash: {
                        0: "Flash did not fire",
                        1: "Flash fired",
                        5: "Strobe return light not detected",
                        7: "Strobe return light detected",
                        9: "Flash fired, compulsory flash mode",
                        13: "Flash fired, compulsory flash mode, return light not detected",
                        15: "Flash fired, compulsory flash mode, return light detected",
                        16: "Flash did not fire, compulsory flash mode",
                        24: "Flash did not fire, auto mode",
                        25: "Flash fired, auto mode",
                        29: "Flash fired, auto mode, return light not detected",
                        31: "Flash fired, auto mode, return light detected",
                        32: "No flash function",
                        65: "Flash fired, red-eye reduction mode",
                        69: "Flash fired, red-eye reduction mode, return light not detected",
                        71: "Flash fired, red-eye reduction mode, return light detected",
                        73: "Flash fired, compulsory flash mode, red-eye reduction mode",
                        77: "Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected",
                        79: "Flash fired, compulsory flash mode, red-eye reduction mode, return light detected",
                        89: "Flash fired, auto mode, red-eye reduction mode",
                        93: "Flash fired, auto mode, return light not detected, red-eye reduction mode",
                        95: "Flash fired, auto mode, return light detected, red-eye reduction mode"
                    },
                    SensingMethod: {
                        1: "Undefined",
                        2: "One-chip color area sensor",
                        3: "Two-chip color area sensor",
                        4: "Three-chip color area sensor",
                        5: "Color sequential area sensor",
                        7: "Trilinear sensor",
                        8: "Color sequential linear sensor"
                    },
                    SceneCaptureType: {
                        0: "Standard",
                        1: "Landscape",
                        2: "Portrait",
                        3: "Night scene"
                    },
                    SceneType: {
                        1: "Directly photographed"
                    },
                    CustomRendered: {
                        0: "Normal process",
                        1: "Custom process"
                    },
                    WhiteBalance: {
                        0: "Auto white balance",
                        1: "Manual white balance"
                    },
                    GainControl: {
                        0: "None",
                        1: "Low gain up",
                        2: "High gain up",
                        3: "Low gain down",
                        4: "High gain down"
                    },
                    Contrast: {
                        0: "Normal",
                        1: "Soft",
                        2: "Hard"
                    },
                    Saturation: {
                        0: "Normal",
                        1: "Low saturation",
                        2: "High saturation"
                    },
                    Sharpness: {
                        0: "Normal",
                        1: "Soft",
                        2: "Hard"
                    },
                    SubjectDistanceRange: {
                        0: "Unknown",
                        1: "Macro",
                        2: "Close view",
                        3: "Distant view"
                    },
                    FileSource: {
                        3: "DSC"
                    },
                    ComponentsConfiguration: {
                        0: "",
                        1: "Y",
                        2: "Cb",
                        3: "Cr",
                        4: "R",
                        5: "G",
                        6: "B"
                    },
                    Orientation: {
                        1: "top-left",
                        2: "top-right",
                        3: "bottom-right",
                        4: "bottom-left",
                        5: "left-top",
                        6: "right-top",
                        7: "right-bottom",
                        8: "left-bottom"
                    }
                }, a.ExifMap.prototype.getText = function(a) {
                    var b = this.get(a);
                    switch (a) {
                        case "LightSource":
                        case "Flash":
                        case "MeteringMode":
                        case "ExposureProgram":
                        case "SensingMethod":
                        case "SceneCaptureType":
                        case "SceneType":
                        case "CustomRendered":
                        case "WhiteBalance":
                        case "GainControl":
                        case "Contrast":
                        case "Saturation":
                        case "Sharpness":
                        case "SubjectDistanceRange":
                        case "FileSource":
                        case "Orientation":
                            return this.stringValues[a][b];
                        case "ExifVersion":
                        case "FlashpixVersion":
                            return String.fromCharCode(b[0], b[1], b[2], b[3]);
                        case "ComponentsConfiguration":
                            return this.stringValues[a][b[0]] + this.stringValues[a][b[1]] + this.stringValues[a][b[2]] + this.stringValues[a][b[3]];
                        case "GPSVersionID":
                            return b[0] + "." + b[1] + "." + b[2] + "." + b[3]
                    }
                    return String(b)
                },
                function(a) {
                    var b, c = a.tags,
                        d = a.map;
                    for (b in c) c.hasOwnProperty(b) && (d[c[b]] = b)
                }(a.ExifMap.prototype), a.ExifMap.prototype.getAll = function() {
                    var a, b, c = {};
                    for (a in this) this.hasOwnProperty(a) && (b = this.tags[a], b && (c[b] = this.getText(b)));
                    return c
                }
        })
    </script>
    <script>
        (function($, window, document, undefined) {
            (function() {
                var lastTime = 0;
                var vendors = ['ms', 'moz', 'webkit', 'o'];
                for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
                    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame']
                }
                if (!window.requestAnimationFrame) window.requestAnimationFrame = function(callback) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                    var id = window.setTimeout(function() {
                        callback(currTime + timeToCall)
                    }, timeToCall);
                    lastTime = currTime + timeToCall;
                    return id
                };
                if (!window.cancelAnimationFrame) window.cancelAnimationFrame = function(id) {
                    clearTimeout(id)
                }
            }());

            function Parallax(element, options) {
                var self = this;
                if (typeof options == 'object') {
                    delete options.refresh;
                    delete options.render;
                    $.extend(this, options)
                }
                this.$element = $(element);
                if (!this.imageSrc && this.$element.is('img')) {
                    this.imageSrc = this.$element.attr('src')
                }
                var positions = (this.position + '').toLowerCase().match(/\S+/g) || [];
                if (positions.length < 1) {
                    positions.push('center')
                }
                if (positions.length == 1) {
                    positions.push(positions[0])
                }
                if (positions[0] == 'top' || positions[0] == 'bottom' || positions[1] == 'left' || positions[1] == 'right') {
                    positions = [positions[1], positions[0]]
                }
                if (this.positionX !== undefined) positions[0] = this.positionX.toLowerCase();
                if (this.positionY !== undefined) positions[1] = this.positionY.toLowerCase();
                self.positionX = positions[0];
                self.positionY = positions[1];
                if (this.positionX != 'left' && this.positionX != 'right') {
                    if (isNaN(parseInt(this.positionX))) {
                        this.positionX = 'center'
                    } else {
                        this.positionX = parseInt(this.positionX)
                    }
                }
                if (this.positionY != 'top' && this.positionY != 'bottom') {
                    if (isNaN(parseInt(this.positionY))) {
                        this.positionY = 'center'
                    } else {
                        this.positionY = parseInt(this.positionY)
                    }
                }
                this.position = this.positionX + (isNaN(this.positionX) ? '' : 'px') + ' ' + this.positionY + (isNaN(this.positionY) ? '' : 'px');
                if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                    if (this.imageSrc && this.iosFix && !this.$element.is('img')) {
                        this.$element.css({
                            backgroundImage: 'url("' + this.imageSrc + '")',
                            backgroundSize: 'cover',
                            backgroundPosition: this.position
                        })
                    }
                    return this
                }
                if (navigator.userAgent.match(/(Android)/)) {
                    if (this.imageSrc && this.androidFix && !this.$element.is('img')) {
                        this.$element.css({
                            backgroundImage: 'url("' + this.imageSrc + '")',
                            backgroundSize: 'cover',
                            backgroundPosition: this.position
                        })
                    }
                    return this
                }
                this.$mirror = $('<div />').prependTo(this.mirrorContainer);
                var slider = this.$element.find('>.parallax-slider');
                var sliderExisted = !1;
                if (slider.length == 0) this.$slider = $('<img />').prependTo(this.$mirror);
                else {
                    this.$slider = slider.prependTo(this.$mirror);
                    sliderExisted = !0
                }
                this.$mirror.addClass('parallax-mirror').css({
                    visibility: 'hidden',
                    zIndex: this.zIndex,
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    overflow: 'hidden'
                });
                this.$slider.addClass('parallax-slider').one('load', function() {
                    if (!self.naturalHeight || !self.naturalWidth) {
                        self.naturalHeight = this.naturalHeight || this.height || 1;
                        self.naturalWidth = this.naturalWidth || this.width || 1
                    }
                    self.aspectRatio = self.naturalWidth / self.naturalHeight;
                    Parallax.isSetup || Parallax.setup();
                    Parallax.sliders.push(self);
                    Parallax.isFresh = !1;
                    Parallax.requestRender()
                });
                if (!sliderExisted) this.$slider[0].src = this.imageSrc;
                if (this.naturalHeight && this.naturalWidth || this.$slider[0].complete || slider.length > 0) {
                    this.$slider.trigger('load')
                }
            }
            $.extend(Parallax.prototype, {
                speed: 0.2,
                bleed: 0,
                zIndex: -100,
                iosFix: !0,
                androidFix: !0,
                position: 'center',
                overScrollFix: !1,
                mirrorContainer: 'body',
                refresh: function() {
                    this.boxWidth = this.$element.outerWidth();
                    this.boxHeight = this.$element.outerHeight() + this.bleed * 2;
                    this.boxOffsetTop = this.$element.offset().top - this.bleed;
                    this.boxOffsetLeft = this.$element.offset().left;
                    this.boxOffsetBottom = this.boxOffsetTop + this.boxHeight;
                    var winHeight = Parallax.winHeight;
                    var docHeight = Parallax.docHeight;
                    var maxOffset = Math.min(this.boxOffsetTop, docHeight - winHeight);
                    var minOffset = Math.max(this.boxOffsetTop + this.boxHeight - winHeight, 0);
                    var imageHeightMin = this.boxHeight + (maxOffset - minOffset) * (1 - this.speed) | 0;
                    var imageOffsetMin = (this.boxOffsetTop - maxOffset) * (1 - this.speed) | 0;
                    var margin;
                    if (imageHeightMin * this.aspectRatio >= this.boxWidth) {
                        this.imageWidth = imageHeightMin * this.aspectRatio | 0;
                        this.imageHeight = imageHeightMin;
                        this.offsetBaseTop = imageOffsetMin;
                        margin = this.imageWidth - this.boxWidth;
                        if (this.positionX == 'left') {
                            this.offsetLeft = 0
                        } else if (this.positionX == 'right') {
                            this.offsetLeft = -margin
                        } else if (!isNaN(this.positionX)) {
                            this.offsetLeft = Math.max(this.positionX, -margin)
                        } else {
                            this.offsetLeft = -margin / 2 | 0
                        }
                    } else {
                        this.imageWidth = this.boxWidth;
                        this.imageHeight = this.boxWidth / this.aspectRatio | 0;
                        this.offsetLeft = 0;
                        margin = this.imageHeight - imageHeightMin;
                        if (this.positionY == 'top') {
                            this.offsetBaseTop = imageOffsetMin
                        } else if (this.positionY == 'bottom') {
                            this.offsetBaseTop = imageOffsetMin - margin
                        } else if (!isNaN(this.positionY)) {
                            this.offsetBaseTop = imageOffsetMin + Math.max(this.positionY, -margin)
                        } else {
                            this.offsetBaseTop = imageOffsetMin - margin / 2 | 0
                        }
                    }
                },
                render: function() {
                    var scrollTop = Parallax.scrollTop;
                    var scrollLeft = Parallax.scrollLeft;
                    var overScroll = this.overScrollFix ? Parallax.overScroll : 0;
                    var scrollBottom = scrollTop + Parallax.winHeight;
                    if (this.boxOffsetBottom > scrollTop && this.boxOffsetTop <= scrollBottom) {
                        this.visibility = 'visible';
                        this.mirrorTop = this.boxOffsetTop - scrollTop;
                        this.mirrorLeft = this.boxOffsetLeft - scrollLeft;
                        this.offsetTop = this.offsetBaseTop - this.mirrorTop * (1 - this.speed)
                    } else {
                        this.visibility = 'hidden'
                    }
                    this.$mirror.css({
                        transform: 'translate3d(' + this.mirrorLeft + 'px, ' + (this.mirrorTop - overScroll) + 'px, 0px)',
                        visibility: this.visibility,
                        height: this.boxHeight,
                        width: this.boxWidth
                    });
                    this.$slider.css({
                        transform: 'translate3d(' + this.offsetLeft + 'px, ' + this.offsetTop + 'px, 0px)',
                        position: 'absolute',
                        height: this.imageHeight,
                        width: this.imageWidth,
                        maxWidth: 'none'
                    })
                }
            });
            $.extend(Parallax, {
                scrollTop: 0,
                scrollLeft: 0,
                winHeight: 0,
                winWidth: 0,
                docHeight: 1 << 30,
                docWidth: 1 << 30,
                sliders: [],
                isReady: !1,
                isFresh: !1,
                isBusy: !1,
                setup: function() {
                    if (this.isReady) return;
                    var self = this;
                    var $doc = $(document),
                        $win = $(window);
                    var loadDimensions = function() {
                        Parallax.winHeight = $win.height();
                        Parallax.winWidth = $win.width();
                        Parallax.docHeight = $doc.height();
                        Parallax.docWidth = $doc.width()
                    };
                    var loadScrollPosition = function() {
                        var winScrollTop = $win.scrollTop();
                        var scrollTopMax = Parallax.docHeight - Parallax.winHeight;
                        var scrollLeftMax = Parallax.docWidth - Parallax.winWidth;
                        Parallax.scrollTop = Math.max(0, Math.min(scrollTopMax, winScrollTop));
                        Parallax.scrollLeft = Math.max(0, Math.min(scrollLeftMax, $win.scrollLeft()));
                        Parallax.overScroll = Math.max(winScrollTop - scrollTopMax, Math.min(winScrollTop, 0))
                    };
                    $win.on('resize.px.parallax load.px.parallax', function() {
                        loadDimensions();
                        self.refresh();
                        Parallax.isFresh = !1;
                        Parallax.requestRender()
                    }).on('scroll.px.parallax load.px.parallax', function() {
                        loadScrollPosition();
                        Parallax.requestRender()
                    });
                    loadDimensions();
                    loadScrollPosition();
                    this.isReady = !0;
                    var lastPosition = -1;

                    function frameLoop() {
                        if (lastPosition == window.pageYOffset) {
                            window.requestAnimationFrame(frameLoop);
                            return !1
                        } else lastPosition = window.pageYOffset;
                        self.render();
                        window.requestAnimationFrame(frameLoop)
                    }
                    frameLoop()
                },
                configure: function(options) {
                    if (typeof options == 'object') {
                        delete options.refresh;
                        delete options.render;
                        $.extend(this.prototype, options)
                    }
                },
                refresh: function() {
                    $.each(this.sliders, function() {
                        this.refresh()
                    });
                    this.isFresh = !0
                },
                render: function() {
                    this.isFresh || this.refresh();
                    $.each(this.sliders, function() {
                        this.render()
                    })
                },
                requestRender: function() {
                    var self = this;
                    self.render();
                    self.isBusy = !1
                },
                destroy: function(el) {
                    var i, parallaxElement = $(el).data('px.parallax');
                    parallaxElement.$mirror.remove();
                    for (i = 0; i < this.sliders.length; i += 1) {
                        if (this.sliders[i] == parallaxElement) {
                            this.sliders.splice(i, 1)
                        }
                    }
                    $(el).data('px.parallax', !1);
                    if (this.sliders.length === 0) {
                        $(window).off('scroll.px.parallax resize.px.parallax load.px.parallax');
                        this.isReady = !1;
                        Parallax.isSetup = !1
                    }
                }
            });

            function Plugin(option) {
                return this.each(function() {
                    var $this = $(this);
                    var options = typeof option == 'object' && option;
                    if (this == window || this == document || $this.is('body')) {
                        Parallax.configure(options)
                    } else if (!$this.data('px.parallax')) {
                        options = $.extend({}, $this.data(), options);
                        $this.data('px.parallax', new Parallax(this, options))
                    } else if (typeof option == 'object') {
                        $.extend($this.data('px.parallax'), options)
                    }
                    if (typeof option == 'string') {
                        if (option == 'destroy') {
                            Parallax.destroy(this)
                        } else {
                            Parallax[option]()
                        }
                    }
                })
            }
            var old = $.fn.parallax;
            $.fn.parallax = Plugin;
            $.fn.parallax.Constructor = Parallax;
            $.fn.parallax.noConflict = function() {
                $.fn.parallax = old;
                return this
            };
            $(function() {
                $('[data-parallax="scroll"]').parallax()
            })
        }(jQuery, window, document))
    </script>
    <script>
        ! function() {
            "use strict";

            function t(o) {
                if (!o) throw new Error("No options passed to Waypoint constructor");
                if (!o.element) throw new Error("No element option passed to Waypoint constructor");
                if (!o.handler) throw new Error("No handler option passed to Waypoint constructor");
                this.key = "waypoint-" + e, this.options = t.Adapter.extend({}, t.defaults, o), this.element = this.options.element, this.adapter = new t.Adapter(this.element), this.callback = o.handler, this.axis = this.options.horizontal ? "horizontal" : "vertical", this.enabled = this.options.enabled, this.triggerPoint = null, this.group = t.Group.findOrCreate({
                    name: this.options.group,
                    axis: this.axis
                }), this.context = t.Context.findOrCreateByElement(this.options.context), t.offsetAliases[this.options.offset] && (this.options.offset = t.offsetAliases[this.options.offset]), this.group.add(this), this.context.add(this), i[this.key] = this, e += 1
            }
            var e = 0,
                i = {};
            t.prototype.queueTrigger = function(t) {
                this.group.queueTrigger(this, t)
            }, t.prototype.trigger = function(t) {
                this.enabled && this.callback && this.callback.apply(this, t)
            }, t.prototype.destroy = function() {
                this.context.remove(this), this.group.remove(this), delete i[this.key]
            }, t.prototype.disable = function() {
                return this.enabled = !1, this
            }, t.prototype.enable = function() {
                return this.context.refresh(), this.enabled = !0, this
            }, t.prototype.next = function() {
                return this.group.next(this)
            }, t.prototype.previous = function() {
                return this.group.previous(this)
            }, t.invokeAll = function(t) {
                var e = [];
                for (var o in i) e.push(i[o]);
                for (var n = 0, r = e.length; r > n; n++) e[n][t]()
            }, t.destroyAll = function() {
                t.invokeAll("destroy")
            }, t.disableAll = function() {
                t.invokeAll("disable")
            }, t.enableAll = function() {
                t.Context.refreshAll();
                for (var e in i) i[e].enabled = !0;
                return this
            }, t.refreshAll = function() {
                t.Context.refreshAll()
            }, t.viewportHeight = function() {
                return window.innerHeight || document.documentElement.clientHeight
            }, t.viewportWidth = function() {
                return document.documentElement.clientWidth
            }, t.adapters = [], t.defaults = {
                context: window,
                continuous: !0,
                enabled: !0,
                group: "default",
                horizontal: !1,
                offset: 0
            }, t.offsetAliases = {
                "bottom-in-view": function() {
                    return this.context.innerHeight() - this.adapter.outerHeight()
                },
                "right-in-view": function() {
                    return this.context.innerWidth() - this.adapter.outerWidth()
                }
            }, window.Waypoint = t
        }(),
        function() {
            "use strict";

            function t(t) {
                window.setTimeout(t, 1e3 / 60)
            }

            function e(t) {
                this.element = t, this.Adapter = n.Adapter, this.adapter = new this.Adapter(t), this.key = "waypoint-context-" + i, this.didScroll = !1, this.didResize = !1, this.oldScroll = {
                    x: this.adapter.scrollLeft(),
                    y: this.adapter.scrollTop()
                }, this.waypoints = {
                    vertical: {},
                    horizontal: {}
                }, t.waypointContextKey = this.key, o[t.waypointContextKey] = this, i += 1, n.windowContext || (n.windowContext = !0, n.windowContext = new e(window)), this.createThrottledScrollHandler(), this.createThrottledResizeHandler()
            }
            var i = 0,
                o = {},
                n = window.Waypoint,
                r = window.onload;
            e.prototype.add = function(t) {
                var e = t.options.horizontal ? "horizontal" : "vertical";
                this.waypoints[e][t.key] = t, this.refresh()
            }, e.prototype.checkEmpty = function() {
                var t = this.Adapter.isEmptyObject(this.waypoints.horizontal),
                    e = this.Adapter.isEmptyObject(this.waypoints.vertical),
                    i = this.element == this.element.window;
                t && e && !i && (this.adapter.off(".waypoints"), delete o[this.key])
            }, e.prototype.createThrottledResizeHandler = function() {
                function t() {
                    e.handleResize(), e.didResize = !1
                }
                var e = this;
                this.adapter.on("resize.waypoints", function() {
                    e.didResize || (e.didResize = !0, n.requestAnimationFrame(t))
                })
            }, e.prototype.createThrottledScrollHandler = function() {
                function t() {
                    e.handleScroll(), e.didScroll = !1
                }
                var e = this;
                this.adapter.on("scroll.waypoints", function() {
                    (!e.didScroll || n.isTouch) && (e.didScroll = !0, n.requestAnimationFrame(t))
                })
            }, e.prototype.handleResize = function() {
                n.Context.refreshAll()
            }, e.prototype.handleScroll = function() {
                var t = {},
                    e = {
                        horizontal: {
                            newScroll: this.adapter.scrollLeft(),
                            oldScroll: this.oldScroll.x,
                            forward: "right",
                            backward: "left"
                        },
                        vertical: {
                            newScroll: this.adapter.scrollTop(),
                            oldScroll: this.oldScroll.y,
                            forward: "down",
                            backward: "up"
                        }
                    };
                for (var i in e) {
                    var o = e[i],
                        n = o.newScroll > o.oldScroll,
                        r = n ? o.forward : o.backward;
                    for (var s in this.waypoints[i]) {
                        var a = this.waypoints[i][s];
                        if (null !== a.triggerPoint) {
                            var l = o.oldScroll < a.triggerPoint,
                                h = o.newScroll >= a.triggerPoint,
                                p = l && h,
                                u = !l && !h;
                            (p || u) && (a.queueTrigger(r), t[a.group.id] = a.group)
                        }
                    }
                }
                for (var c in t) t[c].flushTriggers();
                this.oldScroll = {
                    x: e.horizontal.newScroll,
                    y: e.vertical.newScroll
                }
            }, e.prototype.innerHeight = function() {
                return this.element == this.element.window ? n.viewportHeight() : this.adapter.innerHeight()
            }, e.prototype.remove = function(t) {
                delete this.waypoints[t.axis][t.key], this.checkEmpty()
            }, e.prototype.innerWidth = function() {
                return this.element == this.element.window ? n.viewportWidth() : this.adapter.innerWidth()
            }, e.prototype.destroy = function() {
                var t = [];
                for (var e in this.waypoints)
                    for (var i in this.waypoints[e]) t.push(this.waypoints[e][i]);
                for (var o = 0, n = t.length; n > o; o++) t[o].destroy()
            }, e.prototype.refresh = function() {
                var t, e = this.element == this.element.window,
                    i = e ? void 0 : this.adapter.offset(),
                    o = {};
                this.handleScroll(), t = {
                    horizontal: {
                        contextOffset: e ? 0 : i.left,
                        contextScroll: e ? 0 : this.oldScroll.x,
                        contextDimension: this.innerWidth(),
                        oldScroll: this.oldScroll.x,
                        forward: "right",
                        backward: "left",
                        offsetProp: "left"
                    },
                    vertical: {
                        contextOffset: e ? 0 : i.top,
                        contextScroll: e ? 0 : this.oldScroll.y,
                        contextDimension: this.innerHeight(),
                        oldScroll: this.oldScroll.y,
                        forward: "down",
                        backward: "up",
                        offsetProp: "top"
                    }
                };
                for (var r in t) {
                    var s = t[r];
                    for (var a in this.waypoints[r]) {
                        var l, h, p, u, c, d = this.waypoints[r][a],
                            f = d.options.offset,
                            w = d.triggerPoint,
                            y = 0,
                            g = null == w;
                        d.element !== d.element.window && (y = d.adapter.offset()[s.offsetProp]), "function" == typeof f ? f = f.apply(d) : "string" == typeof f && (f = parseFloat(f), d.options.offset.indexOf("%") > -1 && (f = Math.ceil(s.contextDimension * f / 100))), l = s.contextScroll - s.contextOffset, d.triggerPoint = Math.floor(y + l - f), h = w < s.oldScroll, p = d.triggerPoint >= s.oldScroll, u = h && p, c = !h && !p, !g && u ? (d.queueTrigger(s.backward), o[d.group.id] = d.group) : !g && c ? (d.queueTrigger(s.forward), o[d.group.id] = d.group) : g && s.oldScroll >= d.triggerPoint && (d.queueTrigger(s.forward), o[d.group.id] = d.group)
                    }
                }
                return n.requestAnimationFrame(function() {
                    for (var t in o) o[t].flushTriggers()
                }), this
            }, e.findOrCreateByElement = function(t) {
                return e.findByElement(t) || new e(t)
            }, e.refreshAll = function() {
                for (var t in o) o[t].refresh()
            }, e.findByElement = function(t) {
                return o[t.waypointContextKey]
            }, window.onload = function() {
                r && r(), e.refreshAll()
            }, n.requestAnimationFrame = function(e) {
                var i = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || t;
                i.call(window, e)
            }, n.Context = e
        }(),
        function() {
            "use strict";

            function t(t, e) {
                return t.triggerPoint - e.triggerPoint
            }

            function e(t, e) {
                return e.triggerPoint - t.triggerPoint
            }

            function i(t) {
                this.name = t.name, this.axis = t.axis, this.id = this.name + "-" + this.axis, this.waypoints = [], this.clearTriggerQueues(), o[this.axis][this.name] = this
            }
            var o = {
                    vertical: {},
                    horizontal: {}
                },
                n = window.Waypoint;
            i.prototype.add = function(t) {
                this.waypoints.push(t)
            }, i.prototype.clearTriggerQueues = function() {
                this.triggerQueues = {
                    up: [],
                    down: [],
                    left: [],
                    right: []
                }
            }, i.prototype.flushTriggers = function() {
                for (var i in this.triggerQueues) {
                    var o = this.triggerQueues[i],
                        n = "up" === i || "left" === i;
                    o.sort(n ? e : t);
                    for (var r = 0, s = o.length; s > r; r += 1) {
                        var a = o[r];
                        (a.options.continuous || r === o.length - 1) && a.trigger([i])
                    }
                }
                this.clearTriggerQueues()
            }, i.prototype.next = function(e) {
                this.waypoints.sort(t);
                var i = n.Adapter.inArray(e, this.waypoints),
                    o = i === this.waypoints.length - 1;
                return o ? null : this.waypoints[i + 1]
            }, i.prototype.previous = function(e) {
                this.waypoints.sort(t);
                var i = n.Adapter.inArray(e, this.waypoints);
                return i ? this.waypoints[i - 1] : null
            }, i.prototype.queueTrigger = function(t, e) {
                this.triggerQueues[e].push(t)
            }, i.prototype.remove = function(t) {
                var e = n.Adapter.inArray(t, this.waypoints);
                e > -1 && this.waypoints.splice(e, 1)
            }, i.prototype.first = function() {
                return this.waypoints[0]
            }, i.prototype.last = function() {
                return this.waypoints[this.waypoints.length - 1]
            }, i.findOrCreate = function(t) {
                return o[t.axis][t.name] || new i(t)
            }, n.Group = i
        }(),
        function() {
            "use strict";

            function t(t) {
                this.$element = e(t)
            }
            var e = window.jQuery,
                i = window.Waypoint;
            e.each(["innerHeight", "innerWidth", "off", "offset", "on", "outerHeight", "outerWidth", "scrollLeft", "scrollTop"], function(e, i) {
                t.prototype[i] = function() {
                    var t = Array.prototype.slice.call(arguments);
                    return this.$element[i].apply(this.$element, t)
                }
            }), e.each(["extend", "inArray", "isEmptyObject"], function(i, o) {
                t[o] = e[o]
            }), i.adapters.push({
                name: "jquery",
                Adapter: t
            }), i.Adapter = t
        }(),
        function() {
            "use strict";

            function t(t) {
                return function() {
                    var i = [],
                        o = arguments[0];
                    return t.isFunction(arguments[0]) && (o = t.extend({}, arguments[1]), o.handler = arguments[0]), this.each(function() {
                        var n = t.extend({}, o, {
                            element: this
                        });
                        "string" == typeof n.context && (n.context = t(this).closest(n.context)[0]), i.push(new e(n))
                    }), i
                }
            }
            var e = window.Waypoint;
            window.jQuery && (window.jQuery.fn.waypoint = t(window.jQuery)), window.Zepto && (window.Zepto.fn.waypoint = t(window.Zepto))
        }()
    </script>
    <script>
        ! function(a) {
            "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto)
        }(function(a) {
            var b, c, d, e, f, g, h = "Close",
                i = "BeforeClose",
                j = "AfterClose",
                k = "BeforeAppend",
                l = "MarkupParse",
                m = "Open",
                n = "Change",
                o = "mfp",
                p = "." + o,
                q = "mfp-ready",
                r = "mfp-removing",
                s = "mfp-prevent-close",
                t = function() {},
                u = !!window.jQuery,
                v = a(window),
                w = function(a, c) {
                    b.ev.on(o + a + p, c)
                },
                x = function(b, c, d, e) {
                    var f = document.createElement("div");
                    return f.className = "mfp-" + b, d && (f.innerHTML = d), e ? c && c.appendChild(f) : (f = a(f), c && f.appendTo(c)), f
                },
                y = function(c, d) {
                    b.ev.triggerHandler(o + c, d), b.st.callbacks && (c = c.charAt(0).toLowerCase() + c.slice(1), b.st.callbacks[c] && b.st.callbacks[c].apply(b, a.isArray(d) ? d : [d]))
                },
                z = function(c) {
                    return c === g && b.currTemplate.closeBtn || (b.currTemplate.closeBtn = a(b.st.closeMarkup.replace("%title%", b.st.tClose)), g = c), b.currTemplate.closeBtn
                },
                A = function() {
                    a.magnificPopup.instance || (b = new t, b.init(), a.magnificPopup.instance = b)
                },
                B = function() {
                    var a = document.createElement("p").style,
                        b = ["ms", "O", "Moz", "Webkit"];
                    if (void 0 !== a.transition) return !0;
                    for (; b.length;)
                        if (b.pop() + "Transition" in a) return !0;
                    return !1
                };
            t.prototype = {
                constructor: t,
                init: function() {
                    var c = navigator.appVersion;
                    b.isLowIE = b.isIE8 = document.all && !document.addEventListener, b.isAndroid = /android/gi.test(c), b.isIOS = /iphone|ipad|ipod/gi.test(c), b.supportsTransition = B(), b.probablyMobile = b.isAndroid || b.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), d = a(document), b.popupsCache = {}
                },
                open: function(c) {
                    var e;
                    if (c.isObj === !1) {
                        b.items = c.items.toArray(), b.index = 0;
                        var g, h = c.items;
                        for (e = 0; e < h.length; e++)
                            if (g = h[e], g.parsed && (g = g.el[0]), g === c.el[0]) {
                                b.index = e;
                                break
                            }
                    } else b.items = a.isArray(c.items) ? c.items : [c.items], b.index = c.index || 0;
                    if (b.isOpen) return void b.updateItemHTML();
                    b.types = [], f = "", c.mainEl && c.mainEl.length ? b.ev = c.mainEl.eq(0) : b.ev = d, c.key ? (b.popupsCache[c.key] || (b.popupsCache[c.key] = {}), b.currTemplate = b.popupsCache[c.key]) : b.currTemplate = {}, b.st = a.extend(!0, {}, a.magnificPopup.defaults, c), b.fixedContentPos = "auto" === b.st.fixedContentPos ? !b.probablyMobile : b.st.fixedContentPos, b.st.modal && (b.st.closeOnContentClick = !1, b.st.closeOnBgClick = !1, b.st.showCloseBtn = !1, b.st.enableEscapeKey = !1), b.bgOverlay || (b.bgOverlay = x("bg").on("click" + p, function() {
                        b.close()
                    }), b.wrap = x("wrap").attr("tabindex", -1).on("click" + p, function(a) {
                        b._checkIfClose(a.target) && b.close()
                    }), b.container = x("container", b.wrap)), b.contentContainer = x("content"), b.st.preloader && (b.preloader = x("preloader", b.container, b.st.tLoading));
                    var i = a.magnificPopup.modules;
                    for (e = 0; e < i.length; e++) {
                        var j = i[e];
                        j = j.charAt(0).toUpperCase() + j.slice(1), b["init" + j].call(b)
                    }
                    y("BeforeOpen"), b.st.showCloseBtn && (b.st.closeBtnInside ? (w(l, function(a, b, c, d) {
                        c.close_replaceWith = z(d.type)
                    }), f += " mfp-close-btn-in") : b.wrap.append(z())), b.st.alignTop && (f += " mfp-align-top"), b.fixedContentPos ? b.wrap.css({
                        overflow: b.st.overflowY,
                        overflowX: "hidden",
                        overflowY: b.st.overflowY
                    }) : b.wrap.css({
                        top: v.scrollTop(),
                        position: "absolute"
                    }), (b.st.fixedBgPos === !1 || "auto" === b.st.fixedBgPos && !b.fixedContentPos) && b.bgOverlay.css({
                        height: d.height(),
                        position: "absolute"
                    }), b.st.enableEscapeKey && d.on("keyup" + p, function(a) {
                        27 === a.keyCode && b.close()
                    }), v.on("resize" + p, function() {
                        b.updateSize()
                    }), b.st.closeOnContentClick || (f += " mfp-auto-cursor"), f && b.wrap.addClass(f);
                    var k = b.wH = v.height(),
                        n = {};
                    if (b.fixedContentPos && b._hasScrollBar(k)) {
                        var o = b._getScrollbarSize();
                        o && (n.marginRight = o)
                    }
                    b.fixedContentPos && (b.isIE7 ? a("body, html").css("overflow", "hidden") : n.overflow = "hidden");
                    var r = b.st.mainClass;
                    return b.isIE7 && (r += " mfp-ie7"), r && b._addClassToMFP(r), b.updateItemHTML(), y("BuildControls"), a("html").css(n), b.bgOverlay.add(b.wrap).prependTo(b.st.prependTo || a(document.body)), b._lastFocusedEl = document.activeElement, setTimeout(function() {
                        b.content ? (b._addClassToMFP(q), b._setFocus()) : b.bgOverlay.addClass(q), d.on("focusin" + p, b._onFocusIn)
                    }, 16), b.isOpen = !0, b.updateSize(k), y(m), c
                },
                close: function() {
                    b.isOpen && (y(i), b.isOpen = !1, b.st.removalDelay && !b.isLowIE && b.supportsTransition ? (b._addClassToMFP(r), setTimeout(function() {
                        b._close()
                    }, b.st.removalDelay)) : b._close())
                },
                _close: function() {
                    y(h);
                    var c = r + " " + q + " ";
                    if (b.bgOverlay.detach(), b.wrap.detach(), b.container.empty(), b.st.mainClass && (c += b.st.mainClass + " "), b._removeClassFromMFP(c), b.fixedContentPos) {
                        var e = {
                            marginRight: ""
                        };
                        b.isIE7 ? a("body, html").css("overflow", "") : e.overflow = "", a("html").css(e)
                    }
                    d.off("keyup" + p + " focusin" + p), b.ev.off(p), b.wrap.attr("class", "mfp-wrap").removeAttr("style"), b.bgOverlay.attr("class", "mfp-bg"), b.container.attr("class", "mfp-container"), !b.st.showCloseBtn || b.st.closeBtnInside && b.currTemplate[b.currItem.type] !== !0 || b.currTemplate.closeBtn && b.currTemplate.closeBtn.detach(), b.st.autoFocusLast && b._lastFocusedEl && a(b._lastFocusedEl).focus(), b.currItem = null, b.content = null, b.currTemplate = null, b.prevHeight = 0, y(j)
                },
                updateSize: function(a) {
                    if (b.isIOS) {
                        var c = document.documentElement.clientWidth / window.innerWidth,
                            d = window.innerHeight * c;
                        b.wrap.css("height", d), b.wH = d
                    } else b.wH = a || v.height();
                    b.fixedContentPos || b.wrap.css("height", b.wH), y("Resize")
                },
                updateItemHTML: function() {
                    var c = b.items[b.index];
                    b.contentContainer.detach(), b.content && b.content.detach(), c.parsed || (c = b.parseEl(b.index));
                    var d = c.type;
                    if (y("BeforeChange", [b.currItem ? b.currItem.type : "", d]), b.currItem = c, !b.currTemplate[d]) {
                        var f = b.st[d] ? b.st[d].markup : !1;
                        y("FirstMarkupParse", f), f ? b.currTemplate[d] = a(f) : b.currTemplate[d] = !0
                    }
                    e && e !== c.type && b.container.removeClass("mfp-" + e + "-holder");
                    var g = b["get" + d.charAt(0).toUpperCase() + d.slice(1)](c, b.currTemplate[d]);
                    b.appendContent(g, d), c.preloaded = !0, y(n, c), e = c.type, b.container.prepend(b.contentContainer), y("AfterChange")
                },
                appendContent: function(a, c) {
                    b.content = a, a ? b.st.showCloseBtn && b.st.closeBtnInside && b.currTemplate[c] === !0 ? b.content.find(".mfp-close").length || b.content.append(z()) : b.content = a : b.content = "", y(k), b.container.addClass("mfp-" + c + "-holder"), b.contentContainer.append(b.content)
                },
                parseEl: function(c) {
                    var d, e = b.items[c];
                    if (e.tagName ? e = {
                            el: a(e)
                        } : (d = e.type, e = {
                            data: e,
                            src: e.src
                        }), e.el) {
                        for (var f = b.types, g = 0; g < f.length; g++)
                            if (e.el.hasClass("mfp-" + f[g])) {
                                d = f[g];
                                break
                            }
                        e.src = e.el.attr("data-mfp-src"), e.src || (e.src = e.el.attr("href"))
                    }
                    return e.type = d || b.st.type || "inline", e.index = c, e.parsed = !0, b.items[c] = e, y("ElementParse", e), b.items[c]
                },
                addGroup: function(a, c) {
                    var d = function(d) {
                        d.mfpEl = this, b._openClick(d, a, c)
                    };
                    c || (c = {});
                    var e = "click.magnificPopup";
                    c.mainEl = a, c.items ? (c.isObj = !0, a.off(e).on(e, d)) : (c.isObj = !1, c.delegate ? a.off(e).on(e, c.delegate, d) : (c.items = a, a.off(e).on(e, d)))
                },
                _openClick: function(c, d, e) {
                    var f = void 0 !== e.midClick ? e.midClick : a.magnificPopup.defaults.midClick;
                    if (f || !(2 === c.which || c.ctrlKey || c.metaKey || c.altKey || c.shiftKey)) {
                        var g = void 0 !== e.disableOn ? e.disableOn : a.magnificPopup.defaults.disableOn;
                        if (g)
                            if (a.isFunction(g)) {
                                if (!g.call(b)) return !0
                            } else if (v.width() < g) return !0;
                        c.type && (c.preventDefault(), b.isOpen && c.stopPropagation()), e.el = a(c.mfpEl), e.delegate && (e.items = d.find(e.delegate)), b.open(e)
                    }
                },
                updateStatus: function(a, d) {
                    if (b.preloader) {
                        c !== a && b.container.removeClass("mfp-s-" + c), d || "loading" !== a || (d = b.st.tLoading);
                        var e = {
                            status: a,
                            text: d
                        };
                        y("UpdateStatus", e), a = e.status, d = e.text, b.preloader.html(d), b.preloader.find("a").on("click", function(a) {
                            a.stopImmediatePropagation()
                        }), b.container.addClass("mfp-s-" + a), c = a
                    }
                },
                _checkIfClose: function(c) {
                    if (!a(c).hasClass(s)) {
                        var d = b.st.closeOnContentClick,
                            e = b.st.closeOnBgClick;
                        if (d && e) return !0;
                        if (!b.content || a(c).hasClass("mfp-close") || b.preloader && c === b.preloader[0]) return !0;
                        if (c === b.content[0] || a.contains(b.content[0], c)) {
                            if (d) return !0
                        } else if (e && a.contains(document, c)) return !0;
                        return !1
                    }
                },
                _addClassToMFP: function(a) {
                    b.bgOverlay.addClass(a), b.wrap.addClass(a)
                },
                _removeClassFromMFP: function(a) {
                    this.bgOverlay.removeClass(a), b.wrap.removeClass(a)
                },
                _hasScrollBar: function(a) {
                    return (b.isIE7 ? d.height() : document.body.scrollHeight) > (a || v.height())
                },
                _setFocus: function() {
                    (b.st.focus ? b.content.find(b.st.focus).eq(0) : b.wrap).focus()
                },
                _onFocusIn: function(c) {
                    return c.target === b.wrap[0] || a.contains(b.wrap[0], c.target) ? void 0 : (b._setFocus(), !1)
                },
                _parseMarkup: function(b, c, d) {
                    var e;
                    d.data && (c = a.extend(d.data, c)), y(l, [b, c, d]), a.each(c, function(c, d) {
                        if (void 0 === d || d === !1) return !0;
                        if (e = c.split("_"), e.length > 1) {
                            var f = b.find(p + "-" + e[0]);
                            if (f.length > 0) {
                                var g = e[1];
                                "replaceWith" === g ? f[0] !== d[0] && f.replaceWith(d) : "img" === g ? f.is("img") ? f.attr("src", d) : f.replaceWith(a("<img>").attr("src", d).attr("class", f.attr("class"))) : f.attr(e[1], d)
                            }
                        } else b.find(p + "-" + c).html(d)
                    })
                },
                _getScrollbarSize: function() {
                    if (void 0 === b.scrollbarSize) {
                        var a = document.createElement("div");
                        a.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(a), b.scrollbarSize = a.offsetWidth - a.clientWidth, document.body.removeChild(a)
                    }
                    return b.scrollbarSize
                }
            }, a.magnificPopup = {
                instance: null,
                proto: t.prototype,
                modules: [],
                open: function(b, c) {
                    return A(), b = b ? a.extend(!0, {}, b) : {}, b.isObj = !0, b.index = c || 0, this.instance.open(b)
                },
                close: function() {
                    return a.magnificPopup.instance && a.magnificPopup.instance.close()
                },
                registerModule: function(b, c) {
                    c.options && (a.magnificPopup.defaults[b] = c.options), a.extend(this.proto, c.proto), this.modules.push(b)
                },
                defaults: {
                    disableOn: 0,
                    key: null,
                    midClick: !1,
                    mainClass: "",
                    preloader: !0,
                    focus: "",
                    closeOnContentClick: !1,
                    closeOnBgClick: !0,
                    closeBtnInside: !0,
                    showCloseBtn: !0,
                    enableEscapeKey: !0,
                    modal: !1,
                    alignTop: !1,
                    removalDelay: 0,
                    prependTo: null,
                    fixedContentPos: "auto",
                    fixedBgPos: "auto",
                    overflowY: "auto",
                    closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
                    tClose: "Close (Esc)",
                    tLoading: "Loading...",
                    autoFocusLast: !0
                }
            }, a.fn.magnificPopup = function(c) {
                A();
                var d = a(this);
                if ("string" == typeof c)
                    if ("open" === c) {
                        var e, f = u ? d.data("magnificPopup") : d[0].magnificPopup,
                            g = parseInt(arguments[1], 10) || 0;
                        f.items ? e = f.items[g] : (e = d, f.delegate && (e = e.find(f.delegate)), e = e.eq(g)), b._openClick({
                            mfpEl: e
                        }, d, f)
                    } else b.isOpen && b[c].apply(b, Array.prototype.slice.call(arguments, 1));
                else c = a.extend(!0, {}, c), u ? d.data("magnificPopup", c) : d[0].magnificPopup = c, b.addGroup(d, c);
                return d
            };
            var C, D, E, F = "inline",
                G = function() {
                    E && (D.after(E.addClass(C)).detach(), E = null)
                };
            a.magnificPopup.registerModule(F, {
                options: {
                    hiddenClass: "hide",
                    markup: "",
                    tNotFound: "Content not found"
                },
                proto: {
                    initInline: function() {
                        b.types.push(F), w(h + "." + F, function() {
                            G()
                        })
                    },
                    getInline: function(c, d) {
                        if (G(), c.src) {
                            var e = b.st.inline,
                                f = a(c.src);
                            if (f.length) {
                                var g = f[0].parentNode;
                                g && g.tagName && (D || (C = e.hiddenClass, D = x(C), C = "mfp-" + C), E = f.after(D).detach().removeClass(C)), b.updateStatus("ready")
                            } else b.updateStatus("error", e.tNotFound), f = a("<div>");
                            return c.inlineElement = f, f
                        }
                        return b.updateStatus("ready"), b._parseMarkup(d, {}, c), d
                    }
                }
            });
            var H, I = "ajax",
                J = function() {
                    H && a(document.body).removeClass(H)
                },
                K = function() {
                    J(), b.req && b.req.abort()
                };
            a.magnificPopup.registerModule(I, {
                options: {
                    settings: null,
                    cursor: "mfp-ajax-cur",
                    tError: '<a href="%url%">The content</a> could not be loaded.'
                },
                proto: {
                    initAjax: function() {
                        b.types.push(I), H = b.st.ajax.cursor, w(h + "." + I, K), w("BeforeChange." + I, K)
                    },
                    getAjax: function(c) {
                        H && a(document.body).addClass(H), b.updateStatus("loading");
                        var d = a.extend({
                            url: c.src,
                            success: function(d, e, f) {
                                var g = {
                                    data: d,
                                    xhr: f
                                };
                                y("ParseAjax", g), b.appendContent(a(g.data), I), c.finished = !0, J(), b._setFocus(), setTimeout(function() {
                                    b.wrap.addClass(q)
                                }, 16), b.updateStatus("ready"), y("AjaxContentAdded")
                            },
                            error: function() {
                                J(), c.finished = c.loadError = !0, b.updateStatus("error", b.st.ajax.tError.replace("%url%", c.src))
                            }
                        }, b.st.ajax.settings);
                        return b.req = a.ajax(d), ""
                    }
                }
            });
            var L, M = function(c) {
                if (c.data && void 0 !== c.data.title) return c.data.title;
                var d = b.st.image.titleSrc;
                if (d) {
                    if (a.isFunction(d)) return d.call(b, c);
                    if (c.el) return c.el.attr(d) || ""
                }
                return ""
            };
            a.magnificPopup.registerModule("image", {
                options: {
                    markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
                    cursor: "mfp-zoom-out-cur",
                    titleSrc: "title",
                    verticalFit: !0,
                    tError: '<a href="%url%">The image</a> could not be loaded.'
                },
                proto: {
                    initImage: function() {
                        var c = b.st.image,
                            d = ".image";
                        b.types.push("image"), w(m + d, function() {
                            "image" === b.currItem.type && c.cursor && a(document.body).addClass(c.cursor)
                        }), w(h + d, function() {
                            c.cursor && a(document.body).removeClass(c.cursor), v.off("resize" + p)
                        }), w("Resize" + d, b.resizeImage), b.isLowIE && w("AfterChange", b.resizeImage)
                    },
                    resizeImage: function() {
                        var a = b.currItem;
                        if (a && a.img && b.st.image.verticalFit) {
                            var c = 0;
                            b.isLowIE && (c = parseInt(a.img.css("padding-top"), 10) + parseInt(a.img.css("padding-bottom"), 10)), a.img.css("max-height", b.wH - c)
                        }
                    },
                    _onImageHasSize: function(a) {
                        a.img && (a.hasSize = !0, L && clearInterval(L), a.isCheckingImgSize = !1, y("ImageHasSize", a), a.imgHidden && (b.content && b.content.removeClass("mfp-loading"), a.imgHidden = !1))
                    },
                    findImageSize: function(a) {
                        var c = 0,
                            d = a.img[0],
                            e = function(f) {
                                L && clearInterval(L), L = setInterval(function() {
                                    return d.naturalWidth > 0 ? void b._onImageHasSize(a) : (c > 200 && clearInterval(L), c++, void(3 === c ? e(10) : 40 === c ? e(50) : 100 === c && e(500)))
                                }, f)
                            };
                        e(1)
                    },
                    getImage: function(c, d) {
                        var e = 0,
                            f = function() {
                                c && (c.img[0].complete ? (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("ready")), c.hasSize = !0, c.loaded = !0, y("ImageLoadComplete")) : (e++, 200 > e ? setTimeout(f, 100) : g()))
                            },
                            g = function() {
                                c && (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("error", h.tError.replace("%url%", c.src))), c.hasSize = !0, c.loaded = !0, c.loadError = !0)
                            },
                            h = b.st.image,
                            i = d.find(".mfp-img");
                        if (i.length) {
                            var j = document.createElement("img");
                            j.className = "mfp-img", c.el && c.el.find("img").length && (j.alt = c.el.find("img").attr("alt")), c.img = a(j).on("load.mfploader", f).on("error.mfploader", g), j.src = c.src, i.is("img") && (c.img = c.img.clone()), j = c.img[0], j.naturalWidth > 0 ? c.hasSize = !0 : j.width || (c.hasSize = !1)
                        }
                        return b._parseMarkup(d, {
                            title: M(c),
                            img_replaceWith: c.img
                        }, c), b.resizeImage(), c.hasSize ? (L && clearInterval(L), c.loadError ? (d.addClass("mfp-loading"), b.updateStatus("error", h.tError.replace("%url%", c.src))) : (d.removeClass("mfp-loading"), b.updateStatus("ready")), d) : (b.updateStatus("loading"), c.loading = !0, c.hasSize || (c.imgHidden = !0, d.addClass("mfp-loading"), b.findImageSize(c)), d)
                    }
                }
            });
            var N, O = function() {
                return void 0 === N && (N = void 0 !== document.createElement("p").style.MozTransform), N
            };
            a.magnificPopup.registerModule("zoom", {
                options: {
                    enabled: !1,
                    easing: "ease-in-out",
                    duration: 300,
                    opener: function(a) {
                        return a.is("img") ? a : a.find("img")
                    }
                },
                proto: {
                    initZoom: function() {
                        var a, c = b.st.zoom,
                            d = ".zoom";
                        if (c.enabled && b.supportsTransition) {
                            var e, f, g = c.duration,
                                j = function(a) {
                                    var b = a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
                                        d = "all " + c.duration / 1e3 + "s " + c.easing,
                                        e = {
                                            position: "fixed",
                                            zIndex: 9999,
                                            left: 0,
                                            top: 0,
                                            "-webkit-backface-visibility": "hidden"
                                        },
                                        f = "transition";
                                    return e["-webkit-" + f] = e["-moz-" + f] = e["-o-" + f] = e[f] = d, b.css(e), b
                                },
                                k = function() {
                                    b.content.css("visibility", "visible")
                                };
                            w("BuildControls" + d, function() {
                                if (b._allowZoom()) {
                                    if (clearTimeout(e), b.content.css("visibility", "hidden"), a = b._getItemToZoom(), !a) return void k();
                                    f = j(a), f.css(b._getOffset()), b.wrap.append(f), e = setTimeout(function() {
                                        f.css(b._getOffset(!0)), e = setTimeout(function() {
                                            k(), setTimeout(function() {
                                                f.remove(), a = f = null, y("ZoomAnimationEnded")
                                            }, 16)
                                        }, g)
                                    }, 16)
                                }
                            }), w(i + d, function() {
                                if (b._allowZoom()) {
                                    if (clearTimeout(e), b.st.removalDelay = g, !a) {
                                        if (a = b._getItemToZoom(), !a) return;
                                        f = j(a)
                                    }
                                    f.css(b._getOffset(!0)), b.wrap.append(f), b.content.css("visibility", "hidden"), setTimeout(function() {
                                        f.css(b._getOffset())
                                    }, 16)
                                }
                            }), w(h + d, function() {
                                b._allowZoom() && (k(), f && f.remove(), a = null)
                            })
                        }
                    },
                    _allowZoom: function() {
                        return "image" === b.currItem.type
                    },
                    _getItemToZoom: function() {
                        return b.currItem.hasSize ? b.currItem.img : !1
                    },
                    _getOffset: function(c) {
                        var d;
                        d = c ? b.currItem.img : b.st.zoom.opener(b.currItem.el || b.currItem);
                        var e = d.offset(),
                            f = parseInt(d.css("padding-top"), 10),
                            g = parseInt(d.css("padding-bottom"), 10);
                        e.top -= a(window).scrollTop() - f;
                        var h = {
                            width: d.width(),
                            height: (u ? d.innerHeight() : d[0].offsetHeight) - g - f
                        };
                        return O() ? h["-moz-transform"] = h.transform = "translate(" + e.left + "px," + e.top + "px)" : (h.left = e.left, h.top = e.top), h
                    }
                }
            });
            var P = "iframe",
                Q = "//about:blank",
                R = function(a) {
                    if (b.currTemplate[P]) {
                        var c = b.currTemplate[P].find("iframe");
                        c.length && (a || (c[0].src = Q), b.isIE8 && c.css("display", a ? "block" : "none"))
                    }
                };
            a.magnificPopup.registerModule(P, {
                options: {
                    markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
                    srcAction: "iframe_src",
                    patterns: {
                        youtube: {
                            index: "youtube.com",
                            id: "v=",
                            src: "//www.youtube.com/embed/%id%?autoplay=1"
                        },
                        vimeo: {
                            index: "vimeo.com/",
                            id: "/",
                            src: "//player.vimeo.com/video/%id%?autoplay=1"
                        },
                        gmaps: {
                            index: "//maps.google.",
                            src: "%id%&output=embed"
                        }
                    }
                },
                proto: {
                    initIframe: function() {
                        b.types.push(P), w("BeforeChange", function(a, b, c) {
                            b !== c && (b === P ? R() : c === P && R(!0))
                        }), w(h + "." + P, function() {
                            R()
                        })
                    },
                    getIframe: function(c, d) {
                        var e = c.src,
                            f = b.st.iframe;
                        a.each(f.patterns, function() {
                            return e.indexOf(this.index) > -1 ? (this.id && (e = "string" == typeof this.id ? e.substr(e.lastIndexOf(this.id) + this.id.length, e.length) : this.id.call(this, e)), e = this.src.replace("%id%", e), !1) : void 0
                        });
                        var g = {};
                        return f.srcAction && (g[f.srcAction] = e), b._parseMarkup(d, g, c), b.updateStatus("ready"), d
                    }
                }
            });
            var S = function(a) {
                    var c = b.items.length;
                    return a > c - 1 ? a - c : 0 > a ? c + a : a
                },
                T = function(a, b, c) {
                    return a.replace(/%curr%/gi, b + 1).replace(/%total%/gi, c)
                };
            a.magnificPopup.registerModule("gallery", {
                options: {
                    enabled: !1,
                    arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                    preload: [0, 2],
                    navigateByImgClick: !0,
                    arrows: !0,
                    tPrev: "Previous (Left arrow key)",
                    tNext: "Next (Right arrow key)",
                    tCounter: "%curr% of %total%"
                },
                proto: {
                    initGallery: function() {
                        var c = b.st.gallery,
                            e = ".mfp-gallery";
                        return b.direction = !0, c && c.enabled ? (f += " mfp-gallery", w(m + e, function() {
                            c.navigateByImgClick && b.wrap.on("click" + e, ".mfp-img", function() {
                                return b.items.length > 1 ? (b.next(), !1) : void 0
                            }), d.on("keydown" + e, function(a) {
                                37 === a.keyCode ? b.prev() : 39 === a.keyCode && b.next()
                            })
                        }), w("UpdateStatus" + e, function(a, c) {
                            c.text && (c.text = T(c.text, b.currItem.index, b.items.length))
                        }), w(l + e, function(a, d, e, f) {
                            var g = b.items.length;
                            e.counter = g > 1 ? T(c.tCounter, f.index, g) : ""
                        }), w("BuildControls" + e, function() {
                            if (b.items.length > 1 && c.arrows && !b.arrowLeft) {
                                var d = c.arrowMarkup,
                                    e = b.arrowLeft = a(d.replace(/%title%/gi, c.tPrev).replace(/%dir%/gi, "left")).addClass(s),
                                    f = b.arrowRight = a(d.replace(/%title%/gi, c.tNext).replace(/%dir%/gi, "right")).addClass(s);
                                e.click(function() {
                                    b.prev()
                                }), f.click(function() {
                                    b.next()
                                }), b.container.append(e.add(f))
                            }
                        }), w(n + e, function() {
                            b._preloadTimeout && clearTimeout(b._preloadTimeout), b._preloadTimeout = setTimeout(function() {
                                b.preloadNearbyImages(), b._preloadTimeout = null
                            }, 16)
                        }), void w(h + e, function() {
                            d.off(e), b.wrap.off("click" + e), b.arrowRight = b.arrowLeft = null
                        })) : !1
                    },
                    next: function() {
                        b.direction = !0, b.index = S(b.index + 1), b.updateItemHTML()
                    },
                    prev: function() {
                        b.direction = !1, b.index = S(b.index - 1), b.updateItemHTML()
                    },
                    goTo: function(a) {
                        b.direction = a >= b.index, b.index = a, b.updateItemHTML()
                    },
                    preloadNearbyImages: function() {
                        var a, c = b.st.gallery.preload,
                            d = Math.min(c[0], b.items.length),
                            e = Math.min(c[1], b.items.length);
                        for (a = 1; a <= (b.direction ? e : d); a++) b._preloadItem(b.index + a);
                        for (a = 1; a <= (b.direction ? d : e); a++) b._preloadItem(b.index - a)
                    },
                    _preloadItem: function(c) {
                        if (c = S(c), !b.items[c].preloaded) {
                            var d = b.items[c];
                            d.parsed || (d = b.parseEl(c)), y("LazyLoad", d), "image" === d.type && (d.img = a('<img class="mfp-img" />').on("load.mfploader", function() {
                                d.hasSize = !0
                            }).on("error.mfploader", function() {
                                d.hasSize = !0, d.loadError = !0, y("LazyLoadError", d)
                            }).attr("src", d.src)), d.preloaded = !0
                        }
                    }
                }
            });
            var U = "retina";
            a.magnificPopup.registerModule(U, {
                options: {
                    replaceSrc: function(a) {
                        return a.src.replace(/\.\w+$/, function(a) {
                            return "@2x" + a
                        })
                    },
                    ratio: 1
                },
                proto: {
                    initRetina: function() {
                        if (window.devicePixelRatio > 1) {
                            var a = b.st.retina,
                                c = a.ratio;
                            c = isNaN(c) ? c() : c, c > 1 && (w("ImageHasSize." + U, function(a, b) {
                                b.img.css({
                                    "max-width": b.img[0].naturalWidth / c,
                                    width: "100%"
                                })
                            }), w("ElementParse." + U, function(b, d) {
                                d.src = a.replaceSrc(d, c)
                            }))
                        }
                    }
                }
            }), A()
        })
    </script>
    <script>
        var $jscomp = {
            scope: {},
            findInternal: function(a, l, d) {
                a instanceof String && (a = String(a));
                for (var p = a.length, h = 0; h < p; h++) {
                    var b = a[h];
                    if (l.call(d, b, h, a)) return {
                        i: h,
                        v: b
                    }
                }
                return {
                    i: -1,
                    v: void 0
                }
            }
        };
        $jscomp.defineProperty = "function" == typeof Object.defineProperties ? Object.defineProperty : function(a, l, d) {
            if (d.get || d.set) throw new TypeError("ES3 does not support getters and setters.");
            a != Array.prototype && a != Object.prototype && (a[l] = d.value)
        };
        $jscomp.getGlobal = function(a) {
            return "undefined" != typeof window && window === a ? a : "undefined" != typeof global && null != global ? global : a
        };
        $jscomp.global = $jscomp.getGlobal(this);
        $jscomp.polyfill = function(a, l, d, p) {
            if (l) {
                d = $jscomp.global;
                a = a.split(".");
                for (p = 0; p < a.length - 1; p++) {
                    var h = a[p];
                    h in d || (d[h] = {});
                    d = d[h]
                }
                a = a[a.length - 1];
                p = d[a];
                l = l(p);
                l != p && null != l && $jscomp.defineProperty(d, a, {
                    configurable: !0,
                    writable: !0,
                    value: l
                })
            }
        };
        $jscomp.polyfill("Array.prototype.find", function(a) {
            return a ? a : function(a, d) {
                return $jscomp.findInternal(this, a, d).v
            }
        }, "es6-impl", "es3");
        (function(a, l, d) {
            "function" === typeof define && define.amd ? define(["jquery"], a) : "object" === typeof exports ? module.exports = a(require("jquery")) : a(l || d)
        })(function(a) {
            var l = function(b, e, f) {
                var c = {
                    invalid: [],
                    getCaret: function() {
                        try {
                            var a, r = 0,
                                g = b.get(0),
                                e = document.selection,
                                f = g.selectionStart;
                            if (e && -1 === navigator.appVersion.indexOf("MSIE 10")) a = e.createRange(), a.moveStart("character", -c.val().length), r = a.text.length;
                            else if (f || "0" === f) r = f;
                            return r
                        } catch (C) {}
                    },
                    setCaret: function(a) {
                        try {
                            if (b.is(":focus")) {
                                var c, g = b.get(0);
                                g.setSelectionRange ? g.setSelectionRange(a, a) : (c = g.createTextRange(), c.collapse(!0), c.moveEnd("character", a), c.moveStart("character", a), c.select())
                            }
                        } catch (B) {}
                    },
                    events: function() {
                        b.on("keydown.mask", function(a) {
                            b.data("mask-keycode", a.keyCode || a.which);
                            b.data("mask-previus-value", b.val());
                            b.data("mask-previus-caret-pos", c.getCaret());
                            c.maskDigitPosMapOld = c.maskDigitPosMap
                        }).on(a.jMaskGlobals.useInput ? "input.mask" : "keyup.mask", c.behaviour).on("paste.mask drop.mask", function() {
                            setTimeout(function() {
                                b.keydown().keyup()
                            }, 100)
                        }).on("change.mask", function() {
                            b.data("changed", !0)
                        }).on("blur.mask", function() {
                            d === c.val() || b.data("changed") || b.trigger("change");
                            b.data("changed", !1)
                        }).on("blur.mask", function() {
                            d = c.val()
                        }).on("focus.mask", function(b) {
                            !0 === f.selectOnFocus && a(b.target).select()
                        }).on("focusout.mask", function() {
                            f.clearIfNotMatch && !h.test(c.val()) && c.val("")
                        })
                    },
                    getRegexMask: function() {
                        for (var a = [], b, c, f, n, d = 0; d < e.length; d++)(b = m.translation[e.charAt(d)]) ? (c = b.pattern.toString().replace(/.{1}$|^.{1}/g, ""), f = b.optional, (b = b.recursive) ? (a.push(e.charAt(d)), n = {
                            digit: e.charAt(d),
                            pattern: c
                        }) : a.push(f || b ? c + "?" : c)) : a.push(e.charAt(d).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&"));
                        a = a.join("");
                        n && (a = a.replace(new RegExp("(" + n.digit + "(.*" + n.digit + ")?)"), "($1)?").replace(new RegExp(n.digit, "g"), n.pattern));
                        return new RegExp(a)
                    },
                    destroyEvents: function() {
                        b.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask "))
                    },
                    val: function(a) {
                        var c = b.is("input") ? "val" : "text";
                        if (0 < arguments.length) {
                            if (b[c]() !== a) b[c](a);
                            c = b
                        } else c = b[c]();
                        return c
                    },
                    calculateCaretPosition: function() {
                        var a = b.data("mask-previus-value") || "",
                            e = c.getMasked(),
                            g = c.getCaret();
                        if (a !== e) {
                            var f = b.data("mask-previus-caret-pos") || 0,
                                e = e.length,
                                d = a.length,
                                m = a = 0,
                                h = 0,
                                l = 0,
                                k;
                            for (k = g; k < e && c.maskDigitPosMap[k]; k++) m++;
                            for (k = g - 1; 0 <= k && c.maskDigitPosMap[k]; k--) a++;
                            for (k = g - 1; 0 <= k; k--) c.maskDigitPosMap[k] && h++;
                            for (k = f - 1; 0 <= k; k--) c.maskDigitPosMapOld[k] && l++;
                            g > d ? g = 10 * e : f >= g && f !== d ? c.maskDigitPosMapOld[g] || (f = g, g = g - (l - h) - a, c.maskDigitPosMap[g] && (g = f)) : g > f && (g = g + (h - l) + m)
                        }
                        return g
                    },
                    behaviour: function(f) {
                        f = f || window.event;
                        c.invalid = [];
                        var e = b.data("mask-keycode");
                        if (-1 === a.inArray(e, m.byPassKeys)) {
                            var e = c.getMasked(),
                                g = c.getCaret();
                            setTimeout(function() {
                                c.setCaret(c.calculateCaretPosition())
                            }, 10);
                            c.val(e);
                            c.setCaret(g);
                            return c.callbacks(f)
                        }
                    },
                    getMasked: function(a, b) {
                        var g = [],
                            d = void 0 === b ? c.val() : b + "",
                            n = 0,
                            h = e.length,
                            q = 0,
                            l = d.length,
                            k = 1,
                            r = "push",
                            p = -1,
                            t = 0,
                            y = [],
                            v, z;
                        f.reverse ? (r = "unshift", k = -1, v = 0, n = h - 1, q = l - 1, z = function() {
                            return -1 < n && -1 < q
                        }) : (v = h - 1, z = function() {
                            return n < h && q < l
                        });
                        for (var A; z();) {
                            var x = e.charAt(n),
                                w = d.charAt(q),
                                u = m.translation[x];
                            if (u) w.match(u.pattern) ? (g[r](w), u.recursive && (-1 === p ? p = n : n === v && n !== p && (n = p - k), v === p && (n -= k)), n += k) : w === A ? (t--, A = void 0) : u.optional ? (n += k, q -= k) : u.fallback ? (g[r](u.fallback), n += k, q -= k) : c.invalid.push({
                                p: q,
                                v: w,
                                e: u.pattern
                            }), q += k;
                            else {
                                if (!a) g[r](x);
                                w === x ? (y.push(q), q += k) : (A = x, y.push(q + t), t++);
                                n += k
                            }
                        }
                        d = e.charAt(v);
                        h !== l + 1 || m.translation[d] || g.push(d);
                        g = g.join("");
                        c.mapMaskdigitPositions(g, y, l);
                        return g
                    },
                    mapMaskdigitPositions: function(a, b, e) {
                        a = f.reverse ? a.length - e : 0;
                        c.maskDigitPosMap = {};
                        for (e = 0; e < b.length; e++) c.maskDigitPosMap[b[e] + a] = 1
                    },
                    callbacks: function(a) {
                        var h = c.val(),
                            g = h !== d,
                            m = [h, a, b, f],
                            q = function(a, b, c) {
                                "function" === typeof f[a] && b && f[a].apply(this, c)
                            };
                        q("onChange", !0 === g, m);
                        q("onKeyPress", !0 === g, m);
                        q("onComplete", h.length === e.length, m);
                        q("onInvalid", 0 < c.invalid.length, [h, a, b, c.invalid, f])
                    }
                };
                b = a(b);
                var m = this,
                    d = c.val(),
                    h;
                e = "function" === typeof e ? e(c.val(), void 0, b, f) : e;
                m.mask = e;
                m.options = f;
                m.remove = function() {
                    var a = c.getCaret();
                    c.destroyEvents();
                    c.val(m.getCleanVal());
                    c.setCaret(a);
                    return b
                };
                m.getCleanVal = function() {
                    return c.getMasked(!0)
                };
                m.getMaskedVal = function(a) {
                    return c.getMasked(!1, a)
                };
                m.init = function(d) {
                    d = d || !1;
                    f = f || {};
                    m.clearIfNotMatch = a.jMaskGlobals.clearIfNotMatch;
                    m.byPassKeys = a.jMaskGlobals.byPassKeys;
                    m.translation = a.extend({}, a.jMaskGlobals.translation, f.translation);
                    m = a.extend(!0, {}, m, f);
                    h = c.getRegexMask();
                    if (d) c.events(), c.val(c.getMasked());
                    else {
                        f.placeholder && b.attr("placeholder", f.placeholder);
                        b.data("mask") && b.attr("autocomplete", "off");
                        d = 0;
                        for (var l = !0; d < e.length; d++) {
                            var g = m.translation[e.charAt(d)];
                            if (g && g.recursive) {
                                l = !1;
                                break
                            }
                        }
                        l && b.attr("maxlength", e.length);
                        c.destroyEvents();
                        c.events();
                        d = c.getCaret();
                        c.val(c.getMasked());
                        c.setCaret(d)
                    }
                };
                m.init(!b.is("input"))
            };
            a.maskWatchers = {};
            var d = function() {
                    var b = a(this),
                        e = {},
                        f = b.attr("data-mask");
                    b.attr("data-mask-reverse") && (e.reverse = !0);
                    b.attr("data-mask-clearifnotmatch") && (e.clearIfNotMatch = !0);
                    "true" === b.attr("data-mask-selectonfocus") && (e.selectOnFocus = !0);
                    if (p(b, f, e)) return b.data("mask", new l(this, f, e))
                },
                p = function(b, e, f) {
                    f = f || {};
                    var c = a(b).data("mask"),
                        d = JSON.stringify;
                    b = a(b).val() || a(b).text();
                    try {
                        return "function" === typeof e && (e = e(b)), "object" !== typeof c || d(c.options) !== d(f) || c.mask !== e
                    } catch (t) {}
                },
                h = function(a) {
                    var b = document.createElement("div"),
                        d;
                    a = "on" + a;
                    d = a in b;
                    d || (b.setAttribute(a, "return;"), d = "function" === typeof b[a]);
                    return d
                };
            a.fn.mask = function(b, d) {
                d = d || {};
                var e = this.selector,
                    c = a.jMaskGlobals,
                    h = c.watchInterval,
                    c = d.watchInputs || c.watchInputs,
                    t = function() {
                        if (p(this, b, d)) return a(this).data("mask", new l(this, b, d))
                    };
                a(this).each(t);
                e && "" !== e && c && (clearInterval(a.maskWatchers[e]), a.maskWatchers[e] = setInterval(function() {
                    a(document).find(e).each(t)
                }, h));
                return this
            };
            a.fn.masked = function(a) {
                return this.data("mask").getMaskedVal(a)
            };
            a.fn.unmask = function() {
                clearInterval(a.maskWatchers[this.selector]);
                delete a.maskWatchers[this.selector];
                return this.each(function() {
                    var b = a(this).data("mask");
                    b && b.remove().removeData("mask")
                })
            };
            a.fn.cleanVal = function() {
                return this.data("mask").getCleanVal()
            };
            a.applyDataMask = function(b) {
                b = b || a.jMaskGlobals.maskElements;
                (b instanceof a ? b : a(b)).filter(a.jMaskGlobals.dataMaskAttr).each(d)
            };
            h = {
                maskElements: "input,td,span,div",
                dataMaskAttr: "*[data-mask]",
                dataMask: !0,
                watchInterval: 300,
                watchInputs: !0,
                useInput: !/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent) && h("input"),
                watchDataMask: !1,
                byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
                translation: {
                    0: {
                        pattern: /\d/
                    },
                    9: {
                        pattern: /\d/,
                        optional: !0
                    },
                    "#": {
                        pattern: /\d/,
                        recursive: !0
                    },
                    A: {
                        pattern: /[a-zA-Z0-9]/
                    },
                    S: {
                        pattern: /[a-zA-Z]/
                    }
                }
            };
            a.jMaskGlobals = a.jMaskGlobals || {};
            h = a.jMaskGlobals = a.extend(!0, {}, h, a.jMaskGlobals);
            h.dataMask && a.applyDataMask();
            setInterval(function() {
                a.jMaskGlobals.watchDataMask && a.applyDataMask()
            }, h.watchInterval)
        }, window.jQuery, window.Zepto)
    </script>
    <script>
        ! function(e, t) {
            "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.Swiper = t()
        }(this, function() {
            "use strict";
            var e = "undefined" == typeof document ? {
                    body: {},
                    addEventListener: function() {},
                    removeEventListener: function() {},
                    activeElement: {
                        blur: function() {},
                        nodeName: ""
                    },
                    querySelector: function() {
                        return null
                    },
                    querySelectorAll: function() {
                        return []
                    },
                    getElementById: function() {
                        return null
                    },
                    createEvent: function() {
                        return {
                            initEvent: function() {}
                        }
                    },
                    createElement: function() {
                        return {
                            children: [],
                            childNodes: [],
                            style: {},
                            setAttribute: function() {},
                            getElementsByTagName: function() {
                                return []
                            }
                        }
                    },
                    location: {
                        hash: ""
                    }
                } : document,
                t = "undefined" == typeof window ? {
                    document: e,
                    navigator: {
                        userAgent: ""
                    },
                    location: {},
                    history: {},
                    CustomEvent: function() {
                        return this
                    },
                    addEventListener: function() {},
                    removeEventListener: function() {},
                    getComputedStyle: function() {
                        return {
                            getPropertyValue: function() {
                                return ""
                            }
                        }
                    },
                    Image: function() {},
                    Date: function() {},
                    screen: {},
                    setTimeout: function() {},
                    clearTimeout: function() {}
                } : window,
                i = function(e) {
                    for (var t = 0; t < e.length; t += 1) this[t] = e[t];
                    return this.length = e.length, this
                };

            function s(s, a) {
                var r = [],
                    n = 0;
                if (s && !a && s instanceof i) return s;
                if (s)
                    if ("string" == typeof s) {
                        var o, l, d = s.trim();
                        if (d.indexOf("<") >= 0 && d.indexOf(">") >= 0) {
                            var h = "div";
                            for (0 === d.indexOf("<li") && (h = "ul"), 0 === d.indexOf("<tr") && (h = "tbody"), 0 !== d.indexOf("<td") && 0 !== d.indexOf("<th") || (h = "tr"), 0 === d.indexOf("<tbody") && (h = "table"), 0 === d.indexOf("<option") && (h = "select"), (l = e.createElement(h)).innerHTML = d, n = 0; n < l.childNodes.length; n += 1) r.push(l.childNodes[n])
                        } else
                            for (o = a || "#" !== s[0] || s.match(/[ .<>:~]/) ? (a || e).querySelectorAll(s.trim()) : [e.getElementById(s.trim().split("#")[1])], n = 0; n < o.length; n += 1) o[n] && r.push(o[n])
                    } else if (s.nodeType || s === t || s === e) r.push(s);
                else if (s.length > 0 && s[0].nodeType)
                    for (n = 0; n < s.length; n += 1) r.push(s[n]);
                return new i(r)
            }

            function a(e) {
                for (var t = [], i = 0; i < e.length; i += 1) - 1 === t.indexOf(e[i]) && t.push(e[i]);
                return t
            }
            s.fn = i.prototype, s.Class = i, s.Dom7 = i;
            "resize scroll".split(" ");
            var r = {
                addClass: function(e) {
                    if (void 0 === e) return this;
                    for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                        for (var s = 0; s < this.length; s += 1) void 0 !== this[s].classList && this[s].classList.add(t[i]);
                    return this
                },
                removeClass: function(e) {
                    for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                        for (var s = 0; s < this.length; s += 1) void 0 !== this[s].classList && this[s].classList.remove(t[i]);
                    return this
                },
                hasClass: function(e) {
                    return !!this[0] && this[0].classList.contains(e)
                },
                toggleClass: function(e) {
                    for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                        for (var s = 0; s < this.length; s += 1) void 0 !== this[s].classList && this[s].classList.toggle(t[i]);
                    return this
                },
                attr: function(e, t) {
                    var i = arguments;
                    if (1 === arguments.length && "string" == typeof e) return this[0] ? this[0].getAttribute(e) : void 0;
                    for (var s = 0; s < this.length; s += 1)
                        if (2 === i.length) this[s].setAttribute(e, t);
                        else
                            for (var a in e) this[s][a] = e[a], this[s].setAttribute(a, e[a]);
                    return this
                },
                removeAttr: function(e) {
                    for (var t = 0; t < this.length; t += 1) this[t].removeAttribute(e);
                    return this
                },
                data: function(e, t) {
                    var i;
                    if (void 0 !== t) {
                        for (var s = 0; s < this.length; s += 1)(i = this[s]).dom7ElementDataStorage || (i.dom7ElementDataStorage = {}), i.dom7ElementDataStorage[e] = t;
                        return this
                    }
                    if (i = this[0]) {
                        if (i.dom7ElementDataStorage && e in i.dom7ElementDataStorage) return i.dom7ElementDataStorage[e];
                        var a = i.getAttribute("data-" + e);
                        return a || void 0
                    }
                },
                transform: function(e) {
                    for (var t = 0; t < this.length; t += 1) {
                        var i = this[t].style;
                        i.webkitTransform = e, i.transform = e
                    }
                    return this
                },
                transition: function(e) {
                    "string" != typeof e && (e += "ms");
                    for (var t = 0; t < this.length; t += 1) {
                        var i = this[t].style;
                        i.webkitTransitionDuration = e, i.transitionDuration = e
                    }
                    return this
                },
                on: function() {
                    for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                    var i, a = e[0],
                        r = e[1],
                        n = e[2],
                        o = e[3];

                    function l(e) {
                        var t = e.target;
                        if (t) {
                            var i = e.target.dom7EventData || [];
                            if (i.unshift(e), s(t).is(r)) n.apply(t, i);
                            else
                                for (var a = s(t).parents(), o = 0; o < a.length; o += 1) s(a[o]).is(r) && n.apply(a[o], i)
                        }
                    }

                    function d(e) {
                        var t = e && e.target ? e.target.dom7EventData || [] : [];
                        t.unshift(e), n.apply(this, t)
                    }
                    "function" == typeof e[1] && (a = (i = e)[0], n = i[1], o = i[2], r = void 0), o || (o = !1);
                    for (var h, p = a.split(" "), c = 0; c < this.length; c += 1) {
                        var u = this[c];
                        if (r)
                            for (h = 0; h < p.length; h += 1) u.dom7LiveListeners || (u.dom7LiveListeners = []), u.dom7LiveListeners.push({
                                type: a,
                                listener: n,
                                proxyListener: l
                            }), u.addEventListener(p[h], l, o);
                        else
                            for (h = 0; h < p.length; h += 1) u.dom7Listeners || (u.dom7Listeners = []), u.dom7Listeners.push({
                                type: a,
                                listener: n,
                                proxyListener: d
                            }), u.addEventListener(p[h], d, o)
                    }
                    return this
                },
                off: function() {
                    for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                    var i, s = e[0],
                        a = e[1],
                        r = e[2],
                        n = e[3];
                    "function" == typeof e[1] && (s = (i = e)[0], r = i[1], n = i[2], a = void 0), n || (n = !1);
                    for (var o = s.split(" "), l = 0; l < o.length; l += 1)
                        for (var d = 0; d < this.length; d += 1) {
                            var h = this[d];
                            if (a) {
                                if (h.dom7LiveListeners)
                                    for (var p = 0; p < h.dom7LiveListeners.length; p += 1) r ? h.dom7LiveListeners[p].listener === r && h.removeEventListener(o[l], h.dom7LiveListeners[p].proxyListener, n) : h.dom7LiveListeners[p].type === o[l] && h.removeEventListener(o[l], h.dom7LiveListeners[p].proxyListener, n)
                            } else if (h.dom7Listeners)
                                for (var c = 0; c < h.dom7Listeners.length; c += 1) r ? h.dom7Listeners[c].listener === r && h.removeEventListener(o[l], h.dom7Listeners[c].proxyListener, n) : h.dom7Listeners[c].type === o[l] && h.removeEventListener(o[l], h.dom7Listeners[c].proxyListener, n)
                        }
                    return this
                },
                trigger: function() {
                    for (var i = [], s = arguments.length; s--;) i[s] = arguments[s];
                    for (var a = i[0].split(" "), r = i[1], n = 0; n < a.length; n += 1)
                        for (var o = 0; o < this.length; o += 1) {
                            var l = void 0;
                            try {
                                l = new t.CustomEvent(a[n], {
                                    detail: r,
                                    bubbles: !0,
                                    cancelable: !0
                                })
                            } catch (t) {
                                (l = e.createEvent("Event")).initEvent(a[n], !0, !0), l.detail = r
                            }
                            this[o].dom7EventData = i.filter(function(e, t) {
                                return t > 0
                            }), this[o].dispatchEvent(l), this[o].dom7EventData = [], delete this[o].dom7EventData
                        }
                    return this
                },
                transitionEnd: function(e) {
                    var t, i = ["webkitTransitionEnd", "transitionend"],
                        s = this;

                    function a(r) {
                        if (r.target === this)
                            for (e.call(this, r), t = 0; t < i.length; t += 1) s.off(i[t], a)
                    }
                    if (e)
                        for (t = 0; t < i.length; t += 1) s.on(i[t], a);
                    return this
                },
                outerWidth: function(e) {
                    if (this.length > 0) {
                        if (e) {
                            var t = this.styles();
                            return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left"))
                        }
                        return this[0].offsetWidth
                    }
                    return null
                },
                outerHeight: function(e) {
                    if (this.length > 0) {
                        if (e) {
                            var t = this.styles();
                            return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom"))
                        }
                        return this[0].offsetHeight
                    }
                    return null
                },
                offset: function() {
                    if (this.length > 0) {
                        var i = this[0],
                            s = i.getBoundingClientRect(),
                            a = e.body,
                            r = i.clientTop || a.clientTop || 0,
                            n = i.clientLeft || a.clientLeft || 0,
                            o = i === t ? t.scrollY : i.scrollTop,
                            l = i === t ? t.scrollX : i.scrollLeft;
                        return {
                            top: s.top + o - r,
                            left: s.left + l - n
                        }
                    }
                    return null
                },
                css: function(e, i) {
                    var s;
                    if (1 === arguments.length) {
                        if ("string" != typeof e) {
                            for (s = 0; s < this.length; s += 1)
                                for (var a in e) this[s].style[a] = e[a];
                            return this
                        }
                        if (this[0]) return t.getComputedStyle(this[0], null).getPropertyValue(e)
                    }
                    if (2 === arguments.length && "string" == typeof e) {
                        for (s = 0; s < this.length; s += 1) this[s].style[e] = i;
                        return this
                    }
                    return this
                },
                each: function(e) {
                    if (!e) return this;
                    for (var t = 0; t < this.length; t += 1)
                        if (!1 === e.call(this[t], t, this[t])) return this;
                    return this
                },
                html: function(e) {
                    if (void 0 === e) return this[0] ? this[0].innerHTML : void 0;
                    for (var t = 0; t < this.length; t += 1) this[t].innerHTML = e;
                    return this
                },
                text: function(e) {
                    if (void 0 === e) return this[0] ? this[0].textContent.trim() : null;
                    for (var t = 0; t < this.length; t += 1) this[t].textContent = e;
                    return this
                },
                is: function(a) {
                    var r, n, o = this[0];
                    if (!o || void 0 === a) return !1;
                    if ("string" == typeof a) {
                        if (o.matches) return o.matches(a);
                        if (o.webkitMatchesSelector) return o.webkitMatchesSelector(a);
                        if (o.msMatchesSelector) return o.msMatchesSelector(a);
                        for (r = s(a), n = 0; n < r.length; n += 1)
                            if (r[n] === o) return !0;
                        return !1
                    }
                    if (a === e) return o === e;
                    if (a === t) return o === t;
                    if (a.nodeType || a instanceof i) {
                        for (r = a.nodeType ? [a] : a, n = 0; n < r.length; n += 1)
                            if (r[n] === o) return !0;
                        return !1
                    }
                    return !1
                },
                index: function() {
                    var e, t = this[0];
                    if (t) {
                        for (e = 0; null !== (t = t.previousSibling);) 1 === t.nodeType && (e += 1);
                        return e
                    }
                },
                eq: function(e) {
                    if (void 0 === e) return this;
                    var t, s = this.length;
                    return new i(e > s - 1 ? [] : e < 0 ? (t = s + e) < 0 ? [] : [this[t]] : [this[e]])
                },
                append: function() {
                    for (var t, s = [], a = arguments.length; a--;) s[a] = arguments[a];
                    for (var r = 0; r < s.length; r += 1) {
                        t = s[r];
                        for (var n = 0; n < this.length; n += 1)
                            if ("string" == typeof t) {
                                var o = e.createElement("div");
                                for (o.innerHTML = t; o.firstChild;) this[n].appendChild(o.firstChild)
                            } else if (t instanceof i)
                            for (var l = 0; l < t.length; l += 1) this[n].appendChild(t[l]);
                        else this[n].appendChild(t)
                    }
                    return this
                },
                prepend: function(t) {
                    var s, a;
                    for (s = 0; s < this.length; s += 1)
                        if ("string" == typeof t) {
                            var r = e.createElement("div");
                            for (r.innerHTML = t, a = r.childNodes.length - 1; a >= 0; a -= 1) this[s].insertBefore(r.childNodes[a], this[s].childNodes[0])
                        } else if (t instanceof i)
                        for (a = 0; a < t.length; a += 1) this[s].insertBefore(t[a], this[s].childNodes[0]);
                    else this[s].insertBefore(t, this[s].childNodes[0]);
                    return this
                },
                next: function(e) {
                    return this.length > 0 ? e ? this[0].nextElementSibling && s(this[0].nextElementSibling).is(e) ? new i([this[0].nextElementSibling]) : new i([]) : this[0].nextElementSibling ? new i([this[0].nextElementSibling]) : new i([]) : new i([])
                },
                nextAll: function(e) {
                    var t = [],
                        a = this[0];
                    if (!a) return new i([]);
                    for (; a.nextElementSibling;) {
                        var r = a.nextElementSibling;
                        e ? s(r).is(e) && t.push(r) : t.push(r), a = r
                    }
                    return new i(t)
                },
                prev: function(e) {
                    if (this.length > 0) {
                        var t = this[0];
                        return e ? t.previousElementSibling && s(t.previousElementSibling).is(e) ? new i([t.previousElementSibling]) : new i([]) : t.previousElementSibling ? new i([t.previousElementSibling]) : new i([])
                    }
                    return new i([])
                },
                prevAll: function(e) {
                    var t = [],
                        a = this[0];
                    if (!a) return new i([]);
                    for (; a.previousElementSibling;) {
                        var r = a.previousElementSibling;
                        e ? s(r).is(e) && t.push(r) : t.push(r), a = r
                    }
                    return new i(t)
                },
                parent: function(e) {
                    for (var t = [], i = 0; i < this.length; i += 1) null !== this[i].parentNode && (e ? s(this[i].parentNode).is(e) && t.push(this[i].parentNode) : t.push(this[i].parentNode));
                    return s(a(t))
                },
                parents: function(e) {
                    for (var t = [], i = 0; i < this.length; i += 1)
                        for (var r = this[i].parentNode; r;) e ? s(r).is(e) && t.push(r) : t.push(r), r = r.parentNode;
                    return s(a(t))
                },
                closest: function(e) {
                    var t = this;
                    return void 0 === e ? new i([]) : (t.is(e) || (t = t.parents(e).eq(0)), t)
                },
                find: function(e) {
                    for (var t = [], s = 0; s < this.length; s += 1)
                        for (var a = this[s].querySelectorAll(e), r = 0; r < a.length; r += 1) t.push(a[r]);
                    return new i(t)
                },
                children: function(e) {
                    for (var t = [], r = 0; r < this.length; r += 1)
                        for (var n = this[r].childNodes, o = 0; o < n.length; o += 1) e ? 1 === n[o].nodeType && s(n[o]).is(e) && t.push(n[o]) : 1 === n[o].nodeType && t.push(n[o]);
                    return new i(a(t))
                },
                remove: function() {
                    for (var e = 0; e < this.length; e += 1) this[e].parentNode && this[e].parentNode.removeChild(this[e]);
                    return this
                },
                add: function() {
                    for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                    var i, a;
                    for (i = 0; i < e.length; i += 1) {
                        var r = s(e[i]);
                        for (a = 0; a < r.length; a += 1) this[this.length] = r[a], this.length += 1
                    }
                    return this
                },
                styles: function() {
                    return this[0] ? t.getComputedStyle(this[0], null) : {}
                }
            };
            Object.keys(r).forEach(function(e) {
                s.fn[e] = r[e]
            });
            var n, o, l, d = {
                    deleteProps: function(e) {
                        var t = e;
                        Object.keys(t).forEach(function(e) {
                            try {
                                t[e] = null
                            } catch (e) {}
                            try {
                                delete t[e]
                            } catch (e) {}
                        })
                    },
                    nextTick: function(e, t) {
                        return void 0 === t && (t = 0), setTimeout(e, t)
                    },
                    now: function() {
                        return Date.now()
                    },
                    getTranslate: function(e, i) {
                        var s, a, r;
                        void 0 === i && (i = "x");
                        var n = t.getComputedStyle(e, null);
                        return t.WebKitCSSMatrix ? ((a = n.transform || n.webkitTransform).split(",").length > 6 && (a = a.split(", ").map(function(e) {
                            return e.replace(",", ".")
                        }).join(", ")), r = new t.WebKitCSSMatrix("none" === a ? "" : a)) : s = (r = n.MozTransform || n.OTransform || n.MsTransform || n.msTransform || n.transform || n.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === i && (a = t.WebKitCSSMatrix ? r.m41 : 16 === s.length ? parseFloat(s[12]) : parseFloat(s[4])), "y" === i && (a = t.WebKitCSSMatrix ? r.m42 : 16 === s.length ? parseFloat(s[13]) : parseFloat(s[5])), a || 0
                    },
                    parseUrlQuery: function(e) {
                        var i, s, a, r, n = {},
                            o = e || t.location.href;
                        if ("string" == typeof o && o.length)
                            for (r = (s = (o = o.indexOf("?") > -1 ? o.replace(/\S*\?/, "") : "").split("&").filter(function(e) {
                                    return "" !== e
                                })).length, i = 0; i < r; i += 1) a = s[i].replace(/#\S+/g, "").split("="), n[decodeURIComponent(a[0])] = void 0 === a[1] ? void 0 : decodeURIComponent(a[1]) || "";
                        return n
                    },
                    isObject: function(e) {
                        return "object" == typeof e && null !== e && e.constructor && e.constructor === Object
                    },
                    extend: function() {
                        for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                        for (var i = Object(e[0]), s = 1; s < e.length; s += 1) {
                            var a = e[s];
                            if (void 0 !== a && null !== a)
                                for (var r = Object.keys(Object(a)), n = 0, o = r.length; n < o; n += 1) {
                                    var l = r[n],
                                        h = Object.getOwnPropertyDescriptor(a, l);
                                    void 0 !== h && h.enumerable && (d.isObject(i[l]) && d.isObject(a[l]) ? d.extend(i[l], a[l]) : !d.isObject(i[l]) && d.isObject(a[l]) ? (i[l] = {}, d.extend(i[l], a[l])) : i[l] = a[l])
                                }
                        }
                        return i
                    }
                },
                h = (l = e.createElement("div"), {
                    touch: t.Modernizr && !0 === t.Modernizr.touch || !!("ontouchstart" in t || t.DocumentTouch && e instanceof t.DocumentTouch),
                    pointerEvents: !(!t.navigator.pointerEnabled && !t.PointerEvent),
                    prefixedPointerEvents: !!t.navigator.msPointerEnabled,
                    transition: (o = l.style, "transition" in o || "webkitTransition" in o || "MozTransition" in o),
                    transforms3d: t.Modernizr && !0 === t.Modernizr.csstransforms3d || (n = l.style, "webkitPerspective" in n || "MozPerspective" in n || "OPerspective" in n || "MsPerspective" in n || "perspective" in n),
                    flexbox: function() {
                        for (var e = l.style, t = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), i = 0; i < t.length; i += 1)
                            if (t[i] in e) return !0;
                        return !1
                    }(),
                    observer: "MutationObserver" in t || "WebkitMutationObserver" in t,
                    passiveListener: function() {
                        var e = !1;
                        try {
                            var i = Object.defineProperty({}, "passive", {
                                get: function() {
                                    e = !0
                                }
                            });
                            t.addEventListener("testPassiveListener", null, i)
                        } catch (e) {}
                        return e
                    }(),
                    gestures: "ongesturestart" in t
                }),
                p = function(e) {
                    void 0 === e && (e = {});
                    var t = this;
                    t.params = e, t.eventsListeners = {}, t.params && t.params.on && Object.keys(t.params.on).forEach(function(e) {
                        t.on(e, t.params.on[e])
                    })
                },
                c = {
                    components: {
                        configurable: !0
                    }
                };
            p.prototype.on = function(e, t) {
                var i = this;
                return "function" != typeof t ? i : (e.split(" ").forEach(function(e) {
                    i.eventsListeners[e] || (i.eventsListeners[e] = []), i.eventsListeners[e].push(t)
                }), i)
            }, p.prototype.once = function(e, t) {
                var i = this;
                if ("function" != typeof t) return i;
                return i.on(e, function s() {
                    for (var a = [], r = arguments.length; r--;) a[r] = arguments[r];
                    t.apply(i, a), i.off(e, s)
                })
            }, p.prototype.off = function(e, t) {
                var i = this;
                return e.split(" ").forEach(function(e) {
                    void 0 === t ? i.eventsListeners[e] = [] : i.eventsListeners[e].forEach(function(s, a) {
                        s === t && i.eventsListeners[e].splice(a, 1)
                    })
                }), i
            }, p.prototype.emit = function() {
                for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                var i, s, a, r = this;
                return r.eventsListeners ? ("string" == typeof e[0] || Array.isArray(e[0]) ? (i = e[0], s = e.slice(1, e.length), a = r) : (i = e[0].events, s = e[0].data, a = e[0].context || r), (Array.isArray(i) ? i : i.split(" ")).forEach(function(e) {
                    if (r.eventsListeners[e]) {
                        var t = [];
                        r.eventsListeners[e].forEach(function(e) {
                            t.push(e)
                        }), t.forEach(function(e) {
                            e.apply(a, s)
                        })
                    }
                }), r) : r
            }, p.prototype.useModulesParams = function(e) {
                var t = this;
                t.modules && Object.keys(t.modules).forEach(function(i) {
                    var s = t.modules[i];
                    s.params && d.extend(e, s.params)
                })
            }, p.prototype.useModules = function(e) {
                void 0 === e && (e = {});
                var t = this;
                t.modules && Object.keys(t.modules).forEach(function(i) {
                    var s = t.modules[i],
                        a = e[i] || {};
                    s.instance && Object.keys(s.instance).forEach(function(e) {
                        var i = s.instance[e];
                        t[e] = "function" == typeof i ? i.bind(t) : i
                    }), s.on && t.on && Object.keys(s.on).forEach(function(e) {
                        t.on(e, s.on[e])
                    }), s.create && s.create.bind(t)(a)
                })
            }, c.components.set = function(e) {
                this.use && this.use(e)
            }, p.installModule = function(e) {
                for (var t = [], i = arguments.length - 1; i-- > 0;) t[i] = arguments[i + 1];
                var s = this;
                s.prototype.modules || (s.prototype.modules = {});
                var a = e.name || Object.keys(s.prototype.modules).length + "_" + d.now();
                return s.prototype.modules[a] = e, e.proto && Object.keys(e.proto).forEach(function(t) {
                    s.prototype[t] = e.proto[t]
                }), e.static && Object.keys(e.static).forEach(function(t) {
                    s[t] = e.static[t]
                }), e.install && e.install.apply(s, t), s
            }, p.use = function(e) {
                for (var t = [], i = arguments.length - 1; i-- > 0;) t[i] = arguments[i + 1];
                var s = this;
                return Array.isArray(e) ? (e.forEach(function(e) {
                    return s.installModule(e)
                }), s) : s.installModule.apply(s, [e].concat(t))
            }, Object.defineProperties(p, c);
            var u = {
                    updateSize: function() {
                        var e, t, i = this.$el;
                        e = void 0 !== this.params.width ? this.params.width : i[0].clientWidth, t = void 0 !== this.params.height ? this.params.height : i[0].clientHeight, 0 === e && this.isHorizontal() || 0 === t && this.isVertical() || (e = e - parseInt(i.css("padding-left"), 10) - parseInt(i.css("padding-right"), 10), t = t - parseInt(i.css("padding-top"), 10) - parseInt(i.css("padding-bottom"), 10), d.extend(this, {
                            width: e,
                            height: t,
                            size: this.isHorizontal() ? e : t
                        }))
                    },
                    updateSlides: function() {
                        var e = this.params,
                            t = this.$wrapperEl,
                            i = this.size,
                            s = this.rtl,
                            a = this.wrongRTL,
                            r = t.children("." + this.params.slideClass),
                            n = this.virtual && e.virtual.enabled ? this.virtual.slides.length : r.length,
                            o = [],
                            l = [],
                            p = [],
                            c = e.slidesOffsetBefore;
                        "function" == typeof c && (c = e.slidesOffsetBefore.call(this));
                        var u = e.slidesOffsetAfter;
                        "function" == typeof u && (u = e.slidesOffsetAfter.call(this));
                        var v = n,
                            f = this.snapGrid.length,
                            m = this.snapGrid.length,
                            g = e.spaceBetween,
                            b = -c,
                            w = 0,
                            y = 0;
                        if (void 0 !== i) {
                            var x, E;
                            "string" == typeof g && g.indexOf("%") >= 0 && (g = parseFloat(g.replace("%", "")) / 100 * i), this.virtualSize = -g, s ? r.css({
                                marginLeft: "",
                                marginTop: ""
                            }) : r.css({
                                marginRight: "",
                                marginBottom: ""
                            }), e.slidesPerColumn > 1 && (x = Math.floor(n / e.slidesPerColumn) === n / this.params.slidesPerColumn ? n : Math.ceil(n / e.slidesPerColumn) * e.slidesPerColumn, "auto" !== e.slidesPerView && "row" === e.slidesPerColumnFill && (x = Math.max(x, e.slidesPerView * e.slidesPerColumn)));
                            for (var T, S = e.slidesPerColumn, C = x / S, M = C - (e.slidesPerColumn * C - n), z = 0; z < n; z += 1) {
                                E = 0;
                                var P = r.eq(z);
                                if (e.slidesPerColumn > 1) {
                                    var k = void 0,
                                        $ = void 0,
                                        L = void 0;
                                    "column" === e.slidesPerColumnFill ? (L = z - ($ = Math.floor(z / S)) * S, ($ > M || $ === M && L === S - 1) && (L += 1) >= S && (L = 0, $ += 1), k = $ + L * x / S, P.css({
                                        "-webkit-box-ordinal-group": k,
                                        "-moz-box-ordinal-group": k,
                                        "-ms-flex-order": k,
                                        "-webkit-order": k,
                                        order: k
                                    })) : $ = z - (L = Math.floor(z / C)) * C, P.css("margin-" + (this.isHorizontal() ? "top" : "left"), 0 !== L && e.spaceBetween && e.spaceBetween + "px").attr("data-swiper-column", $).attr("data-swiper-row", L)
                                }
                                "none" !== P.css("display") && ("auto" === e.slidesPerView ? (E = this.isHorizontal() ? P.outerWidth(!0) : P.outerHeight(!0), e.roundLengths && (E = Math.floor(E))) : (E = (i - (e.slidesPerView - 1) * g) / e.slidesPerView, e.roundLengths && (E = Math.floor(E)), r[z] && (this.isHorizontal() ? r[z].style.width = E + "px" : r[z].style.height = E + "px")), r[z] && (r[z].swiperSlideSize = E), p.push(E), e.centeredSlides ? (b = b + E / 2 + w / 2 + g, 0 === w && 0 !== z && (b = b - i / 2 - g), 0 === z && (b = b - i / 2 - g), Math.abs(b) < .001 && (b = 0), y % e.slidesPerGroup == 0 && o.push(b), l.push(b)) : (y % e.slidesPerGroup == 0 && o.push(b), l.push(b), b = b + E + g), this.virtualSize += E + g, w = E, y += 1)
                            }
                            if (this.virtualSize = Math.max(this.virtualSize, i) + u, s && a && ("slide" === e.effect || "coverflow" === e.effect) && t.css({
                                    width: this.virtualSize + e.spaceBetween + "px"
                                }), h.flexbox && !e.setWrapperSize || (this.isHorizontal() ? t.css({
                                    width: this.virtualSize + e.spaceBetween + "px"
                                }) : t.css({
                                    height: this.virtualSize + e.spaceBetween + "px"
                                })), e.slidesPerColumn > 1 && (this.virtualSize = (E + e.spaceBetween) * x, this.virtualSize = Math.ceil(this.virtualSize / e.slidesPerColumn) - e.spaceBetween, this.isHorizontal() ? t.css({
                                    width: this.virtualSize + e.spaceBetween + "px"
                                }) : t.css({
                                    height: this.virtualSize + e.spaceBetween + "px"
                                }), e.centeredSlides)) {
                                T = [];
                                for (var I = 0; I < o.length; I += 1) o[I] < this.virtualSize + o[0] && T.push(o[I]);
                                o = T
                            }
                            if (!e.centeredSlides) {
                                T = [];
                                for (var D = 0; D < o.length; D += 1) o[D] <= this.virtualSize - i && T.push(o[D]);
                                o = T, Math.floor(this.virtualSize - i) - Math.floor(o[o.length - 1]) > 1 && o.push(this.virtualSize - i)
                            }
                            0 === o.length && (o = [0]), 0 !== e.spaceBetween && (this.isHorizontal() ? s ? r.css({
                                marginLeft: g + "px"
                            }) : r.css({
                                marginRight: g + "px"
                            }) : r.css({
                                marginBottom: g + "px"
                            })), d.extend(this, {
                                slides: r,
                                snapGrid: o,
                                slidesGrid: l,
                                slidesSizesGrid: p
                            }), n !== v && this.emit("slidesLengthChange"), o.length !== f && (this.params.watchOverflow && this.checkOverflow(), this.emit("snapGridLengthChange")), l.length !== m && this.emit("slidesGridLengthChange"), (e.watchSlidesProgress || e.watchSlidesVisibility) && this.updateSlidesOffset()
                        }
                    },
                    updateAutoHeight: function() {
                        var e, t = [],
                            i = 0;
                        if ("auto" !== this.params.slidesPerView && this.params.slidesPerView > 1)
                            for (e = 0; e < Math.ceil(this.params.slidesPerView); e += 1) {
                                var s = this.activeIndex + e;
                                if (s > this.slides.length) break;
                                t.push(this.slides.eq(s)[0])
                            } else t.push(this.slides.eq(this.activeIndex)[0]);
                        for (e = 0; e < t.length; e += 1)
                            if (void 0 !== t[e]) {
                                var a = t[e].offsetHeight;
                                i = a > i ? a : i
                            }
                        i && this.$wrapperEl.css("height", i + "px")
                    },
                    updateSlidesOffset: function() {
                        for (var e = this.slides, t = 0; t < e.length; t += 1) e[t].swiperSlideOffset = this.isHorizontal() ? e[t].offsetLeft : e[t].offsetTop
                    },
                    updateSlidesProgress: function(e) {
                        void 0 === e && (e = this.translate || 0);
                        var t = this.params,
                            i = this.slides,
                            s = this.rtl;
                        if (0 !== i.length) {
                            void 0 === i[0].swiperSlideOffset && this.updateSlidesOffset();
                            var a = -e;
                            s && (a = e), i.removeClass(t.slideVisibleClass);
                            for (var r = 0; r < i.length; r += 1) {
                                var n = i[r],
                                    o = (a + (t.centeredSlides ? this.minTranslate() : 0) - n.swiperSlideOffset) / (n.swiperSlideSize + t.spaceBetween);
                                if (t.watchSlidesVisibility) {
                                    var l = -(a - n.swiperSlideOffset),
                                        d = l + this.slidesSizesGrid[r];
                                    (l >= 0 && l < this.size || d > 0 && d <= this.size || l <= 0 && d >= this.size) && i.eq(r).addClass(t.slideVisibleClass)
                                }
                                n.progress = s ? -o : o
                            }
                        }
                    },
                    updateProgress: function(e) {
                        void 0 === e && (e = this.translate || 0);
                        var t = this.params,
                            i = this.maxTranslate() - this.minTranslate(),
                            s = this.progress,
                            a = this.isBeginning,
                            r = this.isEnd,
                            n = a,
                            o = r;
                        0 === i ? (s = 0, a = !0, r = !0) : (a = (s = (e - this.minTranslate()) / i) <= 0, r = s >= 1), d.extend(this, {
                            progress: s,
                            isBeginning: a,
                            isEnd: r
                        }), (t.watchSlidesProgress || t.watchSlidesVisibility) && this.updateSlidesProgress(e), a && !n && this.emit("reachBeginning toEdge"), r && !o && this.emit("reachEnd toEdge"), (n && !a || o && !r) && this.emit("fromEdge"), this.emit("progress", s)
                    },
                    updateSlidesClasses: function() {
                        var e, t = this.slides,
                            i = this.params,
                            s = this.$wrapperEl,
                            a = this.activeIndex,
                            r = this.realIndex,
                            n = this.virtual && i.virtual.enabled;
                        t.removeClass(i.slideActiveClass + " " + i.slideNextClass + " " + i.slidePrevClass + " " + i.slideDuplicateActiveClass + " " + i.slideDuplicateNextClass + " " + i.slideDuplicatePrevClass), (e = n ? this.$wrapperEl.find("." + i.slideClass + '[data-swiper-slide-index="' + a + '"]') : t.eq(a)).addClass(i.slideActiveClass), i.loop && (e.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + r + '"]').addClass(i.slideDuplicateActiveClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + r + '"]').addClass(i.slideDuplicateActiveClass));
                        var o = e.nextAll("." + i.slideClass).eq(0).addClass(i.slideNextClass);
                        i.loop && 0 === o.length && (o = t.eq(0)).addClass(i.slideNextClass);
                        var l = e.prevAll("." + i.slideClass).eq(0).addClass(i.slidePrevClass);
                        i.loop && 0 === l.length && (l = t.eq(-1)).addClass(i.slidePrevClass), i.loop && (o.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + o.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + o.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass), l.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass))
                    },
                    updateActiveIndex: function(e) {
                        var t, i = this.rtl ? this.translate : -this.translate,
                            s = this.slidesGrid,
                            a = this.snapGrid,
                            r = this.params,
                            n = this.activeIndex,
                            o = this.realIndex,
                            l = this.snapIndex,
                            h = e;
                        if (void 0 === h) {
                            for (var p = 0; p < s.length; p += 1) void 0 !== s[p + 1] ? i >= s[p] && i < s[p + 1] - (s[p + 1] - s[p]) / 2 ? h = p : i >= s[p] && i < s[p + 1] && (h = p + 1) : i >= s[p] && (h = p);
                            r.normalizeSlideIndex && (h < 0 || void 0 === h) && (h = 0)
                        }
                        if ((t = a.indexOf(i) >= 0 ? a.indexOf(i) : Math.floor(h / r.slidesPerGroup)) >= a.length && (t = a.length - 1), h !== n) {
                            var c = parseInt(this.slides.eq(h).attr("data-swiper-slide-index") || h, 10);
                            d.extend(this, {
                                snapIndex: t,
                                realIndex: c,
                                previousIndex: n,
                                activeIndex: h
                            }), this.emit("activeIndexChange"), this.emit("snapIndexChange"), o !== c && this.emit("realIndexChange"), this.emit("slideChange")
                        } else t !== l && (this.snapIndex = t, this.emit("snapIndexChange"))
                    },
                    updateClickedSlide: function(e) {
                        var t = this.params,
                            i = s(e.target).closest("." + t.slideClass)[0],
                            a = !1;
                        if (i)
                            for (var r = 0; r < this.slides.length; r += 1) this.slides[r] === i && (a = !0);
                        if (!i || !a) return this.clickedSlide = void 0, void(this.clickedIndex = void 0);
                        this.clickedSlide = i, this.virtual && this.params.virtual.enabled ? this.clickedIndex = parseInt(s(i).attr("data-swiper-slide-index"), 10) : this.clickedIndex = s(i).index(), t.slideToClickedSlide && void 0 !== this.clickedIndex && this.clickedIndex !== this.activeIndex && this.slideToClickedSlide()
                    }
                },
                v = {
                    getTranslate: function(e) {
                        void 0 === e && (e = this.isHorizontal() ? "x" : "y");
                        var t = this.params,
                            i = this.rtl,
                            s = this.translate,
                            a = this.$wrapperEl;
                        if (t.virtualTranslate) return i ? -s : s;
                        var r = d.getTranslate(a[0], e);
                        return i && (r = -r), r || 0
                    },
                    setTranslate: function(e, t) {
                        var i = this.rtl,
                            s = this.params,
                            a = this.$wrapperEl,
                            r = this.progress,
                            n = 0,
                            o = 0;
                        this.isHorizontal() ? n = i ? -e : e : o = e, s.roundLengths && (n = Math.floor(n), o = Math.floor(o)), s.virtualTranslate || (h.transforms3d ? a.transform("translate3d(" + n + "px, " + o + "px, 0px)") : a.transform("translate(" + n + "px, " + o + "px)")), this.translate = this.isHorizontal() ? n : o;
                        var l = this.maxTranslate() - this.minTranslate();
                        (0 === l ? 0 : (e - this.minTranslate()) / l) !== r && this.updateProgress(e), this.emit("setTranslate", this.translate, t)
                    },
                    minTranslate: function() {
                        return -this.snapGrid[0]
                    },
                    maxTranslate: function() {
                        return -this.snapGrid[this.snapGrid.length - 1]
                    }
                },
                f = {
                    setTransition: function(e, t) {
                        this.$wrapperEl.transition(e), this.emit("setTransition", e, t)
                    },
                    transitionStart: function(e, t) {
                        void 0 === e && (e = !0);
                        var i = this.activeIndex,
                            s = this.params,
                            a = this.previousIndex;
                        s.autoHeight && this.updateAutoHeight();
                        var r = t;
                        if (r || (r = i > a ? "next" : i < a ? "prev" : "reset"), this.emit("transitionStart"), e && i !== a) {
                            if ("reset" === r) return void this.emit("slideResetTransitionStart");
                            this.emit("slideChangeTransitionStart"), "next" === r ? this.emit("slideNextTransitionStart") : this.emit("slidePrevTransitionStart")
                        }
                    },
                    transitionEnd: function(e, t) {
                        void 0 === e && (e = !0);
                        var i = this.activeIndex,
                            s = this.previousIndex;
                        this.animating = !1, this.setTransition(0);
                        var a = t;
                        if (a || (a = i > s ? "next" : i < s ? "prev" : "reset"), this.emit("transitionEnd"), e && i !== s) {
                            if ("reset" === a) return void this.emit("slideResetTransitionEnd");
                            this.emit("slideChangeTransitionEnd"), "next" === a ? this.emit("slideNextTransitionEnd") : this.emit("slidePrevTransitionEnd")
                        }
                    }
                },
                m = {
                    slideTo: function(e, t, i, s) {
                        void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
                        var a = this,
                            r = e;
                        r < 0 && (r = 0);
                        var n = a.params,
                            o = a.snapGrid,
                            l = a.slidesGrid,
                            d = a.previousIndex,
                            p = a.activeIndex,
                            c = a.rtl,
                            u = a.$wrapperEl;
                        if (a.animating && n.preventIntercationOnTransition) return !1;
                        var v = Math.floor(r / n.slidesPerGroup);
                        v >= o.length && (v = o.length - 1), (p || n.initialSlide || 0) === (d || 0) && i && a.emit("beforeSlideChangeStart");
                        var f, m = -o[v];
                        if (a.updateProgress(m), n.normalizeSlideIndex)
                            for (var g = 0; g < l.length; g += 1) - Math.floor(100 * m) >= Math.floor(100 * l[g]) && (r = g);
                        if (a.initialized && r !== p) {
                            if (!a.allowSlideNext && m < a.translate && m < a.minTranslate()) return !1;
                            if (!a.allowSlidePrev && m > a.translate && m > a.maxTranslate() && (p || 0) !== r) return !1
                        }
                        return f = r > p ? "next" : r < p ? "prev" : "reset", c && -m === a.translate || !c && m === a.translate ? (a.updateActiveIndex(r), n.autoHeight && a.updateAutoHeight(), a.updateSlidesClasses(), "slide" !== n.effect && a.setTranslate(m), "reset" !== f && (a.transitionStart(i, f), a.transitionEnd(i, f)), !1) : (0 !== t && h.transition ? (a.setTransition(t), a.setTranslate(m), a.updateActiveIndex(r), a.updateSlidesClasses(), a.emit("beforeTransitionStart", t, s), a.transitionStart(i, f), a.animating || (a.animating = !0, u.transitionEnd(function() {
                            a && !a.destroyed && a.transitionEnd(i, f)
                        }))) : (a.setTransition(0), a.setTranslate(m), a.updateActiveIndex(r), a.updateSlidesClasses(), a.emit("beforeTransitionStart", t, s), a.transitionStart(i, f), a.transitionEnd(i, f)), !0)
                    },
                    slideToLoop: function(e, t, i, s) {
                        void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
                        var a = e;
                        return this.params.loop && (a += this.loopedSlides), this.slideTo(a, t, i, s)
                    },
                    slideNext: function(e, t, i) {
                        void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                        var s = this.params,
                            a = this.animating;
                        return s.loop ? !a && (this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft, this.slideTo(this.activeIndex + s.slidesPerGroup, e, t, i)) : this.slideTo(this.activeIndex + s.slidesPerGroup, e, t, i)
                    },
                    slidePrev: function(e, t, i) {
                        void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                        var s = this.params,
                            a = this.animating;
                        return s.loop ? !a && (this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft, this.slideTo(this.activeIndex - 1, e, t, i)) : this.slideTo(this.activeIndex - 1, e, t, i)
                    },
                    slideReset: function(e, t, i) {
                        void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                        return this.slideTo(this.activeIndex, e, t, i)
                    },
                    slideToClickedSlide: function() {
                        var e, t = this,
                            i = t.params,
                            a = t.$wrapperEl,
                            r = "auto" === i.slidesPerView ? t.slidesPerViewDynamic() : i.slidesPerView,
                            n = t.clickedIndex;
                        if (i.loop) {
                            if (t.animating) return;
                            e = parseInt(s(t.clickedSlide).attr("data-swiper-slide-index"), 10), i.centeredSlides ? n < t.loopedSlides - r / 2 || n > t.slides.length - t.loopedSlides + r / 2 ? (t.loopFix(), n = a.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(), d.nextTick(function() {
                                t.slideTo(n)
                            })) : t.slideTo(n) : n > t.slides.length - r ? (t.loopFix(), n = a.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(), d.nextTick(function() {
                                t.slideTo(n)
                            })) : t.slideTo(n)
                        } else t.slideTo(n)
                    }
                },
                g = {
                    loopCreate: function() {
                        var t = this,
                            i = t.params,
                            a = t.$wrapperEl;
                        a.children("." + i.slideClass + "." + i.slideDuplicateClass).remove();
                        var r = a.children("." + i.slideClass);
                        if (i.loopFillGroupWithBlank) {
                            var n = i.slidesPerGroup - r.length % i.slidesPerGroup;
                            if (n !== i.slidesPerGroup) {
                                for (var o = 0; o < n; o += 1) {
                                    var l = s(e.createElement("div")).addClass(i.slideClass + " " + i.slideBlankClass);
                                    a.append(l)
                                }
                                r = a.children("." + i.slideClass)
                            }
                        }
                        "auto" !== i.slidesPerView || i.loopedSlides || (i.loopedSlides = r.length), t.loopedSlides = parseInt(i.loopedSlides || i.slidesPerView, 10), t.loopedSlides += i.loopAdditionalSlides, t.loopedSlides > r.length && (t.loopedSlides = r.length);
                        var d = [],
                            h = [];
                        r.each(function(e, i) {
                            var a = s(i);
                            e < t.loopedSlides && h.push(i), e < r.length && e >= r.length - t.loopedSlides && d.push(i), a.attr("data-swiper-slide-index", e)
                        });
                        for (var p = 0; p < h.length; p += 1) a.append(s(h[p].cloneNode(!0)).addClass(i.slideDuplicateClass));
                        for (var c = d.length - 1; c >= 0; c -= 1) a.prepend(s(d[c].cloneNode(!0)).addClass(i.slideDuplicateClass))
                    },
                    loopFix: function() {
                        var e, t = this.params,
                            i = this.activeIndex,
                            s = this.slides,
                            a = this.loopedSlides,
                            r = this.allowSlidePrev,
                            n = this.allowSlideNext,
                            o = this.snapGrid,
                            l = this.rtl;
                        this.allowSlidePrev = !0, this.allowSlideNext = !0;
                        var d = -o[i] - this.getTranslate();
                        i < a ? (e = s.length - 3 * a + i, e += a, this.slideTo(e, 0, !1, !0) && 0 !== d && this.setTranslate((l ? -this.translate : this.translate) - d)) : ("auto" === t.slidesPerView && i >= 2 * a || i > s.length - 2 * t.slidesPerView) && (e = -s.length + i + a, e += a, this.slideTo(e, 0, !1, !0) && 0 !== d && this.setTranslate((l ? -this.translate : this.translate) - d));
                        this.allowSlidePrev = r, this.allowSlideNext = n
                    },
                    loopDestroy: function() {
                        var e = this.$wrapperEl,
                            t = this.params,
                            i = this.slides;
                        e.children("." + t.slideClass + "." + t.slideDuplicateClass).remove(), i.removeAttr("data-swiper-slide-index")
                    }
                },
                b = {
                    setGrabCursor: function(e) {
                        if (!h.touch && this.params.simulateTouch) {
                            var t = this.el;
                            t.style.cursor = "move", t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab", t.style.cursor = e ? "-moz-grabbin" : "-moz-grab", t.style.cursor = e ? "grabbing" : "grab"
                        }
                    },
                    unsetGrabCursor: function() {
                        h.touch || (this.el.style.cursor = "")
                    }
                },
                w = {
                    appendSlide: function(e) {
                        var t = this.$wrapperEl,
                            i = this.params;
                        if (i.loop && this.loopDestroy(), "object" == typeof e && "length" in e)
                            for (var s = 0; s < e.length; s += 1) e[s] && t.append(e[s]);
                        else t.append(e);
                        i.loop && this.loopCreate(), i.observer && h.observer || this.update()
                    },
                    prependSlide: function(e) {
                        var t = this.params,
                            i = this.$wrapperEl,
                            s = this.activeIndex;
                        t.loop && this.loopDestroy();
                        var a = s + 1;
                        if ("object" == typeof e && "length" in e) {
                            for (var r = 0; r < e.length; r += 1) e[r] && i.prepend(e[r]);
                            a = s + e.length
                        } else i.prepend(e);
                        t.loop && this.loopCreate(), t.observer && h.observer || this.update(), this.slideTo(a, 0, !1)
                    },
                    removeSlide: function(e) {
                        var t = this.params,
                            i = this.$wrapperEl,
                            s = this.activeIndex;
                        t.loop && (this.loopDestroy(), this.slides = i.children("." + t.slideClass));
                        var a, r = s;
                        if ("object" == typeof e && "length" in e) {
                            for (var n = 0; n < e.length; n += 1) a = e[n], this.slides[a] && this.slides.eq(a).remove(), a < r && (r -= 1);
                            r = Math.max(r, 0)
                        } else a = e, this.slides[a] && this.slides.eq(a).remove(), a < r && (r -= 1), r = Math.max(r, 0);
                        t.loop && this.loopCreate(), t.observer && h.observer || this.update(), t.loop ? this.slideTo(r + this.loopedSlides, 0, !1) : this.slideTo(r, 0, !1)
                    },
                    removeAllSlides: function() {
                        for (var e = [], t = 0; t < this.slides.length; t += 1) e.push(t);
                        this.removeSlide(e)
                    }
                },
                y = function() {
                    var i = t.navigator.userAgent,
                        s = {
                            ios: !1,
                            android: !1,
                            androidChrome: !1,
                            desktop: !1,
                            windows: !1,
                            iphone: !1,
                            ipod: !1,
                            ipad: !1,
                            cordova: t.cordova || t.phonegap,
                            phonegap: t.cordova || t.phonegap
                        },
                        a = i.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),
                        r = i.match(/(Android);?[\s\/]+([\d.]+)?/),
                        n = i.match(/(iPad).*OS\s([\d_]+)/),
                        o = i.match(/(iPod)(.*OS\s([\d_]+))?/),
                        l = !n && i.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
                    if (a && (s.os = "windows", s.osVersion = a[2], s.windows = !0), r && !a && (s.os = "android", s.osVersion = r[2], s.android = !0, s.androidChrome = i.toLowerCase().indexOf("chrome") >= 0), (n || l || o) && (s.os = "ios", s.ios = !0), l && !o && (s.osVersion = l[2].replace(/_/g, "."), s.iphone = !0), n && (s.osVersion = n[2].replace(/_/g, "."), s.ipad = !0), o && (s.osVersion = o[3] ? o[3].replace(/_/g, ".") : null, s.iphone = !0), s.ios && s.osVersion && i.indexOf("Version/") >= 0 && "10" === s.osVersion.split(".")[0] && (s.osVersion = i.toLowerCase().split("version/")[1].split(" ")[0]), s.desktop = !(s.os || s.android || s.webView), s.webView = (l || n || o) && i.match(/.*AppleWebKit(?!.*Safari)/i), s.os && "ios" === s.os) {
                        var d = s.osVersion.split("."),
                            h = e.querySelector('meta[name="viewport"]');
                        s.minimalUi = !s.webView && (o || l) && (1 * d[0] == 7 ? 1 * d[1] >= 1 : 1 * d[0] > 7) && h && h.getAttribute("content").indexOf("minimal-ui") >= 0
                    }
                    return s.pixelRatio = t.devicePixelRatio || 1, s
                }(),
                x = function(i) {
                    var a = this.touchEventsData,
                        r = this.params,
                        n = this.touches;
                    if (!this.animating || !r.preventIntercationOnTransition) {
                        var o = i;
                        if (o.originalEvent && (o = o.originalEvent), a.isTouchEvent = "touchstart" === o.type, (a.isTouchEvent || !("which" in o) || 3 !== o.which) && (!a.isTouched || !a.isMoved))
                            if (r.noSwiping && s(o.target).closest(r.noSwipingSelector ? r.noSwipingSelector : "." + r.noSwipingClass)[0]) this.allowClick = !0;
                            else if (!r.swipeHandler || s(o).closest(r.swipeHandler)[0]) {
                            n.currentX = "touchstart" === o.type ? o.targetTouches[0].pageX : o.pageX, n.currentY = "touchstart" === o.type ? o.targetTouches[0].pageY : o.pageY;
                            var l = n.currentX,
                                h = n.currentY;
                            if (!(y.ios && !y.cordova && r.iOSEdgeSwipeDetection && l <= r.iOSEdgeSwipeThreshold && l >= t.screen.width - r.iOSEdgeSwipeThreshold)) {
                                if (d.extend(a, {
                                        isTouched: !0,
                                        isMoved: !1,
                                        allowTouchCallbacks: !0,
                                        isScrolling: void 0,
                                        startMoving: void 0
                                    }), n.startX = l, n.startY = h, a.touchStartTime = d.now(), this.allowClick = !0, this.updateSize(), this.swipeDirection = void 0, r.threshold > 0 && (a.allowThresholdMove = !1), "touchstart" !== o.type) {
                                    var p = !0;
                                    s(o.target).is(a.formElements) && (p = !1), e.activeElement && s(e.activeElement).is(a.formElements) && e.activeElement !== o.target && e.activeElement.blur(), p && this.allowTouchMove && o.preventDefault()
                                }
                                this.emit("touchStart", o)
                            }
                        }
                    }
                },
                E = function(t) {
                    var i = this.touchEventsData,
                        a = this.params,
                        r = this.touches,
                        n = this.rtl,
                        o = t;
                    if (o.originalEvent && (o = o.originalEvent), i.isTouched) {
                        if (!i.isTouchEvent || "mousemove" !== o.type) {
                            var l = "touchmove" === o.type ? o.targetTouches[0].pageX : o.pageX,
                                h = "touchmove" === o.type ? o.targetTouches[0].pageY : o.pageY;
                            if (o.preventedByNestedSwiper) return r.startX = l, void(r.startY = h);
                            if (!this.allowTouchMove) return this.allowClick = !1, void(i.isTouched && (d.extend(r, {
                                startX: l,
                                startY: h,
                                currentX: l,
                                currentY: h
                            }), i.touchStartTime = d.now()));
                            if (i.isTouchEvent && a.touchReleaseOnEdges && !a.loop)
                                if (this.isVertical()) {
                                    if (h < r.startY && this.translate <= this.maxTranslate() || h > r.startY && this.translate >= this.minTranslate()) return i.isTouched = !1, void(i.isMoved = !1)
                                } else if (l < r.startX && this.translate <= this.maxTranslate() || l > r.startX && this.translate >= this.minTranslate()) return;
                            if (i.isTouchEvent && e.activeElement && o.target === e.activeElement && s(o.target).is(i.formElements)) return i.isMoved = !0, void(this.allowClick = !1);
                            if (i.allowTouchCallbacks && this.emit("touchMove", o), !(o.targetTouches && o.targetTouches.length > 1)) {
                                r.currentX = l, r.currentY = h;
                                var p, c = r.currentX - r.startX,
                                    u = r.currentY - r.startY;
                                if (void 0 === i.isScrolling) this.isHorizontal() && r.currentY === r.startY || this.isVertical() && r.currentX === r.startX ? i.isScrolling = !1 : c * c + u * u >= 25 && (p = 180 * Math.atan2(Math.abs(u), Math.abs(c)) / Math.PI, i.isScrolling = this.isHorizontal() ? p > a.touchAngle : 90 - p > a.touchAngle);
                                if (i.isScrolling && this.emit("touchMoveOpposite", o), "undefined" == typeof startMoving && (r.currentX === r.startX && r.currentY === r.startY || (i.startMoving = !0)), i.isScrolling) i.isTouched = !1;
                                else if (i.startMoving) {
                                    this.allowClick = !1, o.preventDefault(), a.touchMoveStopPropagation && !a.nested && o.stopPropagation(), i.isMoved || (a.loop && this.loopFix(), i.startTranslate = this.getTranslate(), this.setTransition(0), this.animating && this.$wrapperEl.trigger("webkitTransitionEnd transitionend"), i.allowMomentumBounce = !1, !a.grabCursor || !0 !== this.allowSlideNext && !0 !== this.allowSlidePrev || this.setGrabCursor(!0), this.emit("sliderFirstMove", o)), this.emit("sliderMove", o), i.isMoved = !0;
                                    var v = this.isHorizontal() ? c : u;
                                    r.diff = v, v *= a.touchRatio, n && (v = -v), this.swipeDirection = v > 0 ? "prev" : "next", i.currentTranslate = v + i.startTranslate;
                                    var f = !0,
                                        m = a.resistanceRatio;
                                    if (a.touchReleaseOnEdges && (m = 0), v > 0 && i.currentTranslate > this.minTranslate() ? (f = !1, a.resistance && (i.currentTranslate = this.minTranslate() - 1 + Math.pow(-this.minTranslate() + i.startTranslate + v, m))) : v < 0 && i.currentTranslate < this.maxTranslate() && (f = !1, a.resistance && (i.currentTranslate = this.maxTranslate() + 1 - Math.pow(this.maxTranslate() - i.startTranslate - v, m))), f && (o.preventedByNestedSwiper = !0), !this.allowSlideNext && "next" === this.swipeDirection && i.currentTranslate < i.startTranslate && (i.currentTranslate = i.startTranslate), !this.allowSlidePrev && "prev" === this.swipeDirection && i.currentTranslate > i.startTranslate && (i.currentTranslate = i.startTranslate), a.threshold > 0) {
                                        if (!(Math.abs(v) > a.threshold || i.allowThresholdMove)) return void(i.currentTranslate = i.startTranslate);
                                        if (!i.allowThresholdMove) return i.allowThresholdMove = !0, r.startX = r.currentX, r.startY = r.currentY, i.currentTranslate = i.startTranslate, void(r.diff = this.isHorizontal() ? r.currentX - r.startX : r.currentY - r.startY)
                                    }
                                    a.followFinger && ((a.freeMode || a.watchSlidesProgress || a.watchSlidesVisibility) && (this.updateActiveIndex(), this.updateSlidesClasses()), a.freeMode && (0 === i.velocities.length && i.velocities.push({
                                        position: r[this.isHorizontal() ? "startX" : "startY"],
                                        time: i.touchStartTime
                                    }), i.velocities.push({
                                        position: r[this.isHorizontal() ? "currentX" : "currentY"],
                                        time: d.now()
                                    })), this.updateProgress(i.currentTranslate), this.setTranslate(i.currentTranslate))
                                }
                            }
                        }
                    } else i.startMoving && i.isScrolling && this.emit("touchMoveOpposite", o)
                },
                T = function(e) {
                    var t = this,
                        i = t.touchEventsData,
                        s = t.params,
                        a = t.touches,
                        r = t.rtl,
                        n = t.$wrapperEl,
                        o = t.slidesGrid,
                        l = t.snapGrid,
                        h = e;
                    if (h.originalEvent && (h = h.originalEvent), i.allowTouchCallbacks && t.emit("touchEnd", h), i.allowTouchCallbacks = !1, !i.isTouched) return i.isMoved && s.grabCursor && t.setGrabCursor(!1), i.isMoved = !1, void(i.startMoving = !1);
                    s.grabCursor && i.isMoved && i.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);
                    var p, c = d.now(),
                        u = c - i.touchStartTime;
                    if (t.allowClick && (t.updateClickedSlide(h), t.emit("tap", h), u < 300 && c - i.lastClickTime > 300 && (i.clickTimeout && clearTimeout(i.clickTimeout), i.clickTimeout = d.nextTick(function() {
                            t && !t.destroyed && t.emit("click", h)
                        }, 300)), u < 300 && c - i.lastClickTime < 300 && (i.clickTimeout && clearTimeout(i.clickTimeout), t.emit("doubleTap", h))), i.lastClickTime = d.now(), d.nextTick(function() {
                            t.destroyed || (t.allowClick = !0)
                        }), !i.isTouched || !i.isMoved || !t.swipeDirection || 0 === a.diff || i.currentTranslate === i.startTranslate) return i.isTouched = !1, i.isMoved = !1, void(i.startMoving = !1);
                    if (i.isTouched = !1, i.isMoved = !1, i.startMoving = !1, p = s.followFinger ? r ? t.translate : -t.translate : -i.currentTranslate, s.freeMode) {
                        if (p < -t.minTranslate()) return void t.slideTo(t.activeIndex);
                        if (p > -t.maxTranslate()) return void(t.slides.length < l.length ? t.slideTo(l.length - 1) : t.slideTo(t.slides.length - 1));
                        if (s.freeModeMomentum) {
                            if (i.velocities.length > 1) {
                                var v = i.velocities.pop(),
                                    f = i.velocities.pop(),
                                    m = v.position - f.position,
                                    g = v.time - f.time;
                                t.velocity = m / g, t.velocity /= 2, Math.abs(t.velocity) < s.freeModeMinimumVelocity && (t.velocity = 0), (g > 150 || d.now() - v.time > 300) && (t.velocity = 0)
                            } else t.velocity = 0;
                            t.velocity *= s.freeModeMomentumVelocityRatio, i.velocities.length = 0;
                            var b = 1e3 * s.freeModeMomentumRatio,
                                w = t.velocity * b,
                                y = t.translate + w;
                            r && (y = -y);
                            var x, E = !1,
                                T = 20 * Math.abs(t.velocity) * s.freeModeMomentumBounceRatio;
                            if (y < t.maxTranslate()) s.freeModeMomentumBounce ? (y + t.maxTranslate() < -T && (y = t.maxTranslate() - T), x = t.maxTranslate(), E = !0, i.allowMomentumBounce = !0) : y = t.maxTranslate();
                            else if (y > t.minTranslate()) s.freeModeMomentumBounce ? (y - t.minTranslate() > T && (y = t.minTranslate() + T), x = t.minTranslate(), E = !0, i.allowMomentumBounce = !0) : y = t.minTranslate();
                            else if (s.freeModeSticky) {
                                for (var S, C = 0; C < l.length; C += 1)
                                    if (l[C] > -y) {
                                        S = C;
                                        break
                                    }
                                y = -(y = Math.abs(l[S] - y) < Math.abs(l[S - 1] - y) || "next" === t.swipeDirection ? l[S] : l[S - 1])
                            }
                            if (0 !== t.velocity) b = r ? Math.abs((-y - t.translate) / t.velocity) : Math.abs((y - t.translate) / t.velocity);
                            else if (s.freeModeSticky) return void t.slideReset();
                            s.freeModeMomentumBounce && E ? (t.updateProgress(x), t.setTransition(b), t.setTranslate(y), t.transitionStart(!0, t.swipeDirection), t.animating = !0, n.transitionEnd(function() {
                                t && !t.destroyed && i.allowMomentumBounce && (t.emit("momentumBounce"), t.setTransition(s.speed), t.setTranslate(x), n.transitionEnd(function() {
                                    t && !t.destroyed && t.transitionEnd()
                                }))
                            })) : t.velocity ? (t.updateProgress(y), t.setTransition(b), t.setTranslate(y), t.transitionStart(!0, t.swipeDirection), t.animating || (t.animating = !0, n.transitionEnd(function() {
                                t && !t.destroyed && t.transitionEnd()
                            }))) : t.updateProgress(y), t.updateActiveIndex(), t.updateSlidesClasses()
                        }(!s.freeModeMomentum || u >= s.longSwipesMs) && (t.updateProgress(), t.updateActiveIndex(), t.updateSlidesClasses())
                    } else {
                        for (var M = 0, z = t.slidesSizesGrid[0], P = 0; P < o.length; P += s.slidesPerGroup) void 0 !== o[P + s.slidesPerGroup] ? p >= o[P] && p < o[P + s.slidesPerGroup] && (M = P, z = o[P + s.slidesPerGroup] - o[P]) : p >= o[P] && (M = P, z = o[o.length - 1] - o[o.length - 2]);
                        var k = (p - o[M]) / z;
                        if (u > s.longSwipesMs) {
                            if (!s.longSwipes) return void t.slideTo(t.activeIndex);
                            "next" === t.swipeDirection && (k >= s.longSwipesRatio ? t.slideTo(M + s.slidesPerGroup) : t.slideTo(M)), "prev" === t.swipeDirection && (k > 1 - s.longSwipesRatio ? t.slideTo(M + s.slidesPerGroup) : t.slideTo(M))
                        } else {
                            if (!s.shortSwipes) return void t.slideTo(t.activeIndex);
                            "next" === t.swipeDirection && t.slideTo(M + s.slidesPerGroup), "prev" === t.swipeDirection && t.slideTo(M)
                        }
                    }
                },
                S = function() {
                    var e = this.params,
                        t = this.el;
                    if (!t || 0 !== t.offsetWidth) {
                        e.breakpoints && this.setBreakpoint();
                        var i = this.allowSlideNext,
                            s = this.allowSlidePrev;
                        if (this.allowSlideNext = !0, this.allowSlidePrev = !0, this.updateSize(), this.updateSlides(), e.freeMode) {
                            var a = Math.min(Math.max(this.translate, this.maxTranslate()), this.minTranslate());
                            this.setTranslate(a), this.updateActiveIndex(), this.updateSlidesClasses(), e.autoHeight && this.updateAutoHeight()
                        } else this.updateSlidesClasses(), ("auto" === e.slidesPerView || e.slidesPerView > 1) && this.isEnd && !this.params.centeredSlides ? this.slideTo(this.slides.length - 1, 0, !1, !0) : this.slideTo(this.activeIndex, 0, !1, !0);
                        this.allowSlidePrev = s, this.allowSlideNext = i
                    }
                },
                C = function(e) {
                    this.allowClick || (this.params.preventClicks && e.preventDefault(), this.params.preventClicksPropagation && this.animating && (e.stopPropagation(), e.stopImmediatePropagation()))
                };
            var M = {
                    attachEvents: function() {
                        var t = this.params,
                            i = this.touchEvents,
                            s = this.el,
                            a = this.wrapperEl;
                        this.onTouchStart = x.bind(this), this.onTouchMove = E.bind(this), this.onTouchEnd = T.bind(this), this.onClick = C.bind(this);
                        var r = "container" === t.touchEventsTarget ? s : a,
                            n = !!t.nested;
                        if (h.touch || !h.pointerEvents && !h.prefixedPointerEvents) {
                            if (h.touch) {
                                var o = !("touchstart" !== i.start || !h.passiveListener || !t.passiveListeners) && {
                                    passive: !0,
                                    capture: !1
                                };
                                r.addEventListener(i.start, this.onTouchStart, o), r.addEventListener(i.move, this.onTouchMove, h.passiveListener ? {
                                    passive: !1,
                                    capture: n
                                } : n), r.addEventListener(i.end, this.onTouchEnd, o)
                            }(t.simulateTouch && !y.ios && !y.android || t.simulateTouch && !h.touch && y.ios) && (r.addEventListener("mousedown", this.onTouchStart, !1), e.addEventListener("mousemove", this.onTouchMove, n), e.addEventListener("mouseup", this.onTouchEnd, !1))
                        } else r.addEventListener(i.start, this.onTouchStart, !1), e.addEventListener(i.move, this.onTouchMove, n), e.addEventListener(i.end, this.onTouchEnd, !1);
                        (t.preventClicks || t.preventClicksPropagation) && r.addEventListener("click", this.onClick, !0), this.on("resize observerUpdate", S)
                    },
                    detachEvents: function() {
                        var t = this.params,
                            i = this.touchEvents,
                            s = this.el,
                            a = this.wrapperEl,
                            r = "container" === t.touchEventsTarget ? s : a,
                            n = !!t.nested;
                        if (h.touch || !h.pointerEvents && !h.prefixedPointerEvents) {
                            if (h.touch) {
                                var o = !("onTouchStart" !== i.start || !h.passiveListener || !t.passiveListeners) && {
                                    passive: !0,
                                    capture: !1
                                };
                                r.removeEventListener(i.start, this.onTouchStart, o), r.removeEventListener(i.move, this.onTouchMove, n), r.removeEventListener(i.end, this.onTouchEnd, o)
                            }(t.simulateTouch && !y.ios && !y.android || t.simulateTouch && !h.touch && y.ios) && (r.removeEventListener("mousedown", this.onTouchStart, !1), e.removeEventListener("mousemove", this.onTouchMove, n), e.removeEventListener("mouseup", this.onTouchEnd, !1))
                        } else r.removeEventListener(i.start, this.onTouchStart, !1), e.removeEventListener(i.move, this.onTouchMove, n), e.removeEventListener(i.end, this.onTouchEnd, !1);
                        (t.preventClicks || t.preventClicksPropagation) && r.removeEventListener("click", this.onClick, !0), this.off("resize observerUpdate", S)
                    }
                },
                z = {
                    setBreakpoint: function() {
                        var e = this.activeIndex,
                            t = this.loopedSlides;
                        void 0 === t && (t = 0);
                        var i = this.params,
                            s = i.breakpoints;
                        if (s && (!s || 0 !== Object.keys(s).length)) {
                            var a = this.getBreakpoint(s);
                            if (a && this.currentBreakpoint !== a) {
                                var r = a in s ? s[a] : this.originalParams,
                                    n = i.loop && r.slidesPerView !== i.slidesPerView;
                                d.extend(this.params, r), d.extend(this, {
                                    allowTouchMove: this.params.allowTouchMove,
                                    allowSlideNext: this.params.allowSlideNext,
                                    allowSlidePrev: this.params.allowSlidePrev
                                }), this.currentBreakpoint = a, n && (this.loopDestroy(), this.loopCreate(), this.updateSlides(), this.slideTo(e - t + this.loopedSlides, 0, !1)), this.emit("breakpoint", r)
                            }
                        }
                    },
                    getBreakpoint: function(e) {
                        if (e) {
                            var i = !1,
                                s = [];
                            Object.keys(e).forEach(function(e) {
                                s.push(e)
                            }), s.sort(function(e, t) {
                                return parseInt(e, 10) - parseInt(t, 10)
                            });
                            for (var a = 0; a < s.length; a += 1) {
                                var r = s[a];
                                r >= t.innerWidth && !i && (i = r)
                            }
                            return i || "max"
                        }
                    }
                },
                P = function() {
                    return {
                        isIE: !!t.navigator.userAgent.match(/Trident/g) || !!t.navigator.userAgent.match(/MSIE/g),
                        isSafari: (e = t.navigator.userAgent.toLowerCase(), e.indexOf("safari") >= 0 && e.indexOf("chrome") < 0 && e.indexOf("android") < 0),
                        isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(t.navigator.userAgent)
                    };
                    var e
                }();
            var k = {
                    init: !0,
                    direction: "horizontal",
                    touchEventsTarget: "container",
                    initialSlide: 0,
                    speed: 300,
                    preventIntercationOnTransition: !1,
                    iOSEdgeSwipeDetection: !1,
                    iOSEdgeSwipeThreshold: 20,
                    freeMode: !1,
                    freeModeMomentum: !0,
                    freeModeMomentumRatio: 1,
                    freeModeMomentumBounce: !0,
                    freeModeMomentumBounceRatio: 1,
                    freeModeMomentumVelocityRatio: 1,
                    freeModeSticky: !1,
                    freeModeMinimumVelocity: .02,
                    autoHeight: !1,
                    setWrapperSize: !1,
                    virtualTranslate: !1,
                    effect: "slide",
                    breakpoints: void 0,
                    spaceBetween: 0,
                    slidesPerView: 1,
                    slidesPerColumn: 1,
                    slidesPerColumnFill: "column",
                    slidesPerGroup: 1,
                    centeredSlides: !1,
                    slidesOffsetBefore: 0,
                    slidesOffsetAfter: 0,
                    normalizeSlideIndex: !0,
                    watchOverflow: !1,
                    roundLengths: !1,
                    touchRatio: 1,
                    touchAngle: 45,
                    simulateTouch: !0,
                    shortSwipes: !0,
                    longSwipes: !0,
                    longSwipesRatio: .5,
                    longSwipesMs: 300,
                    followFinger: !0,
                    allowTouchMove: !0,
                    threshold: 0,
                    touchMoveStopPropagation: !0,
                    touchReleaseOnEdges: !1,
                    uniqueNavElements: !0,
                    resistance: !0,
                    resistanceRatio: .85,
                    watchSlidesProgress: !1,
                    watchSlidesVisibility: !1,
                    grabCursor: !1,
                    preventClicks: !0,
                    preventClicksPropagation: !0,
                    slideToClickedSlide: !1,
                    preloadImages: !0,
                    updateOnImagesReady: !0,
                    loop: !1,
                    loopAdditionalSlides: 0,
                    loopedSlides: null,
                    loopFillGroupWithBlank: !1,
                    allowSlidePrev: !0,
                    allowSlideNext: !0,
                    swipeHandler: null,
                    noSwiping: !0,
                    noSwipingClass: "swiper-no-swiping",
                    noSwipingSelector: null,
                    passiveListeners: !0,
                    containerModifierClass: "swiper-container-",
                    slideClass: "swiper-slide",
                    slideBlankClass: "swiper-slide-invisible-blank",
                    slideActiveClass: "swiper-slide-active",
                    slideDuplicateActiveClass: "swiper-slide-duplicate-active",
                    slideVisibleClass: "swiper-slide-visible",
                    slideDuplicateClass: "swiper-slide-duplicate",
                    slideNextClass: "swiper-slide-next",
                    slideDuplicateNextClass: "swiper-slide-duplicate-next",
                    slidePrevClass: "swiper-slide-prev",
                    slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
                    wrapperClass: "swiper-wrapper",
                    runCallbacksOnInit: !0
                },
                $ = {
                    update: u,
                    translate: v,
                    transition: f,
                    slide: m,
                    loop: g,
                    grabCursor: b,
                    manipulation: w,
                    events: M,
                    breakpoints: z,
                    checkOverflow: {
                        checkOverflow: function() {
                            var e = this.isLocked;
                            this.isLocked = 1 === this.snapGrid.length, this.allowTouchMove = !this.isLocked, e && e !== this.isLocked && (this.isEnd = !1, this.navigation.update())
                        }
                    },
                    classes: {
                        addClasses: function() {
                            var e = this.classNames,
                                t = this.params,
                                i = this.rtl,
                                s = this.$el,
                                a = [];
                            a.push(t.direction), t.freeMode && a.push("free-mode"), h.flexbox || a.push("no-flexbox"), t.autoHeight && a.push("autoheight"), i && a.push("rtl"), t.slidesPerColumn > 1 && a.push("multirow"), y.android && a.push("android"), y.ios && a.push("ios"), P.isIE && (h.pointerEvents || h.prefixedPointerEvents) && a.push("wp8-" + t.direction), a.forEach(function(i) {
                                e.push(t.containerModifierClass + i)
                            }), s.addClass(e.join(" "))
                        },
                        removeClasses: function() {
                            var e = this.$el,
                                t = this.classNames;
                            e.removeClass(t.join(" "))
                        }
                    },
                    images: {
                        loadImage: function(e, i, s, a, r, n) {
                            var o;

                            function l() {
                                n && n()
                            }
                            e.complete && r ? l() : i ? ((o = new t.Image).onload = l, o.onerror = l, a && (o.sizes = a), s && (o.srcset = s), i && (o.src = i)) : l()
                        },
                        preloadImages: function() {
                            var e = this;

                            function t() {
                                void 0 !== e && null !== e && e && !e.destroyed && (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1), e.imagesLoaded === e.imagesToLoad.length && (e.params.updateOnImagesReady && e.update(), e.emit("imagesReady")))
                            }
                            e.imagesToLoad = e.$el.find("img");
                            for (var i = 0; i < e.imagesToLoad.length; i += 1) {
                                var s = e.imagesToLoad[i];
                                e.loadImage(s, s.currentSrc || s.getAttribute("src"), s.srcset || s.getAttribute("srcset"), s.sizes || s.getAttribute("sizes"), !0, t)
                            }
                        }
                    }
                },
                L = {},
                I = function(e) {
                    function t() {
                        for (var i, a, r, n = [], o = arguments.length; o--;) n[o] = arguments[o];
                        1 === n.length && n[0].constructor && n[0].constructor === Object ? a = n[0] : (i = (r = n)[0], a = r[1]);
                        a || (a = {}), a = d.extend({}, a), i && !a.el && (a.el = i), e.call(this, a), Object.keys($).forEach(function(e) {
                            Object.keys($[e]).forEach(function(i) {
                                t.prototype[i] || (t.prototype[i] = $[e][i])
                            })
                        });
                        var l = this;
                        void 0 === l.modules && (l.modules = {}), Object.keys(l.modules).forEach(function(e) {
                            var t = l.modules[e];
                            if (t.params) {
                                var i = Object.keys(t.params)[0],
                                    s = t.params[i];
                                if ("object" != typeof s) return;
                                if (!(i in a && "enabled" in s)) return;
                                !0 === a[i] && (a[i] = {
                                    enabled: !0
                                }), "object" != typeof a[i] || "enabled" in a[i] || (a[i].enabled = !0), a[i] || (a[i] = {
                                    enabled: !1
                                })
                            }
                        });
                        var p = d.extend({}, k);
                        l.useModulesParams(p), l.params = d.extend({}, p, L, a), l.originalParams = d.extend({}, l.params), l.passedParams = d.extend({}, a), l.$ = s;
                        var c = s(l.params.el);
                        if (i = c[0]) {
                            if (c.length > 1) {
                                var u = [];
                                return c.each(function(e, i) {
                                    var s = d.extend({}, a, {
                                        el: i
                                    });
                                    u.push(new t(s))
                                }), u
                            }
                            i.swiper = l, c.data("swiper", l);
                            var v, f, m = c.children("." + l.params.wrapperClass);
                            return d.extend(l, {
                                $el: c,
                                el: i,
                                $wrapperEl: m,
                                wrapperEl: m[0],
                                classNames: [],
                                slides: s(),
                                slidesGrid: [],
                                snapGrid: [],
                                slidesSizesGrid: [],
                                isHorizontal: function() {
                                    return "horizontal" === l.params.direction
                                },
                                isVertical: function() {
                                    return "vertical" === l.params.direction
                                },
                                rtl: "horizontal" === l.params.direction && ("rtl" === i.dir.toLowerCase() || "rtl" === c.css("direction")),
                                wrongRTL: "-webkit-box" === m.css("display"),
                                activeIndex: 0,
                                realIndex: 0,
                                isBeginning: !0,
                                isEnd: !1,
                                translate: 0,
                                progress: 0,
                                velocity: 0,
                                animating: !1,
                                allowSlideNext: l.params.allowSlideNext,
                                allowSlidePrev: l.params.allowSlidePrev,
                                touchEvents: (v = ["touchstart", "touchmove", "touchend"], f = ["mousedown", "mousemove", "mouseup"], h.pointerEvents ? f = ["pointerdown", "pointermove", "pointerup"] : h.prefixedPointerEvents && (f = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), l.touchEventsTouch = {
                                    start: v[0],
                                    move: v[1],
                                    end: v[2]
                                }, l.touchEventsDesktop = {
                                    start: f[0],
                                    move: f[1],
                                    end: f[2]
                                }, h.touch || !l.params.simulateTouch ? l.touchEventsTouch : l.touchEventsDesktop),
                                touchEventsData: {
                                    isTouched: void 0,
                                    isMoved: void 0,
                                    allowTouchCallbacks: void 0,
                                    touchStartTime: void 0,
                                    isScrolling: void 0,
                                    currentTranslate: void 0,
                                    startTranslate: void 0,
                                    allowThresholdMove: void 0,
                                    formElements: "input, select, option, textarea, button, video",
                                    lastClickTime: d.now(),
                                    clickTimeout: void 0,
                                    velocities: [],
                                    allowMomentumBounce: void 0,
                                    isTouchEvent: void 0,
                                    startMoving: void 0
                                },
                                allowClick: !0,
                                allowTouchMove: l.params.allowTouchMove,
                                touches: {
                                    startX: 0,
                                    startY: 0,
                                    currentX: 0,
                                    currentY: 0,
                                    diff: 0
                                },
                                imagesToLoad: [],
                                imagesLoaded: 0
                            }), l.useModules(), l.params.init && l.init(), l
                        }
                    }
                    e && (t.__proto__ = e), t.prototype = Object.create(e && e.prototype), t.prototype.constructor = t;
                    var i = {
                        extendedDefaults: {
                            configurable: !0
                        },
                        defaults: {
                            configurable: !0
                        },
                        Class: {
                            configurable: !0
                        },
                        $: {
                            configurable: !0
                        }
                    };
                    return t.prototype.slidesPerViewDynamic = function() {
                        var e = this.params,
                            t = this.slides,
                            i = this.slidesGrid,
                            s = this.size,
                            a = this.activeIndex,
                            r = 1;
                        if (e.centeredSlides) {
                            for (var n, o = t[a].swiperSlideSize, l = a + 1; l < t.length; l += 1) t[l] && !n && (r += 1, (o += t[l].swiperSlideSize) > s && (n = !0));
                            for (var d = a - 1; d >= 0; d -= 1) t[d] && !n && (r += 1, (o += t[d].swiperSlideSize) > s && (n = !0))
                        } else
                            for (var h = a + 1; h < t.length; h += 1) i[h] - i[a] < s && (r += 1);
                        return r
                    }, t.prototype.update = function() {
                        var e = this;
                        e && !e.destroyed && (e.updateSize(), e.updateSlides(), e.updateProgress(), e.updateSlidesClasses(), e.params.freeMode ? (t(), e.params.autoHeight && e.updateAutoHeight()) : (("auto" === e.params.slidesPerView || e.params.slidesPerView > 1) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0)) || t(), e.emit("update"));

                        function t() {
                            var t = e.rtl ? -1 * e.translate : e.translate,
                                i = Math.min(Math.max(t, e.maxTranslate()), e.minTranslate());
                            e.setTranslate(i), e.updateActiveIndex(), e.updateSlidesClasses()
                        }
                    }, t.prototype.init = function() {
                        this.initialized || (this.emit("beforeInit"), this.params.breakpoints && this.setBreakpoint(), this.addClasses(), this.params.loop && this.loopCreate(), this.updateSize(), this.updateSlides(), this.params.watchOverflow && this.checkOverflow(), this.params.grabCursor && this.setGrabCursor(), this.params.preloadImages && this.preloadImages(), this.params.loop ? this.slideTo(this.params.initialSlide + this.loopedSlides, 0, this.params.runCallbacksOnInit) : this.slideTo(this.params.initialSlide, 0, this.params.runCallbacksOnInit), this.attachEvents(), this.initialized = !0, this.emit("init"))
                    }, t.prototype.destroy = function(e, t) {
                        void 0 === e && (e = !0), void 0 === t && (t = !0);
                        var i = this,
                            s = i.params,
                            a = i.$el,
                            r = i.$wrapperEl,
                            n = i.slides;
                        i.emit("beforeDestroy"), i.initialized = !1, i.detachEvents(), s.loop && i.loopDestroy(), t && (i.removeClasses(), a.removeAttr("style"), r.removeAttr("style"), n && n.length && n.removeClass([s.slideVisibleClass, s.slideActiveClass, s.slideNextClass, s.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")), i.emit("destroy"), Object.keys(i.eventsListeners).forEach(function(e) {
                            i.off(e)
                        }), !1 !== e && (i.$el[0].swiper = null, i.$el.data("swiper", null), d.deleteProps(i)), i.destroyed = !0
                    }, t.extendDefaults = function(e) {
                        d.extend(L, e)
                    }, i.extendedDefaults.get = function() {
                        return L
                    }, i.defaults.get = function() {
                        return k
                    }, i.Class.get = function() {
                        return e
                    }, i.$.get = function() {
                        return s
                    }, Object.defineProperties(t, i), t
                }(p),
                D = {
                    name: "device",
                    proto: {
                        device: y
                    },
                    static: {
                        device: y
                    }
                },
                O = {
                    name: "support",
                    proto: {
                        support: h
                    },
                    static: {
                        support: h
                    }
                },
                A = {
                    name: "browser",
                    proto: {
                        browser: P
                    },
                    static: {
                        browser: P
                    }
                },
                H = {
                    name: "resize",
                    create: function() {
                        var e = this;
                        d.extend(e, {
                            resize: {
                                resizeHandler: function() {
                                    e && !e.destroyed && e.initialized && (e.emit("beforeResize"), e.emit("resize"))
                                },
                                orientationChangeHandler: function() {
                                    e && !e.destroyed && e.initialized && e.emit("orientationchange")
                                }
                            }
                        })
                    },
                    on: {
                        init: function() {
                            t.addEventListener("resize", this.resize.resizeHandler), t.addEventListener("orientationchange", this.resize.orientationChangeHandler)
                        },
                        destroy: function() {
                            t.removeEventListener("resize", this.resize.resizeHandler), t.removeEventListener("orientationchange", this.resize.orientationChangeHandler)
                        }
                    }
                },
                N = {
                    func: t.MutationObserver || t.WebkitMutationObserver,
                    attach: function(e, t) {
                        void 0 === t && (t = {});
                        var i = this,
                            s = new(0, N.func)(function(e) {
                                e.forEach(function(e) {
                                    i.emit("observerUpdate", e)
                                })
                            });
                        s.observe(e, {
                            attributes: void 0 === t.attributes || t.attributes,
                            childList: void 0 === t.childList || t.childList,
                            characterData: void 0 === t.characterData || t.characterData
                        }), i.observer.observers.push(s)
                    },
                    init: function() {
                        if (h.observer && this.params.observer) {
                            if (this.params.observeParents)
                                for (var e = this.$el.parents(), t = 0; t < e.length; t += 1) this.observer.attach(e[t]);
                            this.observer.attach(this.$el[0], {
                                childList: !1
                            }), this.observer.attach(this.$wrapperEl[0], {
                                attributes: !1
                            })
                        }
                    },
                    destroy: function() {
                        this.observer.observers.forEach(function(e) {
                            e.disconnect()
                        }), this.observer.observers = []
                    }
                },
                X = {
                    name: "observer",
                    params: {
                        observer: !1,
                        observeParents: !1
                    },
                    create: function() {
                        d.extend(this, {
                            observer: {
                                init: N.init.bind(this),
                                attach: N.attach.bind(this),
                                destroy: N.destroy.bind(this),
                                observers: []
                            }
                        })
                    },
                    on: {
                        init: function() {
                            this.observer.init()
                        },
                        destroy: function() {
                            this.observer.destroy()
                        }
                    }
                },
                Y = {
                    update: function(e) {
                        var t = this,
                            i = t.params,
                            s = i.slidesPerView,
                            a = i.slidesPerGroup,
                            r = i.centeredSlides,
                            n = t.virtual,
                            o = n.from,
                            l = n.to,
                            h = n.slides,
                            p = n.slidesGrid,
                            c = n.renderSlide,
                            u = n.offset;
                        t.updateActiveIndex();
                        var v, f, m, g = t.activeIndex || 0;
                        v = t.rtl && t.isHorizontal() ? "right" : t.isHorizontal() ? "left" : "top", r ? (f = Math.floor(s / 2) + a, m = Math.floor(s / 2) + a) : (f = s + (a - 1), m = a);
                        var b = Math.max((g || 0) - m, 0),
                            w = Math.min((g || 0) + f, h.length - 1),
                            y = (t.slidesGrid[b] || 0) - (t.slidesGrid[0] || 0);

                        function x() {
                            t.updateSlides(), t.updateProgress(), t.updateSlidesClasses(), t.lazy && t.params.lazy.enabled && t.lazy.load()
                        }
                        if (d.extend(t.virtual, {
                                from: b,
                                to: w,
                                offset: y,
                                slidesGrid: t.slidesGrid
                            }), o === b && l === w && !e) return t.slidesGrid !== p && y !== u && t.slides.css(v, y + "px"), void t.updateProgress();
                        if (t.params.virtual.renderExternal) return t.params.virtual.renderExternal.call(t, {
                            offset: y,
                            from: b,
                            to: w,
                            slides: function() {
                                for (var e = [], t = b; t <= w; t += 1) e.push(h[t]);
                                return e
                            }()
                        }), void x();
                        var E = [],
                            T = [];
                        if (e) t.$wrapperEl.find("." + t.params.slideClass).remove();
                        else
                            for (var S = o; S <= l; S += 1)(S < b || S > w) && t.$wrapperEl.find("." + t.params.slideClass + '[data-swiper-slide-index="' + S + '"]').remove();
                        for (var C = 0; C < h.length; C += 1) C >= b && C <= w && (void 0 === l || e ? T.push(C) : (C > l && T.push(C), C < o && E.push(C)));
                        T.forEach(function(e) {
                            t.$wrapperEl.append(c(h[e], e))
                        }), E.sort(function(e, t) {
                            return e < t
                        }).forEach(function(e) {
                            t.$wrapperEl.prepend(c(h[e], e))
                        }), t.$wrapperEl.children(".swiper-slide").css(v, y + "px"), x()
                    },
                    renderSlide: function(e, t) {
                        var i = this.params.virtual;
                        if (i.cache && this.virtual.cache[t]) return this.virtual.cache[t];
                        var a = i.renderSlide ? s(i.renderSlide.call(this, e, t)) : s('<div class="' + this.params.slideClass + '" data-swiper-slide-index="' + t + '">' + e + "</div>");
                        return a.attr("data-swiper-slide-index") || a.attr("data-swiper-slide-index", t), i.cache && (this.virtual.cache[t] = a), a
                    },
                    appendSlide: function(e) {
                        this.virtual.slides.push(e), this.virtual.update(!0)
                    },
                    prependSlide: function(e) {
                        if (this.virtual.slides.unshift(e), this.params.virtual.cache) {
                            var t = this.virtual.cache,
                                i = {};
                            Object.keys(t).forEach(function(e) {
                                i[e + 1] = t[e]
                            }), this.virtual.cache = i
                        }
                        this.virtual.update(!0), this.slideNext(0)
                    }
                },
                B = {
                    name: "virtual",
                    params: {
                        virtual: {
                            enabled: !1,
                            slides: [],
                            cache: !0,
                            renderSlide: null,
                            renderExternal: null
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            virtual: {
                                update: Y.update.bind(this),
                                appendSlide: Y.appendSlide.bind(this),
                                prependSlide: Y.prependSlide.bind(this),
                                renderSlide: Y.renderSlide.bind(this),
                                slides: this.params.virtual.slides,
                                cache: {}
                            }
                        })
                    },
                    on: {
                        beforeInit: function() {
                            if (this.params.virtual.enabled) {
                                this.classNames.push(this.params.containerModifierClass + "virtual");
                                var e = {
                                    watchSlidesProgress: !0
                                };
                                d.extend(this.params, e), d.extend(this.originalParams, e), this.virtual.update()
                            }
                        },
                        setTranslate: function() {
                            this.params.virtual.enabled && this.virtual.update()
                        }
                    }
                },
                G = {
                    handle: function(i) {
                        var s = i;
                        s.originalEvent && (s = s.originalEvent);
                        var a = s.keyCode || s.charCode;
                        if (!this.allowSlideNext && (this.isHorizontal() && 39 === a || this.isVertical() && 40 === a)) return !1;
                        if (!this.allowSlidePrev && (this.isHorizontal() && 37 === a || this.isVertical() && 38 === a)) return !1;
                        if (!(s.shiftKey || s.altKey || s.ctrlKey || s.metaKey || e.activeElement && e.activeElement.nodeName && ("input" === e.activeElement.nodeName.toLowerCase() || "textarea" === e.activeElement.nodeName.toLowerCase()))) {
                            if (this.params.keyboard.onlyInViewport && (37 === a || 39 === a || 38 === a || 40 === a)) {
                                var r = !1;
                                if (this.$el.parents("." + this.params.slideClass).length > 0 && 0 === this.$el.parents("." + this.params.slideActiveClass).length) return;
                                var n = t.innerWidth,
                                    o = t.innerHeight,
                                    l = this.$el.offset();
                                this.rtl && (l.left -= this.$el[0].scrollLeft);
                                for (var d = [
                                        [l.left, l.top],
                                        [l.left + this.width, l.top],
                                        [l.left, l.top + this.height],
                                        [l.left + this.width, l.top + this.height]
                                    ], h = 0; h < d.length; h += 1) {
                                    var p = d[h];
                                    p[0] >= 0 && p[0] <= n && p[1] >= 0 && p[1] <= o && (r = !0)
                                }
                                if (!r) return
                            }
                            this.isHorizontal() ? (37 !== a && 39 !== a || (s.preventDefault ? s.preventDefault() : s.returnValue = !1), (39 === a && !this.rtl || 37 === a && this.rtl) && this.slideNext(), (37 === a && !this.rtl || 39 === a && this.rtl) && this.slidePrev()) : (38 !== a && 40 !== a || (s.preventDefault ? s.preventDefault() : s.returnValue = !1), 40 === a && this.slideNext(), 38 === a && this.slidePrev()), this.emit("keyPress", a)
                        }
                    },
                    enable: function() {
                        this.keyboard.enabled || (s(e).on("keydown", this.keyboard.handle), this.keyboard.enabled = !0)
                    },
                    disable: function() {
                        this.keyboard.enabled && (s(e).off("keydown", this.keyboard.handle), this.keyboard.enabled = !1)
                    }
                },
                V = {
                    name: "keyboard",
                    params: {
                        keyboard: {
                            enabled: !1,
                            onlyInViewport: !0
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            keyboard: {
                                enabled: !1,
                                enable: G.enable.bind(this),
                                disable: G.disable.bind(this),
                                handle: G.handle.bind(this)
                            }
                        })
                    },
                    on: {
                        init: function() {
                            this.params.keyboard.enabled && this.keyboard.enable()
                        },
                        destroy: function() {
                            this.keyboard.enabled && this.keyboard.disable()
                        }
                    }
                };
            var R = {
                    lastScrollTime: d.now(),
                    event: t.navigator.userAgent.indexOf("firefox") > -1 ? "DOMMouseScroll" : function() {
                        var t = "onwheel" in e;
                        if (!t) {
                            var i = e.createElement("div");
                            i.setAttribute("onwheel", "return;"), t = "function" == typeof i.onwheel
                        }
                        return !t && e.implementation && e.implementation.hasFeature && !0 !== e.implementation.hasFeature("", "") && (t = e.implementation.hasFeature("Events.wheel", "3.0")), t
                    }() ? "wheel" : "mousewheel",
                    normalize: function(e) {
                        var t = 0,
                            i = 0,
                            s = 0,
                            a = 0;
                        return "detail" in e && (i = e.detail), "wheelDelta" in e && (i = -e.wheelDelta / 120), "wheelDeltaY" in e && (i = -e.wheelDeltaY / 120), "wheelDeltaX" in e && (t = -e.wheelDeltaX / 120), "axis" in e && e.axis === e.HORIZONTAL_AXIS && (t = i, i = 0), s = 10 * t, a = 10 * i, "deltaY" in e && (a = e.deltaY), "deltaX" in e && (s = e.deltaX), (s || a) && e.deltaMode && (1 === e.deltaMode ? (s *= 40, a *= 40) : (s *= 800, a *= 800)), s && !t && (t = s < 1 ? -1 : 1), a && !i && (i = a < 1 ? -1 : 1), {
                            spinX: t,
                            spinY: i,
                            pixelX: s,
                            pixelY: a
                        }
                    },
                    handle: function(e) {
                        var i = e,
                            s = this,
                            a = s.params.mousewheel;
                        i.originalEvent && (i = i.originalEvent);
                        var r = 0,
                            n = s.rtl ? -1 : 1,
                            o = R.normalize(i);
                        if (a.forceToAxis)
                            if (s.isHorizontal()) {
                                if (!(Math.abs(o.pixelX) > Math.abs(o.pixelY))) return !0;
                                r = o.pixelX * n
                            } else {
                                if (!(Math.abs(o.pixelY) > Math.abs(o.pixelX))) return !0;
                                r = o.pixelY
                            } else r = Math.abs(o.pixelX) > Math.abs(o.pixelY) ? -o.pixelX * n : -o.pixelY;
                        if (0 === r) return !0;
                        if (a.invert && (r = -r), s.params.freeMode) {
                            var l = s.getTranslate() + r * a.sensitivity,
                                h = s.isBeginning,
                                p = s.isEnd;
                            if (l >= s.minTranslate() && (l = s.minTranslate()), l <= s.maxTranslate() && (l = s.maxTranslate()), s.setTransition(0), s.setTranslate(l), s.updateProgress(), s.updateActiveIndex(), s.updateSlidesClasses(), (!h && s.isBeginning || !p && s.isEnd) && s.updateSlidesClasses(), s.params.freeModeSticky && (clearTimeout(s.mousewheel.timeout), s.mousewheel.timeout = d.nextTick(function() {
                                    s.slideReset()
                                }, 300)), s.emit("scroll", i), s.params.autoplay && s.params.autoplayDisableOnInteraction && s.stopAutoplay(), l === s.minTranslate() || l === s.maxTranslate()) return !0
                        } else {
                            if (d.now() - s.mousewheel.lastScrollTime > 60)
                                if (r < 0)
                                    if (s.isEnd && !s.params.loop || s.animating) {
                                        if (a.releaseOnEdges) return !0
                                    } else s.slideNext(), s.emit("scroll", i);
                            else if (s.isBeginning && !s.params.loop || s.animating) {
                                if (a.releaseOnEdges) return !0
                            } else s.slidePrev(), s.emit("scroll", i);
                            s.mousewheel.lastScrollTime = (new t.Date).getTime()
                        }
                        return i.preventDefault ? i.preventDefault() : i.returnValue = !1, !1
                    },
                    enable: function() {
                        if (!R.event) return !1;
                        if (this.mousewheel.enabled) return !1;
                        var e = this.$el;
                        return "container" !== this.params.mousewheel.eventsTarged && (e = s(this.params.mousewheel.eventsTarged)), e.on(R.event, this.mousewheel.handle), this.mousewheel.enabled = !0, !0
                    },
                    disable: function() {
                        if (!R.event) return !1;
                        if (!this.mousewheel.enabled) return !1;
                        var e = this.$el;
                        return "container" !== this.params.mousewheel.eventsTarged && (e = s(this.params.mousewheel.eventsTarged)), e.off(R.event, this.mousewheel.handle), this.mousewheel.enabled = !1, !0
                    }
                },
                F = {
                    update: function() {
                        var e = this.params.navigation;
                        if (!this.params.loop) {
                            var t = this.navigation,
                                i = t.$nextEl,
                                s = t.$prevEl;
                            s && s.length > 0 && (this.isBeginning ? s.addClass(e.disabledClass) : s.removeClass(e.disabledClass), s[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass)), i && i.length > 0 && (this.isEnd ? i.addClass(e.disabledClass) : i.removeClass(e.disabledClass), i[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass))
                        }
                    },
                    init: function() {
                        var e, t, i = this,
                            a = i.params.navigation;
                        (a.nextEl || a.prevEl) && (a.nextEl && (e = s(a.nextEl), i.params.uniqueNavElements && "string" == typeof a.nextEl && e.length > 1 && 1 === i.$el.find(a.nextEl).length && (e = i.$el.find(a.nextEl))), a.prevEl && (t = s(a.prevEl), i.params.uniqueNavElements && "string" == typeof a.prevEl && t.length > 1 && 1 === i.$el.find(a.prevEl).length && (t = i.$el.find(a.prevEl))), e && e.length > 0 && e.on("click", function(e) {
                            e.preventDefault(), i.isEnd && !i.params.loop || i.slideNext()
                        }), t && t.length > 0 && t.on("click", function(e) {
                            e.preventDefault(), i.isBeginning && !i.params.loop || i.slidePrev()
                        }), d.extend(i.navigation, {
                            $nextEl: e,
                            nextEl: e && e[0],
                            $prevEl: t,
                            prevEl: t && t[0]
                        }))
                    },
                    destroy: function() {
                        var e = this.navigation,
                            t = e.$nextEl,
                            i = e.$prevEl;
                        t && t.length && (t.off("click"), t.removeClass(this.params.navigation.disabledClass)), i && i.length && (i.off("click"), i.removeClass(this.params.navigation.disabledClass))
                    }
                },
                W = {
                    update: function() {
                        var e = this.rtl,
                            t = this.params.pagination;
                        if (t.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                            var i, a = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
                                r = this.pagination.$el,
                                n = this.params.loop ? Math.ceil((a - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length;
                            if (this.params.loop ? ((i = Math.ceil((this.activeIndex - this.loopedSlides) / this.params.slidesPerGroup)) > a - 1 - 2 * this.loopedSlides && (i -= a - 2 * this.loopedSlides), i > n - 1 && (i -= n), i < 0 && "bullets" !== this.params.paginationType && (i = n + i)) : i = void 0 !== this.snapIndex ? this.snapIndex : this.activeIndex || 0, "bullets" === t.type && this.pagination.bullets && this.pagination.bullets.length > 0) {
                                var o, l, d, h = this.pagination.bullets;
                                if (t.dynamicBullets && (this.pagination.bulletSize = h.eq(0)[this.isHorizontal() ? "outerWidth" : "outerHeight"](!0), r.css(this.isHorizontal() ? "width" : "height", this.pagination.bulletSize * (t.dynamicMainBullets + 4) + "px"), t.dynamicMainBullets > 1 && void 0 !== this.previousIndex && (i > this.previousIndex && this.pagination.dynamicBulletIndex < t.dynamicMainBullets - 1 ? this.pagination.dynamicBulletIndex += 1 : i < this.previousIndex && this.pagination.dynamicBulletIndex > 0 && (this.pagination.dynamicBulletIndex -= 1)), o = i - this.pagination.dynamicBulletIndex, d = ((l = o + (t.dynamicMainBullets - 1)) + o) / 2), h.removeClass(t.bulletActiveClass + " " + t.bulletActiveClass + "-next " + t.bulletActiveClass + "-next-next " + t.bulletActiveClass + "-prev " + t.bulletActiveClass + "-prev-prev " + t.bulletActiveClass + "-main"), r.length > 1) h.each(function(e, a) {
                                    var r = s(a),
                                        n = r.index();
                                    n === i && r.addClass(t.bulletActiveClass), t.dynamicBullets && (n >= o && n <= l && r.addClass(t.bulletActiveClass + "-main"), n === o && r.prev().addClass(t.bulletActiveClass + "-prev").prev().addClass(t.bulletActiveClass + "-prev-prev"), n === l && r.next().addClass(t.bulletActiveClass + "-next").next().addClass(t.bulletActiveClass + "-next-next"))
                                });
                                else if (h.eq(i).addClass(t.bulletActiveClass), t.dynamicBullets) {
                                    for (var p = h.eq(o), c = h.eq(l), u = o; u <= l; u += 1) h.eq(u).addClass(t.bulletActiveClass + "-main");
                                    p.prev().addClass(t.bulletActiveClass + "-prev").prev().addClass(t.bulletActiveClass + "-prev-prev"), c.next().addClass(t.bulletActiveClass + "-next").next().addClass(t.bulletActiveClass + "-next-next")
                                }
                                if (t.dynamicBullets) {
                                    var v = Math.min(h.length, t.dynamicMainBullets + 4),
                                        f = (this.pagination.bulletSize * v - this.pagination.bulletSize) / 2 - d * this.pagination.bulletSize,
                                        m = e ? "right" : "left";
                                    h.css(this.isHorizontal() ? m : "top", f + "px")
                                }
                            }
                            if ("fraction" === t.type && (r.find("." + t.currentClass).text(i + 1), r.find("." + t.totalClass).text(n)), "progressbar" === t.type) {
                                var g = (i + 1) / n,
                                    b = g,
                                    w = 1;
                                this.isHorizontal() || (w = g, b = 1), r.find("." + t.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + b + ") scaleY(" + w + ")").transition(this.params.speed)
                            }
                            "custom" === t.type && t.renderCustom ? (r.html(t.renderCustom(this, i + 1, n)), this.emit("paginationRender", this, r[0])) : this.emit("paginationUpdate", this, r[0]), r[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](t.lockClass)
                        }
                    },
                    render: function() {
                        var e = this.params.pagination;
                        if (e.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                            var t = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
                                i = this.pagination.$el,
                                s = "";
                            if ("bullets" === e.type) {
                                for (var a = this.params.loop ? Math.ceil((t - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length, r = 0; r < a; r += 1) e.renderBullet ? s += e.renderBullet.call(this, r, e.bulletClass) : s += "<" + e.bulletElement + ' class="' + e.bulletClass + '"></' + e.bulletElement + ">";
                                i.html(s), this.pagination.bullets = i.find("." + e.bulletClass)
                            }
                            "fraction" === e.type && (s = e.renderFraction ? e.renderFraction.call(this, e.currentClass, e.totalClass) : '<span class="' + e.currentClass + '"></span> / <span class="' + e.totalClass + '"></span>', i.html(s)), "progressbar" === e.type && (s = e.renderProgressbar ? e.renderProgressbar.call(this, e.progressbarFillClass) : '<span class="' + e.progressbarFillClass + '"></span>', i.html(s)), "custom" !== e.type && this.emit("paginationRender", this.pagination.$el[0])
                        }
                    },
                    init: function() {
                        var e = this,
                            t = e.params.pagination;
                        if (t.el) {
                            var i = s(t.el);
                            0 !== i.length && (e.params.uniqueNavElements && "string" == typeof t.el && i.length > 1 && 1 === e.$el.find(t.el).length && (i = e.$el.find(t.el)), "bullets" === t.type && t.clickable && i.addClass(t.clickableClass), i.addClass(t.modifierClass + t.type), "bullets" === t.type && t.dynamicBullets && (i.addClass("" + t.modifierClass + t.type + "-dynamic"), e.pagination.dynamicBulletIndex = 0, t.dynamicMainBullets < 1 && (t.dynamicMainBullets = 1)), t.clickable && i.on("click", "." + t.bulletClass, function(t) {
                                t.preventDefault();
                                var i = s(this).index() * e.params.slidesPerGroup;
                                e.params.loop && (i += e.loopedSlides), e.slideTo(i)
                            }), d.extend(e.pagination, {
                                $el: i,
                                el: i[0]
                            }))
                        }
                    },
                    destroy: function() {
                        var e = this.params.pagination;
                        if (e.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                            var t = this.pagination.$el;
                            t.removeClass(e.hiddenClass), t.removeClass(e.modifierClass + e.type), this.pagination.bullets && this.pagination.bullets.removeClass(e.bulletActiveClass), e.clickable && t.off("click", "." + e.bulletClass)
                        }
                    }
                },
                q = {
                    setTranslate: function() {
                        if (this.params.scrollbar.el && this.scrollbar.el) {
                            var e = this.scrollbar,
                                t = this.rtl,
                                i = this.progress,
                                s = e.dragSize,
                                a = e.trackSize,
                                r = e.$dragEl,
                                n = e.$el,
                                o = this.params.scrollbar,
                                l = s,
                                d = (a - s) * i;
                            t && this.isHorizontal() ? (d = -d) > 0 ? (l = s - d, d = 0) : -d + s > a && (l = a + d) : d < 0 ? (l = s + d, d = 0) : d + s > a && (l = a - d), this.isHorizontal() ? (h.transforms3d ? r.transform("translate3d(" + d + "px, 0, 0)") : r.transform("translateX(" + d + "px)"), r[0].style.width = l + "px") : (h.transforms3d ? r.transform("translate3d(0px, " + d + "px, 0)") : r.transform("translateY(" + d + "px)"), r[0].style.height = l + "px"), o.hide && (clearTimeout(this.scrollbar.timeout), n[0].style.opacity = 1, this.scrollbar.timeout = setTimeout(function() {
                                n[0].style.opacity = 0, n.transition(400)
                            }, 1e3))
                        }
                    },
                    setTransition: function(e) {
                        this.params.scrollbar.el && this.scrollbar.el && this.scrollbar.$dragEl.transition(e)
                    },
                    updateSize: function() {
                        if (this.params.scrollbar.el && this.scrollbar.el) {
                            var e = this.scrollbar,
                                t = e.$dragEl,
                                i = e.$el;
                            t[0].style.width = "", t[0].style.height = "";
                            var s, a = this.isHorizontal() ? i[0].offsetWidth : i[0].offsetHeight,
                                r = this.size / this.virtualSize,
                                n = r * (a / this.size);
                            s = "auto" === this.params.scrollbar.dragSize ? a * r : parseInt(this.params.scrollbar.dragSize, 10), this.isHorizontal() ? t[0].style.width = s + "px" : t[0].style.height = s + "px", i[0].style.display = r >= 1 ? "none" : "", this.params.scrollbarHide && (i[0].style.opacity = 0), d.extend(e, {
                                trackSize: a,
                                divider: r,
                                moveDivider: n,
                                dragSize: s
                            }), e.$el[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](this.params.scrollbar.lockClass)
                        }
                    },
                    setDragPosition: function(e) {
                        var t, i = this.scrollbar,
                            s = i.$el,
                            a = i.dragSize,
                            r = i.trackSize;
                        t = ((this.isHorizontal() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX || e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY || e.clientY) - s.offset()[this.isHorizontal() ? "left" : "top"] - a / 2) / (r - a), t = Math.max(Math.min(t, 1), 0), this.rtl && (t = 1 - t);
                        var n = this.minTranslate() + (this.maxTranslate() - this.minTranslate()) * t;
                        this.updateProgress(n), this.setTranslate(n), this.updateActiveIndex(), this.updateSlidesClasses()
                    },
                    onDragStart: function(e) {
                        var t = this.params.scrollbar,
                            i = this.scrollbar,
                            s = this.$wrapperEl,
                            a = i.$el,
                            r = i.$dragEl;
                        this.scrollbar.isTouched = !0, e.preventDefault(), e.stopPropagation(), s.transition(100), r.transition(100), i.setDragPosition(e), clearTimeout(this.scrollbar.dragTimeout), a.transition(0), t.hide && a.css("opacity", 1), this.emit("scrollbarDragStart", e)
                    },
                    onDragMove: function(e) {
                        var t = this.scrollbar,
                            i = this.$wrapperEl,
                            s = t.$el,
                            a = t.$dragEl;
                        this.scrollbar.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1, t.setDragPosition(e), i.transition(0), s.transition(0), a.transition(0), this.emit("scrollbarDragMove", e))
                    },
                    onDragEnd: function(e) {
                        var t = this.params.scrollbar,
                            i = this.scrollbar.$el;
                        this.scrollbar.isTouched && (this.scrollbar.isTouched = !1, t.hide && (clearTimeout(this.scrollbar.dragTimeout), this.scrollbar.dragTimeout = d.nextTick(function() {
                            i.css("opacity", 0), i.transition(400)
                        }, 1e3)), this.emit("scrollbarDragEnd", e), t.snapOnRelease && this.slideReset())
                    },
                    enableDraggable: function() {
                        if (this.params.scrollbar.el) {
                            var t = this.scrollbar,
                                i = this.touchEvents,
                                s = this.touchEventsDesktop,
                                a = this.params,
                                r = t.$el[0],
                                n = !(!h.passiveListener || !a.passiveListener) && {
                                    passive: !1,
                                    capture: !1
                                },
                                o = !(!h.passiveListener || !a.passiveListener) && {
                                    passive: !0,
                                    capture: !1
                                };
                            h.touch || !h.pointerEvents && !h.prefixedPointerEvents ? (h.touch && (r.addEventListener(i.start, this.scrollbar.onDragStart, n), r.addEventListener(i.move, this.scrollbar.onDragMove, n), r.addEventListener(i.end, this.scrollbar.onDragEnd, o)), (a.simulateTouch && !y.ios && !y.android || a.simulateTouch && !h.touch && y.ios) && (r.addEventListener("mousedown", this.scrollbar.onDragStart, n), e.addEventListener("mousemove", this.scrollbar.onDragMove, n), e.addEventListener("mouseup", this.scrollbar.onDragEnd, o))) : (r.addEventListener(s.start, this.scrollbar.onDragStart, n), e.addEventListener(s.move, this.scrollbar.onDragMove, n), e.addEventListener(s.end, this.scrollbar.onDragEnd, o))
                        }
                    },
                    disableDraggable: function() {
                        if (this.params.scrollbar.el) {
                            var t = this.scrollbar,
                                i = this.touchEvents,
                                s = this.touchEventsDesktop,
                                a = this.params,
                                r = t.$el[0],
                                n = !(!h.passiveListener || !a.passiveListener) && {
                                    passive: !1,
                                    capture: !1
                                },
                                o = !(!h.passiveListener || !a.passiveListener) && {
                                    passive: !0,
                                    capture: !1
                                };
                            h.touch || !h.pointerEvents && !h.prefixedPointerEvents ? (h.touch && (r.removeEventListener(i.start, this.scrollbar.onDragStart, n), r.removeEventListener(i.move, this.scrollbar.onDragMove, n), r.removeEventListener(i.end, this.scrollbar.onDragEnd, o)), (a.simulateTouch && !y.ios && !y.android || a.simulateTouch && !h.touch && y.ios) && (r.removeEventListener("mousedown", this.scrollbar.onDragStart, n), e.removeEventListener("mousemove", this.scrollbar.onDragMove, n), e.removeEventListener("mouseup", this.scrollbar.onDragEnd, o))) : (r.removeEventListener(s.start, this.scrollbar.onDragStart, n), e.removeEventListener(s.move, this.scrollbar.onDragMove, n), e.removeEventListener(s.end, this.scrollbar.onDragEnd, o))
                        }
                    },
                    init: function() {
                        if (this.params.scrollbar.el) {
                            var e = this.scrollbar,
                                t = this.$el,
                                i = this.params.scrollbar,
                                a = s(i.el);
                            this.params.uniqueNavElements && "string" == typeof i.el && a.length > 1 && 1 === t.find(i.el).length && (a = t.find(i.el));
                            var r = a.find("." + this.params.scrollbar.dragClass);
                            0 === r.length && (r = s('<div class="' + this.params.scrollbar.dragClass + '"></div>'), a.append(r)), d.extend(e, {
                                $el: a,
                                el: a[0],
                                $dragEl: r,
                                dragEl: r[0]
                            }), i.draggable && e.enableDraggable()
                        }
                    },
                    destroy: function() {
                        this.scrollbar.disableDraggable()
                    }
                },
                j = {
                    setTransform: function(e, t) {
                        var i = this.rtl,
                            a = s(e),
                            r = i ? -1 : 1,
                            n = a.attr("data-swiper-parallax") || "0",
                            o = a.attr("data-swiper-parallax-x"),
                            l = a.attr("data-swiper-parallax-y"),
                            d = a.attr("data-swiper-parallax-scale"),
                            h = a.attr("data-swiper-parallax-opacity");
                        if (o || l ? (o = o || "0", l = l || "0") : this.isHorizontal() ? (o = n, l = "0") : (l = n, o = "0"), o = o.indexOf("%") >= 0 ? parseInt(o, 10) * t * r + "%" : o * t * r + "px", l = l.indexOf("%") >= 0 ? parseInt(l, 10) * t + "%" : l * t + "px", void 0 !== h && null !== h) {
                            var p = h - (h - 1) * (1 - Math.abs(t));
                            a[0].style.opacity = p
                        }
                        if (void 0 === d || null === d) a.transform("translate3d(" + o + ", " + l + ", 0px)");
                        else {
                            var c = d - (d - 1) * (1 - Math.abs(t));
                            a.transform("translate3d(" + o + ", " + l + ", 0px) scale(" + c + ")")
                        }
                    },
                    setTranslate: function() {
                        var e = this,
                            t = e.$el,
                            i = e.slides,
                            a = e.progress,
                            r = e.snapGrid;
                        t.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t, i) {
                            e.parallax.setTransform(i, a)
                        }), i.each(function(t, i) {
                            var n = i.progress;
                            e.params.slidesPerGroup > 1 && "auto" !== e.params.slidesPerView && (n += Math.ceil(t / 2) - a * (r.length - 1)), n = Math.min(Math.max(n, -1), 1), s(i).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t, i) {
                                e.parallax.setTransform(i, n)
                            })
                        })
                    },
                    setTransition: function(e) {
                        void 0 === e && (e = this.params.speed);
                        this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t, i) {
                            var a = s(i),
                                r = parseInt(a.attr("data-swiper-parallax-duration"), 10) || e;
                            0 === e && (r = 0), a.transition(r)
                        })
                    }
                },
                K = {
                    getDistanceBetweenTouches: function(e) {
                        if (e.targetTouches.length < 2) return 1;
                        var t = e.targetTouches[0].pageX,
                            i = e.targetTouches[0].pageY,
                            s = e.targetTouches[1].pageX,
                            a = e.targetTouches[1].pageY;
                        return Math.sqrt(Math.pow(s - t, 2) + Math.pow(a - i, 2))
                    },
                    onGestureStart: function(e) {
                        var t = this.params.zoom,
                            i = this.zoom,
                            a = i.gesture;
                        if (i.fakeGestureTouched = !1, i.fakeGestureMoved = !1, !h.gestures) {
                            if ("touchstart" !== e.type || "touchstart" === e.type && e.targetTouches.length < 2) return;
                            i.fakeGestureTouched = !0, a.scaleStart = K.getDistanceBetweenTouches(e)
                        }
                        a.$slideEl && a.$slideEl.length || (a.$slideEl = s(e.target).closest(".swiper-slide"), 0 === a.$slideEl.length && (a.$slideEl = this.slides.eq(this.activeIndex)), a.$imageEl = a.$slideEl.find("img, svg, canvas"), a.$imageWrapEl = a.$imageEl.parent("." + t.containerClass), a.maxRatio = a.$imageWrapEl.attr("data-swiper-zoom") || t.maxRatio, 0 !== a.$imageWrapEl.length) ? (a.$imageEl.transition(0), this.zoom.isScaling = !0) : a.$imageEl = void 0
                    },
                    onGestureChange: function(e) {
                        var t = this.params.zoom,
                            i = this.zoom,
                            s = i.gesture;
                        if (!h.gestures) {
                            if ("touchmove" !== e.type || "touchmove" === e.type && e.targetTouches.length < 2) return;
                            i.fakeGestureMoved = !0, s.scaleMove = K.getDistanceBetweenTouches(e)
                        }
                        s.$imageEl && 0 !== s.$imageEl.length && (h.gestures ? this.zoom.scale = e.scale * i.currentScale : i.scale = s.scaleMove / s.scaleStart * i.currentScale, i.scale > s.maxRatio && (i.scale = s.maxRatio - 1 + Math.pow(i.scale - s.maxRatio + 1, .5)), i.scale < t.minRatio && (i.scale = t.minRatio + 1 - Math.pow(t.minRatio - i.scale + 1, .5)), s.$imageEl.transform("translate3d(0,0,0) scale(" + i.scale + ")"))
                    },
                    onGestureEnd: function(e) {
                        var t = this.params.zoom,
                            i = this.zoom,
                            s = i.gesture;
                        if (!h.gestures) {
                            if (!i.fakeGestureTouched || !i.fakeGestureMoved) return;
                            if ("touchend" !== e.type || "touchend" === e.type && e.changedTouches.length < 2 && !y.android) return;
                            i.fakeGestureTouched = !1, i.fakeGestureMoved = !1
                        }
                        s.$imageEl && 0 !== s.$imageEl.length && (i.scale = Math.max(Math.min(i.scale, s.maxRatio), t.minRatio), s.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale(" + i.scale + ")"), i.currentScale = i.scale, i.isScaling = !1, 1 === i.scale && (s.$slideEl = void 0))
                    },
                    onTouchStart: function(e) {
                        var t = this.zoom,
                            i = t.gesture,
                            s = t.image;
                        i.$imageEl && 0 !== i.$imageEl.length && (s.isTouched || (y.android && e.preventDefault(), s.isTouched = !0, s.touchesStart.x = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX, s.touchesStart.y = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY))
                    },
                    onTouchMove: function(e) {
                        var t = this.zoom,
                            i = t.gesture,
                            s = t.image,
                            a = t.velocity;
                        if (i.$imageEl && 0 !== i.$imageEl.length && (this.allowClick = !1, s.isTouched && i.$slideEl)) {
                            s.isMoved || (s.width = i.$imageEl[0].offsetWidth, s.height = i.$imageEl[0].offsetHeight, s.startX = d.getTranslate(i.$imageWrapEl[0], "x") || 0, s.startY = d.getTranslate(i.$imageWrapEl[0], "y") || 0, i.slideWidth = i.$slideEl[0].offsetWidth, i.slideHeight = i.$slideEl[0].offsetHeight, i.$imageWrapEl.transition(0), this.rtl && (s.startX = -s.startX), this.rtl && (s.startY = -s.startY));
                            var r = s.width * t.scale,
                                n = s.height * t.scale;
                            if (!(r < i.slideWidth && n < i.slideHeight)) {
                                if (s.minX = Math.min(i.slideWidth / 2 - r / 2, 0), s.maxX = -s.minX, s.minY = Math.min(i.slideHeight / 2 - n / 2, 0), s.maxY = -s.minY, s.touchesCurrent.x = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, s.touchesCurrent.y = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, !s.isMoved && !t.isScaling) {
                                    if (this.isHorizontal() && (Math.floor(s.minX) === Math.floor(s.startX) && s.touchesCurrent.x < s.touchesStart.x || Math.floor(s.maxX) === Math.floor(s.startX) && s.touchesCurrent.x > s.touchesStart.x)) return void(s.isTouched = !1);
                                    if (!this.isHorizontal() && (Math.floor(s.minY) === Math.floor(s.startY) && s.touchesCurrent.y < s.touchesStart.y || Math.floor(s.maxY) === Math.floor(s.startY) && s.touchesCurrent.y > s.touchesStart.y)) return void(s.isTouched = !1)
                                }
                                e.preventDefault(), e.stopPropagation(), s.isMoved = !0, s.currentX = s.touchesCurrent.x - s.touchesStart.x + s.startX, s.currentY = s.touchesCurrent.y - s.touchesStart.y + s.startY, s.currentX < s.minX && (s.currentX = s.minX + 1 - Math.pow(s.minX - s.currentX + 1, .8)), s.currentX > s.maxX && (s.currentX = s.maxX - 1 + Math.pow(s.currentX - s.maxX + 1, .8)), s.currentY < s.minY && (s.currentY = s.minY + 1 - Math.pow(s.minY - s.currentY + 1, .8)), s.currentY > s.maxY && (s.currentY = s.maxY - 1 + Math.pow(s.currentY - s.maxY + 1, .8)), a.prevPositionX || (a.prevPositionX = s.touchesCurrent.x), a.prevPositionY || (a.prevPositionY = s.touchesCurrent.y), a.prevTime || (a.prevTime = Date.now()), a.x = (s.touchesCurrent.x - a.prevPositionX) / (Date.now() - a.prevTime) / 2, a.y = (s.touchesCurrent.y - a.prevPositionY) / (Date.now() - a.prevTime) / 2, Math.abs(s.touchesCurrent.x - a.prevPositionX) < 2 && (a.x = 0), Math.abs(s.touchesCurrent.y - a.prevPositionY) < 2 && (a.y = 0), a.prevPositionX = s.touchesCurrent.x, a.prevPositionY = s.touchesCurrent.y, a.prevTime = Date.now(), i.$imageWrapEl.transform("translate3d(" + s.currentX + "px, " + s.currentY + "px,0)")
                            }
                        }
                    },
                    onTouchEnd: function() {
                        var e = this.zoom,
                            t = e.gesture,
                            i = e.image,
                            s = e.velocity;
                        if (t.$imageEl && 0 !== t.$imageEl.length) {
                            if (!i.isTouched || !i.isMoved) return i.isTouched = !1, void(i.isMoved = !1);
                            i.isTouched = !1, i.isMoved = !1;
                            var a = 300,
                                r = 300,
                                n = s.x * a,
                                o = i.currentX + n,
                                l = s.y * r,
                                d = i.currentY + l;
                            0 !== s.x && (a = Math.abs((o - i.currentX) / s.x)), 0 !== s.y && (r = Math.abs((d - i.currentY) / s.y));
                            var h = Math.max(a, r);
                            i.currentX = o, i.currentY = d;
                            var p = i.width * e.scale,
                                c = i.height * e.scale;
                            i.minX = Math.min(t.slideWidth / 2 - p / 2, 0), i.maxX = -i.minX, i.minY = Math.min(t.slideHeight / 2 - c / 2, 0), i.maxY = -i.minY, i.currentX = Math.max(Math.min(i.currentX, i.maxX), i.minX), i.currentY = Math.max(Math.min(i.currentY, i.maxY), i.minY), t.$imageWrapEl.transition(h).transform("translate3d(" + i.currentX + "px, " + i.currentY + "px,0)")
                        }
                    },
                    onTransitionEnd: function() {
                        var e = this.zoom,
                            t = e.gesture;
                        t.$slideEl && this.previousIndex !== this.activeIndex && (t.$imageEl.transform("translate3d(0,0,0) scale(1)"), t.$imageWrapEl.transform("translate3d(0,0,0)"), t.$slideEl = void 0, t.$imageEl = void 0, t.$imageWrapEl = void 0, e.scale = 1, e.currentScale = 1)
                    },
                    toggle: function(e) {
                        var t = this.zoom;
                        t.scale && 1 !== t.scale ? t.out() : t.in(e)
                    },
                    in : function(e) {
                        var t, i, a, r, n, o, l, d, h, p, c, u, v, f, m, g, b = this.zoom,
                            w = this.params.zoom,
                            y = b.gesture,
                            x = b.image;
                        (y.$slideEl || (y.$slideEl = this.clickedSlide ? s(this.clickedSlide) : this.slides.eq(this.activeIndex), y.$imageEl = y.$slideEl.find("img, svg, canvas"), y.$imageWrapEl = y.$imageEl.parent("." + w.containerClass)), y.$imageEl && 0 !== y.$imageEl.length) && (y.$slideEl.addClass("" + w.zoomedSlideClass), void 0 === x.touchesStart.x && e ? (t = "touchend" === e.type ? e.changedTouches[0].pageX : e.pageX, i = "touchend" === e.type ? e.changedTouches[0].pageY : e.pageY) : (t = x.touchesStart.x, i = x.touchesStart.y), b.scale = y.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, b.currentScale = y.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, e ? (m = y.$slideEl[0].offsetWidth, g = y.$slideEl[0].offsetHeight, a = y.$slideEl.offset().left + m / 2 - t, r = y.$slideEl.offset().top + g / 2 - i, l = y.$imageEl[0].offsetWidth, d = y.$imageEl[0].offsetHeight, h = l * b.scale, p = d * b.scale, v = -(c = Math.min(m / 2 - h / 2, 0)), f = -(u = Math.min(g / 2 - p / 2, 0)), n = a * b.scale, o = r * b.scale, n < c && (n = c), n > v && (n = v), o < u && (o = u), o > f && (o = f)) : (n = 0, o = 0), y.$imageWrapEl.transition(300).transform("translate3d(" + n + "px, " + o + "px,0)"), y.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + b.scale + ")"))
                    },
                    out: function() {
                        var e = this.zoom,
                            t = this.params.zoom,
                            i = e.gesture;
                        i.$slideEl || (i.$slideEl = this.clickedSlide ? s(this.clickedSlide) : this.slides.eq(this.activeIndex), i.$imageEl = i.$slideEl.find("img, svg, canvas"), i.$imageWrapEl = i.$imageEl.parent("." + t.containerClass)), i.$imageEl && 0 !== i.$imageEl.length && (e.scale = 1, e.currentScale = 1, i.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"), i.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"), i.$slideEl.removeClass("" + t.zoomedSlideClass), i.$slideEl = void 0)
                    },
                    enable: function() {
                        var e = this.zoom;
                        if (!e.enabled) {
                            e.enabled = !0;
                            var t = !("touchstart" !== this.touchEvents.start || !h.passiveListener || !this.params.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                            h.gestures ? (this.$wrapperEl.on("gesturestart", ".swiper-slide", e.onGestureStart, t), this.$wrapperEl.on("gesturechange", ".swiper-slide", e.onGestureChange, t), this.$wrapperEl.on("gestureend", ".swiper-slide", e.onGestureEnd, t)) : "touchstart" === this.touchEvents.start && (this.$wrapperEl.on(this.touchEvents.start, ".swiper-slide", e.onGestureStart, t), this.$wrapperEl.on(this.touchEvents.move, ".swiper-slide", e.onGestureChange, t), this.$wrapperEl.on(this.touchEvents.end, ".swiper-slide", e.onGestureEnd, t)), this.$wrapperEl.on(this.touchEvents.move, "." + this.params.zoom.containerClass, e.onTouchMove)
                        }
                    },
                    disable: function() {
                        var e = this.zoom;
                        if (e.enabled) {
                            this.zoom.enabled = !1;
                            var t = !("touchstart" !== this.touchEvents.start || !h.passiveListener || !this.params.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                            h.gestures ? (this.$wrapperEl.off("gesturestart", ".swiper-slide", e.onGestureStart, t), this.$wrapperEl.off("gesturechange", ".swiper-slide", e.onGestureChange, t), this.$wrapperEl.off("gestureend", ".swiper-slide", e.onGestureEnd, t)) : "touchstart" === this.touchEvents.start && (this.$wrapperEl.off(this.touchEvents.start, ".swiper-slide", e.onGestureStart, t), this.$wrapperEl.off(this.touchEvents.move, ".swiper-slide", e.onGestureChange, t), this.$wrapperEl.off(this.touchEvents.end, ".swiper-slide", e.onGestureEnd, t)), this.$wrapperEl.off(this.touchEvents.move, "." + this.params.zoom.containerClass, e.onTouchMove)
                        }
                    }
                },
                U = {
                    loadInSlide: function(e, t) {
                        void 0 === t && (t = !0);
                        var i = this,
                            a = i.params.lazy;
                        if (void 0 !== e && 0 !== i.slides.length) {
                            var r = i.virtual && i.params.virtual.enabled ? i.$wrapperEl.children("." + i.params.slideClass + '[data-swiper-slide-index="' + e + '"]') : i.slides.eq(e),
                                n = r.find("." + a.elementClass + ":not(." + a.loadedClass + "):not(." + a.loadingClass + ")");
                            !r.hasClass(a.elementClass) || r.hasClass(a.loadedClass) || r.hasClass(a.loadingClass) || (n = n.add(r[0])), 0 !== n.length && n.each(function(e, n) {
                                var o = s(n);
                                o.addClass(a.loadingClass);
                                var l = o.attr("data-background"),
                                    d = o.attr("data-src"),
                                    h = o.attr("data-srcset"),
                                    p = o.attr("data-sizes");
                                i.loadImage(o[0], d || l, h, p, !1, function() {
                                    if (void 0 !== i && null !== i && i && (!i || i.params) && !i.destroyed) {
                                        if (l ? (o.css("background-image", 'url("' + l + '")'), o.removeAttr("data-background")) : (h && (o.attr("srcset", h), o.removeAttr("data-srcset")), p && (o.attr("sizes", p), o.removeAttr("data-sizes")), d && (o.attr("src", d), o.removeAttr("data-src"))), o.addClass(a.loadedClass).removeClass(a.loadingClass), r.find("." + a.preloaderClass).remove(), i.params.loop && t) {
                                            var e = r.attr("data-swiper-slide-index");
                                            if (r.hasClass(i.params.slideDuplicateClass)) {
                                                var s = i.$wrapperEl.children('[data-swiper-slide-index="' + e + '"]:not(.' + i.params.slideDuplicateClass + ")");
                                                i.lazy.loadInSlide(s.index(), !1)
                                            } else {
                                                var n = i.$wrapperEl.children("." + i.params.slideDuplicateClass + '[data-swiper-slide-index="' + e + '"]');
                                                i.lazy.loadInSlide(n.index(), !1)
                                            }
                                        }
                                        i.emit("lazyImageReady", r[0], o[0])
                                    }
                                }), i.emit("lazyImageLoad", r[0], o[0])
                            })
                        }
                    },
                    load: function() {
                        var e = this,
                            t = e.$wrapperEl,
                            i = e.params,
                            a = e.slides,
                            r = e.activeIndex,
                            n = e.virtual && i.virtual.enabled,
                            o = i.lazy,
                            l = i.slidesPerView;

                        function d(e) {
                            if (n) {
                                if (t.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]').length) return !0
                            } else if (a[e]) return !0;
                            return !1
                        }

                        function h(e) {
                            return n ? s(e).attr("data-swiper-slide-index") : s(e).index()
                        }
                        if ("auto" === l && (l = 0), e.lazy.initialImageLoaded || (e.lazy.initialImageLoaded = !0), e.params.watchSlidesVisibility) t.children("." + i.slideVisibleClass).each(function(t, i) {
                            var a = n ? s(i).attr("data-swiper-slide-index") : s(i).index();
                            e.lazy.loadInSlide(a)
                        });
                        else if (l > 1)
                            for (var p = r; p < r + l; p += 1) d(p) && e.lazy.loadInSlide(p);
                        else e.lazy.loadInSlide(r);
                        if (o.loadPrevNext)
                            if (l > 1 || o.loadPrevNextAmount && o.loadPrevNextAmount > 1) {
                                for (var c = o.loadPrevNextAmount, u = l, v = Math.min(r + u + Math.max(c, u), a.length), f = Math.max(r - Math.max(u, c), 0), m = r + l; m < v; m += 1) d(m) && e.lazy.loadInSlide(m);
                                for (var g = f; g < r; g += 1) d(g) && e.lazy.loadInSlide(g)
                            } else {
                                var b = t.children("." + i.slideNextClass);
                                b.length > 0 && e.lazy.loadInSlide(h(b));
                                var w = t.children("." + i.slidePrevClass);
                                w.length > 0 && e.lazy.loadInSlide(h(w))
                            }
                    }
                },
                _ = {
                    LinearSpline: function(e, t) {
                        var i, s, a, r, n, o = function(e, t) {
                            for (s = -1, i = e.length; i - s > 1;) e[a = i + s >> 1] <= t ? s = a : i = a;
                            return i
                        };
                        return this.x = e, this.y = t, this.lastIndex = e.length - 1, this.interpolate = function(e) {
                            return e ? (n = o(this.x, e), r = n - 1, (e - this.x[r]) * (this.y[n] - this.y[r]) / (this.x[n] - this.x[r]) + this.y[r]) : 0
                        }, this
                    },
                    getInterpolateFunction: function(e) {
                        this.controller.spline || (this.controller.spline = this.params.loop ? new _.LinearSpline(this.slidesGrid, e.slidesGrid) : new _.LinearSpline(this.snapGrid, e.snapGrid))
                    },
                    setTranslate: function(e, t) {
                        var i, s, a = this,
                            r = a.controller.control;

                        function n(e) {
                            var t = e.rtl && "horizontal" === e.params.direction ? -a.translate : a.translate;
                            "slide" === a.params.controller.by && (a.controller.getInterpolateFunction(e), s = -a.controller.spline.interpolate(-t)), s && "container" !== a.params.controller.by || (i = (e.maxTranslate() - e.minTranslate()) / (a.maxTranslate() - a.minTranslate()), s = (t - a.minTranslate()) * i + e.minTranslate()), a.params.controller.inverse && (s = e.maxTranslate() - s), e.updateProgress(s), e.setTranslate(s, a), e.updateActiveIndex(), e.updateSlidesClasses()
                        }
                        if (Array.isArray(r))
                            for (var o = 0; o < r.length; o += 1) r[o] !== t && r[o] instanceof I && n(r[o]);
                        else r instanceof I && t !== r && n(r)
                    },
                    setTransition: function(e, t) {
                        var i, s = this,
                            a = s.controller.control;

                        function r(t) {
                            t.setTransition(e, s), 0 !== e && (t.transitionStart(), t.$wrapperEl.transitionEnd(function() {
                                a && (t.params.loop && "slide" === s.params.controller.by && t.loopFix(), t.transitionEnd())
                            }))
                        }
                        if (Array.isArray(a))
                            for (i = 0; i < a.length; i += 1) a[i] !== t && a[i] instanceof I && r(a[i]);
                        else a instanceof I && t !== a && r(a)
                    }
                },
                Z = {
                    makeElFocusable: function(e) {
                        return e.attr("tabIndex", "0"), e
                    },
                    addElRole: function(e, t) {
                        return e.attr("role", t), e
                    },
                    addElLabel: function(e, t) {
                        return e.attr("aria-label", t), e
                    },
                    disableEl: function(e) {
                        return e.attr("aria-disabled", !0), e
                    },
                    enableEl: function(e) {
                        return e.attr("aria-disabled", !1), e
                    },
                    onEnterKey: function(e) {
                        var t = this.params.a11y;
                        if (13 === e.keyCode) {
                            var i = s(e.target);
                            this.navigation && this.navigation.$nextEl && i.is(this.navigation.$nextEl) && (this.isEnd && !this.params.loop || this.slideNext(), this.isEnd ? this.a11y.notify(t.lastSlideMessage) : this.a11y.notify(t.nextSlideMessage)), this.navigation && this.navigation.$prevEl && i.is(this.navigation.$prevEl) && (this.isBeginning && !this.params.loop || this.slidePrev(), this.isBeginning ? this.a11y.notify(t.firstSlideMessage) : this.a11y.notify(t.prevSlideMessage)), this.pagination && i.is("." + this.params.pagination.bulletClass) && i[0].click()
                        }
                    },
                    notify: function(e) {
                        var t = this.a11y.liveRegion;
                        0 !== t.length && (t.html(""), t.html(e))
                    },
                    updateNavigation: function() {
                        if (!this.params.loop) {
                            var e = this.navigation,
                                t = e.$nextEl,
                                i = e.$prevEl;
                            i && i.length > 0 && (this.isBeginning ? this.a11y.disableEl(i) : this.a11y.enableEl(i)), t && t.length > 0 && (this.isEnd ? this.a11y.disableEl(t) : this.a11y.enableEl(t))
                        }
                    },
                    updatePagination: function() {
                        var e = this,
                            t = e.params.a11y;
                        e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.bullets.each(function(i, a) {
                            var r = s(a);
                            e.a11y.makeElFocusable(r), e.a11y.addElRole(r, "button"), e.a11y.addElLabel(r, t.paginationBulletMessage.replace(/index/, r.index() + 1))
                        })
                    },
                    init: function() {
                        this.$el.append(this.a11y.liveRegion);
                        var e, t, i = this.params.a11y;
                        this.navigation && this.navigation.$nextEl && (e = this.navigation.$nextEl), this.navigation && this.navigation.$prevEl && (t = this.navigation.$prevEl), e && (this.a11y.makeElFocusable(e), this.a11y.addElRole(e, "button"), this.a11y.addElLabel(e, i.nextSlideMessage), e.on("keydown", this.a11y.onEnterKey)), t && (this.a11y.makeElFocusable(t), this.a11y.addElRole(t, "button"), this.a11y.addElLabel(t, i.prevSlideMessage), t.on("keydown", this.a11y.onEnterKey)), this.pagination && this.params.pagination.clickable && this.pagination.bullets && this.pagination.bullets.length && this.pagination.$el.on("keydown", "." + this.params.pagination.bulletClass, this.a11y.onEnterKey)
                    },
                    destroy: function() {
                        var e, t;
                        this.a11y.liveRegion && this.a11y.liveRegion.length > 0 && this.a11y.liveRegion.remove(), this.navigation && this.navigation.$nextEl && (e = this.navigation.$nextEl), this.navigation && this.navigation.$prevEl && (t = this.navigation.$prevEl), e && e.off("keydown", this.a11y.onEnterKey), t && t.off("keydown", this.a11y.onEnterKey), this.pagination && this.params.pagination.clickable && this.pagination.bullets && this.pagination.bullets.length && this.pagination.$el.off("keydown", "." + this.params.pagination.bulletClass, this.a11y.onEnterKey)
                    }
                },
                Q = {
                    init: function() {
                        if (this.params.history) {
                            if (!t.history || !t.history.pushState) return this.params.history.enabled = !1, void(this.params.hashNavigation.enabled = !0);
                            var e = this.history;
                            e.initialized = !0, e.paths = Q.getPathValues(), (e.paths.key || e.paths.value) && (e.scrollToSlide(0, e.paths.value, this.params.runCallbacksOnInit), this.params.history.replaceState || t.addEventListener("popstate", this.history.setHistoryPopState))
                        }
                    },
                    destroy: function() {
                        this.params.history.replaceState || t.removeEventListener("popstate", this.history.setHistoryPopState)
                    },
                    setHistoryPopState: function() {
                        this.history.paths = Q.getPathValues(), this.history.scrollToSlide(this.params.speed, this.history.paths.value, !1)
                    },
                    getPathValues: function() {
                        var e = t.location.pathname.slice(1).split("/").filter(function(e) {
                                return "" !== e
                            }),
                            i = e.length;
                        return {
                            key: e[i - 2],
                            value: e[i - 1]
                        }
                    },
                    setHistory: function(e, i) {
                        if (this.history.initialized && this.params.history.enabled) {
                            var s = this.slides.eq(i),
                                a = Q.slugify(s.attr("data-history"));
                            t.location.pathname.includes(e) || (a = e + "/" + a);
                            var r = t.history.state;
                            r && r.value === a || (this.params.history.replaceState ? t.history.replaceState({
                                value: a
                            }, null, a) : t.history.pushState({
                                value: a
                            }, null, a))
                        }
                    },
                    slugify: function(e) {
                        return e.toString().toLowerCase().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "")
                    },
                    scrollToSlide: function(e, t, i) {
                        if (t)
                            for (var s = 0, a = this.slides.length; s < a; s += 1) {
                                var r = this.slides.eq(s);
                                if (Q.slugify(r.attr("data-history")) === t && !r.hasClass(this.params.slideDuplicateClass)) {
                                    var n = r.index();
                                    this.slideTo(n, e, i)
                                }
                            } else this.slideTo(0, e, i)
                    }
                },
                J = {
                    onHashCange: function() {
                        var t = e.location.hash.replace("#", "");
                        t !== this.slides.eq(this.activeIndex).attr("data-hash") && this.slideTo(this.$wrapperEl.children("." + this.params.slideClass + '[data-hash="' + t + '"]').index())
                    },
                    setHash: function() {
                        if (this.hashNavigation.initialized && this.params.hashNavigation.enabled)
                            if (this.params.hashNavigation.replaceState && t.history && t.history.replaceState) t.history.replaceState(null, null, "#" + this.slides.eq(this.activeIndex).attr("data-hash") || "");
                            else {
                                var i = this.slides.eq(this.activeIndex),
                                    s = i.attr("data-hash") || i.attr("data-history");
                                e.location.hash = s || ""
                            }
                    },
                    init: function() {
                        if (!(!this.params.hashNavigation.enabled || this.params.history && this.params.history.enabled)) {
                            this.hashNavigation.initialized = !0;
                            var i = e.location.hash.replace("#", "");
                            if (i)
                                for (var a = 0, r = this.slides.length; a < r; a += 1) {
                                    var n = this.slides.eq(a);
                                    if ((n.attr("data-hash") || n.attr("data-history")) === i && !n.hasClass(this.params.slideDuplicateClass)) {
                                        var o = n.index();
                                        this.slideTo(o, 0, this.params.runCallbacksOnInit, !0)
                                    }
                                }
                            this.params.hashNavigation.watchState && s(t).on("hashchange", this.hashNavigation.onHashCange)
                        }
                    },
                    destroy: function() {
                        this.params.hashNavigation.watchState && s(t).off("hashchange", this.hashNavigation.onHashCange)
                    }
                },
                ee = {
                    run: function() {
                        var e = this,
                            t = e.slides.eq(e.activeIndex),
                            i = e.params.autoplay.delay;
                        t.attr("data-swiper-autoplay") && (i = t.attr("data-swiper-autoplay") || e.params.autoplay.delay), e.autoplay.timeout = d.nextTick(function() {
                            e.params.autoplay.reverseDirection ? e.params.loop ? (e.loopFix(), e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.isBeginning ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.params.loop ? (e.loopFix(), e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")) : e.isEnd ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(0, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slideNext(e.params.speed, !0, !0), e.emit("autoplay"))
                        }, i)
                    },
                    start: function() {
                        return void 0 === this.autoplay.timeout && (!this.autoplay.running && (this.autoplay.running = !0, this.emit("autoplayStart"), this.autoplay.run(), !0))
                    },
                    stop: function() {
                        return !!this.autoplay.running && (void 0 !== this.autoplay.timeout && (this.autoplay.timeout && (clearTimeout(this.autoplay.timeout), this.autoplay.timeout = void 0), this.autoplay.running = !1, this.emit("autoplayStop"), !0))
                    },
                    pause: function(e) {
                        var t = this;
                        t.autoplay.running && (t.autoplay.paused || (t.autoplay.timeout && clearTimeout(t.autoplay.timeout), t.autoplay.paused = !0, 0 !== e && t.params.autoplay.waitForTransition ? t.$wrapperEl.transitionEnd(function() {
                            t && !t.destroyed && (t.autoplay.paused = !1, t.autoplay.running ? t.autoplay.run() : t.autoplay.stop())
                        }) : (t.autoplay.paused = !1, t.autoplay.run())))
                    }
                },
                te = {
                    setTranslate: function() {
                        for (var e = this.slides, t = 0; t < e.length; t += 1) {
                            var i = this.slides.eq(t),
                                s = -i[0].swiperSlideOffset;
                            this.params.virtualTranslate || (s -= this.translate);
                            var a = 0;
                            this.isHorizontal() || (a = s, s = 0);
                            var r = this.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(i[0].progress), 0) : 1 + Math.min(Math.max(i[0].progress, -1), 0);
                            i.css({
                                opacity: r
                            }).transform("translate3d(" + s + "px, " + a + "px, 0px)")
                        }
                    },
                    setTransition: function(e) {
                        var t = this,
                            i = t.slides,
                            s = t.$wrapperEl;
                        if (i.transition(e), t.params.virtualTranslate && 0 !== e) {
                            var a = !1;
                            i.transitionEnd(function() {
                                if (!a && t && !t.destroyed) {
                                    a = !0, t.animating = !1;
                                    for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) s.trigger(e[i])
                                }
                            })
                        }
                    }
                },
                ie = {
                    setTranslate: function() {
                        var e, t = this.$el,
                            i = this.$wrapperEl,
                            a = this.slides,
                            r = this.width,
                            n = this.height,
                            o = this.rtl,
                            l = this.size,
                            d = this.params.cubeEffect,
                            h = this.isHorizontal(),
                            p = this.virtual && this.params.virtual.enabled,
                            c = 0;
                        d.shadow && (h ? (0 === (e = i.find(".swiper-cube-shadow")).length && (e = s('<div class="swiper-cube-shadow"></div>'), i.append(e)), e.css({
                            height: r + "px"
                        })) : 0 === (e = t.find(".swiper-cube-shadow")).length && (e = s('<div class="swiper-cube-shadow"></div>'), t.append(e)));
                        for (var u = 0; u < a.length; u += 1) {
                            var v = a.eq(u),
                                f = u;
                            p && (f = parseInt(v.attr("data-swiper-slide-index"), 10));
                            var m = 90 * f,
                                g = Math.floor(m / 360);
                            o && (m = -m, g = Math.floor(-m / 360));
                            var b = Math.max(Math.min(v[0].progress, 1), -1),
                                w = 0,
                                y = 0,
                                x = 0;
                            f % 4 == 0 ? (w = 4 * -g * l, x = 0) : (f - 1) % 4 == 0 ? (w = 0, x = 4 * -g * l) : (f - 2) % 4 == 0 ? (w = l + 4 * g * l, x = l) : (f - 3) % 4 == 0 && (w = -l, x = 3 * l + 4 * l * g), o && (w = -w), h || (y = w, w = 0);
                            var E = "rotateX(" + (h ? 0 : -m) + "deg) rotateY(" + (h ? m : 0) + "deg) translate3d(" + w + "px, " + y + "px, " + x + "px)";
                            if (b <= 1 && b > -1 && (c = 90 * f + 90 * b, o && (c = 90 * -f - 90 * b)), v.transform(E), d.slideShadows) {
                                var T = h ? v.find(".swiper-slide-shadow-left") : v.find(".swiper-slide-shadow-top"),
                                    S = h ? v.find(".swiper-slide-shadow-right") : v.find(".swiper-slide-shadow-bottom");
                                0 === T.length && (T = s('<div class="swiper-slide-shadow-' + (h ? "left" : "top") + '"></div>'), v.append(T)), 0 === S.length && (S = s('<div class="swiper-slide-shadow-' + (h ? "right" : "bottom") + '"></div>'), v.append(S)), T.length && (T[0].style.opacity = Math.max(-b, 0)), S.length && (S[0].style.opacity = Math.max(b, 0))
                            }
                        }
                        if (i.css({
                                "-webkit-transform-origin": "50% 50% -" + l / 2 + "px",
                                "-moz-transform-origin": "50% 50% -" + l / 2 + "px",
                                "-ms-transform-origin": "50% 50% -" + l / 2 + "px",
                                "transform-origin": "50% 50% -" + l / 2 + "px"
                            }), d.shadow)
                            if (h) e.transform("translate3d(0px, " + (r / 2 + d.shadowOffset) + "px, " + -r / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + d.shadowScale + ")");
                            else {
                                var C = Math.abs(c) - 90 * Math.floor(Math.abs(c) / 90),
                                    M = 1.5 - (Math.sin(2 * C * Math.PI / 360) / 2 + Math.cos(2 * C * Math.PI / 360) / 2),
                                    z = d.shadowScale,
                                    k = d.shadowScale / M,
                                    $ = d.shadowOffset;
                                e.transform("scale3d(" + z + ", 1, " + k + ") translate3d(0px, " + (n / 2 + $) + "px, " + -n / 2 / k + "px) rotateX(-90deg)")
                            }
                        var L = P.isSafari || P.isUiWebView ? -l / 2 : 0;
                        i.transform("translate3d(0px,0," + L + "px) rotateX(" + (this.isHorizontal() ? 0 : c) + "deg) rotateY(" + (this.isHorizontal() ? -c : 0) + "deg)")
                    },
                    setTransition: function(e) {
                        var t = this.$el;
                        this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), this.params.cubeEffect.shadow && !this.isHorizontal() && t.find(".swiper-cube-shadow").transition(e)
                    }
                },
                se = {
                    setTranslate: function() {
                        for (var e = this.slides, t = 0; t < e.length; t += 1) {
                            var i = e.eq(t),
                                a = i[0].progress;
                            this.params.flipEffect.limitRotation && (a = Math.max(Math.min(i[0].progress, 1), -1));
                            var r = -180 * a,
                                n = 0,
                                o = -i[0].swiperSlideOffset,
                                l = 0;
                            if (this.isHorizontal() ? this.rtl && (r = -r) : (l = o, o = 0, n = -r, r = 0), i[0].style.zIndex = -Math.abs(Math.round(a)) + e.length, this.params.flipEffect.slideShadows) {
                                var d = this.isHorizontal() ? i.find(".swiper-slide-shadow-left") : i.find(".swiper-slide-shadow-top"),
                                    h = this.isHorizontal() ? i.find(".swiper-slide-shadow-right") : i.find(".swiper-slide-shadow-bottom");
                                0 === d.length && (d = s('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "left" : "top") + '"></div>'), i.append(d)), 0 === h.length && (h = s('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "right" : "bottom") + '"></div>'), i.append(h)), d.length && (d[0].style.opacity = Math.max(-a, 0)), h.length && (h[0].style.opacity = Math.max(a, 0))
                            }
                            i.transform("translate3d(" + o + "px, " + l + "px, 0px) rotateX(" + n + "deg) rotateY(" + r + "deg)")
                        }
                    },
                    setTransition: function(e) {
                        var t = this,
                            i = t.slides,
                            s = t.activeIndex,
                            a = t.$wrapperEl;
                        if (i.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), t.params.virtualTranslate && 0 !== e) {
                            var r = !1;
                            i.eq(s).transitionEnd(function() {
                                if (!r && t && !t.destroyed) {
                                    r = !0, t.animating = !1;
                                    for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) a.trigger(e[i])
                                }
                            })
                        }
                    }
                },
                ae = {
                    setTranslate: function() {
                        for (var e = this.width, t = this.height, i = this.slides, a = this.$wrapperEl, r = this.slidesSizesGrid, n = this.params.coverflowEffect, o = this.isHorizontal(), l = this.translate, d = o ? e / 2 - l : t / 2 - l, p = o ? n.rotate : -n.rotate, c = n.depth, u = 0, v = i.length; u < v; u += 1) {
                            var f = i.eq(u),
                                m = r[u],
                                g = (d - f[0].swiperSlideOffset - m / 2) / m * n.modifier,
                                b = o ? p * g : 0,
                                w = o ? 0 : p * g,
                                y = -c * Math.abs(g),
                                x = o ? 0 : n.stretch * g,
                                E = o ? n.stretch * g : 0;
                            Math.abs(E) < .001 && (E = 0), Math.abs(x) < .001 && (x = 0), Math.abs(y) < .001 && (y = 0), Math.abs(b) < .001 && (b = 0), Math.abs(w) < .001 && (w = 0);
                            var T = "translate3d(" + E + "px," + x + "px," + y + "px) rotateX(" + w + "deg) rotateY(" + b + "deg)";
                            if (f.transform(T), f[0].style.zIndex = 1 - Math.abs(Math.round(g)), n.slideShadows) {
                                var S = o ? f.find(".swiper-slide-shadow-left") : f.find(".swiper-slide-shadow-top"),
                                    C = o ? f.find(".swiper-slide-shadow-right") : f.find(".swiper-slide-shadow-bottom");
                                0 === S.length && (S = s('<div class="swiper-slide-shadow-' + (o ? "left" : "top") + '"></div>'), f.append(S)), 0 === C.length && (C = s('<div class="swiper-slide-shadow-' + (o ? "right" : "bottom") + '"></div>'), f.append(C)), S.length && (S[0].style.opacity = g > 0 ? g : 0), C.length && (C[0].style.opacity = -g > 0 ? -g : 0)
                            }
                        }(h.pointerEvents || h.prefixedPointerEvents) && (a[0].style.perspectiveOrigin = d + "px 50%")
                    },
                    setTransition: function(e) {
                        this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)
                    }
                },
                re = [D, O, A, H, X, B, V, {
                    name: "mousewheel",
                    params: {
                        mousewheel: {
                            enabled: !1,
                            releaseOnEdges: !1,
                            invert: !1,
                            forceToAxis: !1,
                            sensitivity: 1,
                            eventsTarged: "container"
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            mousewheel: {
                                enabled: !1,
                                enable: R.enable.bind(this),
                                disable: R.disable.bind(this),
                                handle: R.handle.bind(this),
                                lastScrollTime: d.now()
                            }
                        })
                    },
                    on: {
                        init: function() {
                            this.params.mousewheel.enabled && this.mousewheel.enable()
                        },
                        destroy: function() {
                            this.mousewheel.enabled && this.mousewheel.disable()
                        }
                    }
                }, {
                    name: "navigation",
                    params: {
                        navigation: {
                            nextEl: null,
                            prevEl: null,
                            hideOnClick: !1,
                            disabledClass: "swiper-button-disabled",
                            hiddenClass: "swiper-button-hidden",
                            lockClass: "swiper-button-lock"
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            navigation: {
                                init: F.init.bind(this),
                                update: F.update.bind(this),
                                destroy: F.destroy.bind(this)
                            }
                        })
                    },
                    on: {
                        init: function() {
                            this.navigation.init(), this.navigation.update()
                        },
                        toEdge: function() {
                            this.navigation.update()
                        },
                        fromEdge: function() {
                            this.navigation.update()
                        },
                        destroy: function() {
                            this.navigation.destroy()
                        },
                        click: function(e) {
                            var t = this.navigation,
                                i = t.$nextEl,
                                a = t.$prevEl;
                            !this.params.navigation.hideOnClick || s(e.target).is(a) || s(e.target).is(i) || (i && i.toggleClass(this.params.navigation.hiddenClass), a && a.toggleClass(this.params.navigation.hiddenClass))
                        }
                    }
                }, {
                    name: "pagination",
                    params: {
                        pagination: {
                            el: null,
                            bulletElement: "span",
                            clickable: !1,
                            hideOnClick: !1,
                            renderBullet: null,
                            renderProgressbar: null,
                            renderFraction: null,
                            renderCustom: null,
                            type: "bullets",
                            dynamicBullets: !1,
                            dynamicMainBullets: 1,
                            bulletClass: "swiper-pagination-bullet",
                            bulletActiveClass: "swiper-pagination-bullet-active",
                            modifierClass: "swiper-pagination-",
                            currentClass: "swiper-pagination-current",
                            totalClass: "swiper-pagination-total",
                            hiddenClass: "swiper-pagination-hidden",
                            progressbarFillClass: "swiper-pagination-progressbar-fill",
                            clickableClass: "swiper-pagination-clickable",
                            lockClass: "swiper-pagination-lock"
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            pagination: {
                                init: W.init.bind(this),
                                render: W.render.bind(this),
                                update: W.update.bind(this),
                                destroy: W.destroy.bind(this),
                                dynamicBulletIndex: 0
                            }
                        })
                    },
                    on: {
                        init: function() {
                            this.pagination.init(), this.pagination.render(), this.pagination.update()
                        },
                        activeIndexChange: function() {
                            this.params.loop ? this.pagination.update() : void 0 === this.snapIndex && this.pagination.update()
                        },
                        snapIndexChange: function() {
                            this.params.loop || this.pagination.update()
                        },
                        slidesLengthChange: function() {
                            this.params.loop && (this.pagination.render(), this.pagination.update())
                        },
                        snapGridLengthChange: function() {
                            this.params.loop || (this.pagination.render(), this.pagination.update())
                        },
                        destroy: function() {
                            this.pagination.destroy()
                        },
                        click: function(e) {
                            this.params.pagination.el && this.params.pagination.hideOnClick && this.pagination.$el.length > 0 && !s(e.target).hasClass(this.params.pagination.bulletClass) && this.pagination.$el.toggleClass(this.params.pagination.hiddenClass)
                        }
                    }
                }, {
                    name: "scrollbar",
                    params: {
                        scrollbar: {
                            el: null,
                            dragSize: "auto",
                            hide: !1,
                            draggable: !1,
                            snapOnRelease: !0,
                            lockClass: "swiper-scrollbar-lock",
                            dragClass: "swiper-scrollbar-drag"
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            scrollbar: {
                                init: q.init.bind(this),
                                destroy: q.destroy.bind(this),
                                updateSize: q.updateSize.bind(this),
                                setTranslate: q.setTranslate.bind(this),
                                setTransition: q.setTransition.bind(this),
                                enableDraggable: q.enableDraggable.bind(this),
                                disableDraggable: q.disableDraggable.bind(this),
                                setDragPosition: q.setDragPosition.bind(this),
                                onDragStart: q.onDragStart.bind(this),
                                onDragMove: q.onDragMove.bind(this),
                                onDragEnd: q.onDragEnd.bind(this),
                                isTouched: !1,
                                timeout: null,
                                dragTimeout: null
                            }
                        })
                    },
                    on: {
                        init: function() {
                            this.scrollbar.init(), this.scrollbar.updateSize(), this.scrollbar.setTranslate()
                        },
                        update: function() {
                            this.scrollbar.updateSize()
                        },
                        resize: function() {
                            this.scrollbar.updateSize()
                        },
                        observerUpdate: function() {
                            this.scrollbar.updateSize()
                        },
                        setTranslate: function() {
                            this.scrollbar.setTranslate()
                        },
                        setTransition: function(e) {
                            this.scrollbar.setTransition(e)
                        },
                        destroy: function() {
                            this.scrollbar.destroy()
                        }
                    }
                }, {
                    name: "parallax",
                    params: {
                        parallax: {
                            enabled: !1
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            parallax: {
                                setTransform: j.setTransform.bind(this),
                                setTranslate: j.setTranslate.bind(this),
                                setTransition: j.setTransition.bind(this)
                            }
                        })
                    },
                    on: {
                        beforeInit: function() {
                            this.params.parallax.enabled && (this.params.watchSlidesProgress = !0)
                        },
                        init: function() {
                            this.params.parallax && this.parallax.setTranslate()
                        },
                        setTranslate: function() {
                            this.params.parallax && this.parallax.setTranslate()
                        },
                        setTransition: function(e) {
                            this.params.parallax && this.parallax.setTransition(e)
                        }
                    }
                }, {
                    name: "zoom",
                    params: {
                        zoom: {
                            enabled: !1,
                            maxRatio: 3,
                            minRatio: 1,
                            toggle: !0,
                            containerClass: "swiper-zoom-container",
                            zoomedSlideClass: "swiper-slide-zoomed"
                        }
                    },
                    create: function() {
                        var e = this,
                            t = {
                                enabled: !1,
                                scale: 1,
                                currentScale: 1,
                                isScaling: !1,
                                gesture: {
                                    $slideEl: void 0,
                                    slideWidth: void 0,
                                    slideHeight: void 0,
                                    $imageEl: void 0,
                                    $imageWrapEl: void 0,
                                    maxRatio: 3
                                },
                                image: {
                                    isTouched: void 0,
                                    isMoved: void 0,
                                    currentX: void 0,
                                    currentY: void 0,
                                    minX: void 0,
                                    minY: void 0,
                                    maxX: void 0,
                                    maxY: void 0,
                                    width: void 0,
                                    height: void 0,
                                    startX: void 0,
                                    startY: void 0,
                                    touchesStart: {},
                                    touchesCurrent: {}
                                },
                                velocity: {
                                    x: void 0,
                                    y: void 0,
                                    prevPositionX: void 0,
                                    prevPositionY: void 0,
                                    prevTime: void 0
                                }
                            };
                        "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function(i) {
                            t[i] = K[i].bind(e)
                        }), d.extend(e, {
                            zoom: t
                        })
                    },
                    on: {
                        init: function() {
                            this.params.zoom.enabled && this.zoom.enable()
                        },
                        destroy: function() {
                            this.zoom.disable()
                        },
                        touchStart: function(e) {
                            this.zoom.enabled && this.zoom.onTouchStart(e)
                        },
                        touchEnd: function(e) {
                            this.zoom.enabled && this.zoom.onTouchEnd(e)
                        },
                        doubleTap: function(e) {
                            this.params.zoom.enabled && this.zoom.enabled && this.params.zoom.toggle && this.zoom.toggle(e)
                        },
                        transitionEnd: function() {
                            this.zoom.enabled && this.params.zoom.enabled && this.zoom.onTransitionEnd()
                        }
                    }
                }, {
                    name: "lazy",
                    params: {
                        lazy: {
                            enabled: !1,
                            loadPrevNext: !1,
                            loadPrevNextAmount: 1,
                            loadOnTransitionStart: !1,
                            elementClass: "swiper-lazy",
                            loadingClass: "swiper-lazy-loading",
                            loadedClass: "swiper-lazy-loaded",
                            preloaderClass: "swiper-lazy-preloader"
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            lazy: {
                                initialImageLoaded: !1,
                                load: U.load.bind(this),
                                loadInSlide: U.loadInSlide.bind(this)
                            }
                        })
                    },
                    on: {
                        beforeInit: function() {
                            this.params.lazy.enabled && this.params.preloadImages && (this.params.preloadImages = !1)
                        },
                        init: function() {
                            this.params.lazy.enabled && !this.params.loop && 0 === this.params.initialSlide && this.lazy.load()
                        },
                        scroll: function() {
                            this.params.freeMode && !this.params.freeModeSticky && this.lazy.load()
                        },
                        resize: function() {
                            this.params.lazy.enabled && this.lazy.load()
                        },
                        scrollbarDragMove: function() {
                            this.params.lazy.enabled && this.lazy.load()
                        },
                        transitionStart: function() {
                            this.params.lazy.enabled && (this.params.lazy.loadOnTransitionStart || !this.params.lazy.loadOnTransitionStart && !this.lazy.initialImageLoaded) && this.lazy.load()
                        },
                        transitionEnd: function() {
                            this.params.lazy.enabled && !this.params.lazy.loadOnTransitionStart && this.lazy.load()
                        }
                    }
                }, {
                    name: "controller",
                    params: {
                        controller: {
                            control: void 0,
                            inverse: !1,
                            by: "slide"
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            controller: {
                                control: this.params.controller.control,
                                getInterpolateFunction: _.getInterpolateFunction.bind(this),
                                setTranslate: _.setTranslate.bind(this),
                                setTransition: _.setTransition.bind(this)
                            }
                        })
                    },
                    on: {
                        update: function() {
                            this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                        },
                        resize: function() {
                            this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                        },
                        observerUpdate: function() {
                            this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                        },
                        setTranslate: function(e, t) {
                            this.controller.control && this.controller.setTranslate(e, t)
                        },
                        setTransition: function(e, t) {
                            this.controller.control && this.controller.setTransition(e, t)
                        }
                    }
                }, {
                    name: "a11y",
                    params: {
                        a11y: {
                            enabled: !1,
                            notificationClass: "swiper-notification",
                            prevSlideMessage: "Previous slide",
                            nextSlideMessage: "Next slide",
                            firstSlideMessage: "This is the first slide",
                            lastSlideMessage: "This is the last slide",
                            paginationBulletMessage: "Go to slide index"
                        }
                    },
                    create: function() {
                        var e = this;
                        d.extend(e, {
                            a11y: {
                                liveRegion: s('<span class="' + e.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>')
                            }
                        }), Object.keys(Z).forEach(function(t) {
                            e.a11y[t] = Z[t].bind(e)
                        })
                    },
                    on: {
                        init: function() {
                            this.params.a11y.enabled && (this.a11y.init(), this.a11y.updateNavigation())
                        },
                        toEdge: function() {
                            this.params.a11y.enabled && this.a11y.updateNavigation()
                        },
                        fromEdge: function() {
                            this.params.a11y.enabled && this.a11y.updateNavigation()
                        },
                        paginationUpdate: function() {
                            this.params.a11y.enabled && this.a11y.updatePagination()
                        },
                        destroy: function() {
                            this.params.a11y.enabled && this.a11y.destroy()
                        }
                    }
                }, {
                    name: "history",
                    params: {
                        history: {
                            enabled: !1,
                            replaceState: !1,
                            key: "slides"
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            history: {
                                init: Q.init.bind(this),
                                setHistory: Q.setHistory.bind(this),
                                setHistoryPopState: Q.setHistoryPopState.bind(this),
                                scrollToSlide: Q.scrollToSlide.bind(this),
                                destroy: Q.destroy.bind(this)
                            }
                        })
                    },
                    on: {
                        init: function() {
                            this.params.history.enabled && this.history.init()
                        },
                        destroy: function() {
                            this.params.history.enabled && this.history.destroy()
                        },
                        transitionEnd: function() {
                            this.history.initialized && this.history.setHistory(this.params.history.key, this.activeIndex)
                        }
                    }
                }, {
                    name: "hash-navigation",
                    params: {
                        hashNavigation: {
                            enabled: !1,
                            replaceState: !1,
                            watchState: !1
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            hashNavigation: {
                                initialized: !1,
                                init: J.init.bind(this),
                                destroy: J.destroy.bind(this),
                                setHash: J.setHash.bind(this),
                                onHashCange: J.onHashCange.bind(this)
                            }
                        })
                    },
                    on: {
                        init: function() {
                            this.params.hashNavigation.enabled && this.hashNavigation.init()
                        },
                        destroy: function() {
                            this.params.hashNavigation.enabled && this.hashNavigation.destroy()
                        },
                        transitionEnd: function() {
                            this.hashNavigation.initialized && this.hashNavigation.setHash()
                        }
                    }
                }, {
                    name: "autoplay",
                    params: {
                        autoplay: {
                            enabled: !1,
                            delay: 3e3,
                            waitForTransition: !0,
                            disableOnInteraction: !0,
                            stopOnLastSlide: !1,
                            reverseDirection: !1
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            autoplay: {
                                running: !1,
                                paused: !1,
                                run: ee.run.bind(this),
                                start: ee.start.bind(this),
                                stop: ee.stop.bind(this),
                                pause: ee.pause.bind(this)
                            }
                        })
                    },
                    on: {
                        init: function() {
                            this.params.autoplay.enabled && this.autoplay.start()
                        },
                        beforeTransitionStart: function(e, t) {
                            this.autoplay.running && (t || !this.params.autoplay.disableOnInteraction ? this.autoplay.pause(e) : this.autoplay.stop())
                        },
                        sliderFirstMove: function() {
                            this.autoplay.running && (this.params.autoplay.disableOnInteraction ? this.autoplay.stop() : this.autoplay.pause())
                        },
                        destroy: function() {
                            this.autoplay.running && this.autoplay.stop()
                        }
                    }
                }, {
                    name: "effect-fade",
                    params: {
                        fadeEffect: {
                            crossFade: !1
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            fadeEffect: {
                                setTranslate: te.setTranslate.bind(this),
                                setTransition: te.setTransition.bind(this)
                            }
                        })
                    },
                    on: {
                        beforeInit: function() {
                            if ("fade" === this.params.effect) {
                                this.classNames.push(this.params.containerModifierClass + "fade");
                                var e = {
                                    slidesPerView: 1,
                                    slidesPerColumn: 1,
                                    slidesPerGroup: 1,
                                    watchSlidesProgress: !0,
                                    spaceBetween: 0,
                                    virtualTranslate: !0
                                };
                                d.extend(this.params, e), d.extend(this.originalParams, e)
                            }
                        },
                        setTranslate: function() {
                            "fade" === this.params.effect && this.fadeEffect.setTranslate()
                        },
                        setTransition: function(e) {
                            "fade" === this.params.effect && this.fadeEffect.setTransition(e)
                        }
                    }
                }, {
                    name: "effect-cube",
                    params: {
                        cubeEffect: {
                            slideShadows: !0,
                            shadow: !0,
                            shadowOffset: 20,
                            shadowScale: .94
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            cubeEffect: {
                                setTranslate: ie.setTranslate.bind(this),
                                setTransition: ie.setTransition.bind(this)
                            }
                        })
                    },
                    on: {
                        beforeInit: function() {
                            if ("cube" === this.params.effect) {
                                this.classNames.push(this.params.containerModifierClass + "cube"), this.classNames.push(this.params.containerModifierClass + "3d");
                                var e = {
                                    slidesPerView: 1,
                                    slidesPerColumn: 1,
                                    slidesPerGroup: 1,
                                    watchSlidesProgress: !0,
                                    resistanceRatio: 0,
                                    spaceBetween: 0,
                                    centeredSlides: !1,
                                    virtualTranslate: !0
                                };
                                d.extend(this.params, e), d.extend(this.originalParams, e)
                            }
                        },
                        setTranslate: function() {
                            "cube" === this.params.effect && this.cubeEffect.setTranslate()
                        },
                        setTransition: function(e) {
                            "cube" === this.params.effect && this.cubeEffect.setTransition(e)
                        }
                    }
                }, {
                    name: "effect-flip",
                    params: {
                        flipEffect: {
                            slideShadows: !0,
                            limitRotation: !0
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            flipEffect: {
                                setTranslate: se.setTranslate.bind(this),
                                setTransition: se.setTransition.bind(this)
                            }
                        })
                    },
                    on: {
                        beforeInit: function() {
                            if ("flip" === this.params.effect) {
                                this.classNames.push(this.params.containerModifierClass + "flip"), this.classNames.push(this.params.containerModifierClass + "3d");
                                var e = {
                                    slidesPerView: 1,
                                    slidesPerColumn: 1,
                                    slidesPerGroup: 1,
                                    watchSlidesProgress: !0,
                                    spaceBetween: 0,
                                    virtualTranslate: !0
                                };
                                d.extend(this.params, e), d.extend(this.originalParams, e)
                            }
                        },
                        setTranslate: function() {
                            "flip" === this.params.effect && this.flipEffect.setTranslate()
                        },
                        setTransition: function(e) {
                            "flip" === this.params.effect && this.flipEffect.setTransition(e)
                        }
                    }
                }, {
                    name: "effect-coverflow",
                    params: {
                        coverflowEffect: {
                            rotate: 50,
                            stretch: 0,
                            depth: 100,
                            modifier: 1,
                            slideShadows: !0
                        }
                    },
                    create: function() {
                        d.extend(this, {
                            coverflowEffect: {
                                setTranslate: ae.setTranslate.bind(this),
                                setTransition: ae.setTransition.bind(this)
                            }
                        })
                    },
                    on: {
                        beforeInit: function() {
                            "coverflow" === this.params.effect && (this.classNames.push(this.params.containerModifierClass + "coverflow"), this.classNames.push(this.params.containerModifierClass + "3d"), this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0)
                        },
                        setTranslate: function() {
                            "coverflow" === this.params.effect && this.coverflowEffect.setTranslate()
                        },
                        setTransition: function(e) {
                            "coverflow" === this.params.effect && this.coverflowEffect.setTransition(e)
                        }
                    }
                }];
            return void 0 === I.use && (I.use = I.Class.use, I.installModule = I.Class.installModule), I.use(re), I
        })
    </script>
    <script>
        $(window).on('load', function() {
            $.ready.then(function() {
                if ($('.preloading').length) {
                    setTimeout(function() {
                        $('.preloading').addClass('preloading--hidden');
                        setTimeout(function() {
                            $('.preloading').remove()
                        }, 350)
                    }, 2000)
                }
                $('[js-handler="navbarToggle"]').on('click', function(evt) {
                    evt.preventDefault();
                    var $this = $(this);
                    $this.toggleClass('clicked');
                    $('[js-handler="navbarLinks"]').slideToggle()
                });
                $('.navbar__link--mobile').on('click', function(evt) {
                    $('[js-handler="navbarLinks"]').slideUp();
                    $('[js-handler="navbarToggle"]').removeClass('clicked')
                });
                var SPMaskBehavior = function(val) {
                        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009'
                    },
                    spOptions = {
                        onKeyPress: function(val, e, field, options) {
                            field.mask(SPMaskBehavior.apply({}, arguments), options)
                        }
                    };
                $('input[name=phone]').mask(SPMaskBehavior, spOptions);
                $(window).on('resize', function() {
                    $('[js-handler="textarea"]').css('min-height', '0');
                    $('[js-handler="textarea"]').css('min-height', $('[js-handler="contactForm"]').height())
                });
                $('[js-handler="textarea"]').css('min-height', $('[js-handler="contactForm"]').height());
                $('[js-handler="contactForm"]').on('submit', function(evt) {
                    evt.preventDefault();
                    var $this = $(this);
                    $.ajax({
                        type: $this.attr('method'),
                        url: $this.attr('action'),
                        data: new FormData($this[0]),
                        cache: !1,
                        processContent: !1,
                        processData: !1,
                        contentType: !1,
                        dataType: 'json',
                        beforeSend: function() {
                            $this.find('button[type=submit]').prop('disabled', !0).text('Aguarde...')
                        },
                        success: function(response) {
                            notifyit({
                                'message': response[1],
                                'status': (response[0]) ? 'success' : 'error'
                            });
                            if (response[0]) {
                                $this[0].reset()
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            notifyit({
                                'message': errorThrown,
                                'status': 'error'
                            })
                        },
                        complete: function() {
                            $this.find('button[type=submit]').prop('disabled', !1).text('Enviar mensagem')
                        }
                    })
                })
            })
        })
    </script>
    <script>
        $(document).ready(function() {
            var $scrollToTarget = $('[js-handler="scrollToTarget"]');
            $scrollToTarget.waypoint(function(direction) {
                if (direction == 'up') {
                    $('.navbar__link').removeClass('active');
                    var $section = $(this.element);
                    var sectionTarget = $section.attr('js-target');
                    var navbarLink = $('a[href="' + sectionTarget + '"]');
                    navbarLink.addClass('active')
                }
            }, {
                offset: '99px'
            });
            $scrollToTarget.waypoint(function(direction) {
                if (direction == 'down') {
                    $('.navbar__link').removeClass('active');
                    var $section = $(this.element);
                    var sectionTarget = $section.attr('js-target');
                    var navbarLink = $('a[href="' + sectionTarget + '"]');
                    navbarLink.addClass('active')
                }
            }, {
                offset: '100px'
            });
            $('[js-handler="scrollToLink"]').on('click', function(evt) {
                evt.preventDefault();
                var $this = $(this);
                var href = $this.attr('href');
                $('html, body').animate({
                    scrollTop: $('[js-target="' + href + '"]').offset().top - 100
                }, 600)
            });
            $('.gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: !1,
                closeBtnInside: !1,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: !0
                },
                gallery: {
                    enabled: !0
                },
                zoom: {
                    enabled: !0,
                    duration: 300,
                    opener: function(element) {
                        return element.find('img')
                    }
                }
            });
            var videoSlider = new Swiper('.video-slider', {
                navigation: {
                    nextEl: '.next-video',
                    prevEl: '.prev-video',
                }
            })
        })
    </script>
</body>

</html>
