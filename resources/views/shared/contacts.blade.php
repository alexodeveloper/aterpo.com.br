<section class="section section--contact" js-handler="scrollToTarget" js-target="#contact" id="contact">
    <div class="container">
        <h1 class="section__title">Fale conosco!</h1>
        <p class="section__paragraph">
            Consultório Aterpo<br/>
            Telefone +55 11 3132-4888<br/>
            consultorio@aterpo.com.br<br/>
            <br/>
            Vivien Schmeling Piccin<br>
            Celular + 55 11 99116-8417<br>
            vivien.piccin@aterpo.com.br<br>
            <br>
            Rafaela Garcia Santos de Andrade<br>
            Celular +55 11 98651-8023<br>
            rafaela.andrade@aterpo.com.br<br>
        </p>
        <form action="/ajax/contact-form" method="POST" accept-charset="utf-8" class="contact-form"
              js-handler="contactForm">
            <div class="contact-form__left">
                @csrf
                <input type="text" name="name" placeholder="Nome" maxlength="255" autocomplete="off">
                <input type="text" name="email" placeholder="Email" maxlength="255" autocomplete="off">
                <input type="text" name="phone" placeholder="Telefone" maxlength="255" autocomplete="off">
                <input type="text" name="subject" placeholder="Assunto" maxlength="255" autocomplete="off"></div>
            <div class="contact-form__right">
                <textarea name="message" placeholder="Mensagem" autocomplete="off" maxlength="2048"
                          js-handler="textarea"></textarea>
                <button type="submit">Enviar mensagem</button>
            </div>
        </form>
    </div>
</section>
